page 88060 "Prenta afh.fylgibréf"
{
    // version RDN,SAME
    /*
    PageType = Card;
    SourceTable = "Sales Invoice Header";

    layout
    {
        area(content)
        {
            group("Viðskiptavinur")
            {
                field(SalesInvNo;SalesInvNo)
                {
                    CaptionML = ENU='No.',
                                ISL='Nr.';
                    Editable = false;

                    trigger OnLookup(Text : Text) : Boolean;
                    var
                        SalesInvoice : Record "Sales Invoice Header";
                    begin
                        IF SalesInvoice.GET(Rec."No.") THEN;
                        IF PAGE.RUNMODAL(0,SalesInvoice,SalesInvoice."No.") = ACTION::LookupOK THEN BEGIN
                          IF Rec.GET(SalesInvoice."No.") THEN
                            CurrPage.UPDATE(FALSE)
                        END;
                    end;
                }
                field("Sell-to Customer Name";"Sell-to Customer Name")
                {
                    Editable = false;
                }
            }
            group("Upplýsingar fylgibréfs")
            {
                field("Fjöldi pakka/miða";gvFjöldi)
                {
                }
                field("Fjöldi aukamiða";gvFjöldiAukamiða)
                {
                }
                field("Innsláttarlína 1";Innsláttarlína1)
                {
                }
                field("Innsláttarlína 2";Innsláttarlína2)
                {
                }
                field("Magn 1";Magn1)
                {
                }
                field("Magn 2";Magn2)
                {
                }
                field("Þyngd (kg)";gvÞyngd)
                {
                }
            }
            group(Prenta1)
            {
                CaptionML = ENU='Print',
                            ISL='Prenta';
                field("Merkimiða";Merkimiðar)
                {
                }
                field("Vörufylgibréf";Fylgibréf)
                {
                }
                field("C-Gíró";Gíró)
                {
                }
                field("C-Gíró forprentað";ForprGíró)
                {
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            //Caption = 'Aðgerðir';
            action(Prenta)
            {
                Caption = 'Prenta';
                Image = Print;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;

                trigger OnAction();
                begin
                    Forskoðun := FALSE;
                    Prenta;
                end;
            }
            action("Forskoðun")
            {
                Caption = 'Forskoðun';
                Image = PrintCheck;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;

                trigger OnAction();
                begin
                    Forskoðun := TRUE;
                    Prenta;
                end;
            }
        }
        area(navigation)
        {
            //Caption = 'Færsluleit';
            action(Listi)
            {
                Caption = 'Listi';
                Image = List;
                RunObject = Page "Sales Invoice List";
            }
        }
    }

    trigger OnAfterGetCurrRecord();
    begin
        SalesInvNo := "No.";
    end;

    trigger OnOpenPage();
    var
        SalesInvoice : Record "Sales Invoice Header";
    begin
        gvFjöldi := 1;
        gvFjöldiAukamiða := 0;

        SalesInvoice.FINDFIRST;
        "No." := SalesInvoice."No.";
        IF Rec.GET("No.") THEN
          CurrPage.UPDATE(FALSE);
    end;

    var
        "grSölureikningshaus" : Record "Sales Invoice Header";
        //"grepVörufylgibr" : Report "Vörufylgibréf";
        "gvSölureikningur" : Code[10];
        "gvFjöldi" : Integer;
        "gvFjöldiAukamiða" : Integer;
        "Innsláttarlína1" : Text[25];
        "Innsláttarlína2" : Text[25];
        Magn1 : Integer;
        Magn2 : Integer;
        "gvÞyngd" : Decimal;
        "Merkimiðar" : Boolean;
        "Fylgibréf" : Boolean;
        "Gíró" : Boolean;
        "ForprGíró" : Boolean;
        i : Integer;
        "Forskoðun" : Boolean;
        SalesInvNo : Code[20];

    procedure Prenta();
    begin
        grSölureikningshaus := Rec;
        grSölureikningshaus.SETRECFILTER;

        IF Fylgibréf THEN BEGIN
          IF NOT Landfl(grSölureikningshaus) THEN BEGIN //SAM 18.03.2015
            CLEAR(grepVörufylgibr);
            grepVörufylgibr.SetjaPakkaFjölda(gvFjöldi);
            grepVörufylgibr.SetjaTextaOgMagn(Innsláttarlína1, Innsláttarlína2, Magn1, Magn2);
            grepVörufylgibr.SetjaÞyngd(gvÞyngd);
            grepVörufylgibr.SETTABLEVIEW(grSölureikningshaus);
            IF Forskoðun THEN
              grepVörufylgibr.USEREQUESTPAGE(TRUE)
            ELSE
              grepVörufylgibr.USEREQUESTPAGE(FALSE);
            grepVörufylgibr.RUNMODAL();
          END;
        END;

        Forskoðun := TRUE;
    end;

    procedure Landfl(SH : Record "Sales Invoice Header") : Boolean;
    var
        recFlutningsadili : Record "Shipping Agent";
        //THREC : Record "Transport Waybill SAM";
        PaidByS : Boolean;
    begin
        //eymi 17.03.2015....

        recFlutningsadili.INIT;
        IF recFlutningsadili.GET(SH."Shipping Agent Code") THEN;
        IF recFlutningsadili."Transport Waybill" = recFlutningsadili."Transport Waybill"::Landflutningar THEN BEGIN
          CLEAR(THREC);
          THREC.SetjaPakkaFjölda(gvFjöldi);
          THREC.SetjaÞyngd(gvÞyngd);
          PaidByS := FALSE;
          IF SH."Freight Paid By" = SH."Freight Paid By"::"Senders Account" THEN
            PaidByS := TRUE;
          THREC.CreateTransportHead(5,SH."No.",PaidByS);
          EXIT(TRUE);
        END ELSE
          EXIT(FALSE);
        // EXIT(FALSE);
    end;
    */
}

