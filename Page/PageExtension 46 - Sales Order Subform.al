pageextension 46 "Sales Order Subform Ext" extends "Sales Order Subform"
{
    layout
    {
        addlast(Control1)
        {
            field("Vehicle Bin";"Vehicle Bin")
            {
            }

            field("Silo at Customer side";"Silo at Customer side")
            {
            }

            field(Tank;Tank)
            {
            }

            field("Silo date and time";"Silo date and time")
            {
            }
        }
    }

    actions
    {
        // Add changes to page actions here
    }
    
    var
        myInt : Integer;
}