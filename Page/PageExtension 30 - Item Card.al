pageextension 30 "Item Card Extension" extends "Item Card"
{
    layout
    {
        addlast(Content)
        {
            field("Show On Web";"Show On Web")
            {
            }
        }

        addlast(Purchase)
        {
            field("Vendor description";"Vendor description")
            {
            }
        }
    }

    actions
    {
        // Add changes to page actions here
    }
    
    var
        myInt : Integer;
}