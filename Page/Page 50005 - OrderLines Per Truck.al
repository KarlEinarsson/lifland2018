page 50005 "OrderLines Per Truck"
{
    // version FóðurAfgr0.00

    // rdn.ka = Rue de Net Reykjavik, Kristín Ásgeirsdóttir, kristin@ruedenet.net
    // 
    // Num Date       Jira       Developer Description
    // --- ---------- ---------- --------- -------------------
    // #01 27.11.15              rdn.ka    Changed Dimension Filter field from Lookup to dropdown (Table Relation)
    // #02 10.12.15              rdn.ka    Added Ship to Name field to the list and dimension dropdown filter
    // 
    // //Gamli kóði í onlookup á dimvalue
    // //EXIT(LookUpDimFilter(GLSetup."Global Dimension 2 Code",Text));

    DelayedInsert = true;
    InsertAllowed = false;
    PageType = Worksheet;
    PromotedActionCategoriesML = ENU='New,Process,Reports,Print',
                                 ISL='Nýtt,Vinna,Skýrslur,Prenta';
                                 
    SourceTable = "Sales Line";
    SourceTableView = SORTING("Shortcut Dimension 2 Code","Shipment Date") WHERE(Type=FILTER(Item), 
                      "Document Type"=FILTER(Order..Invoice), 
                      "Unit of Measure Code"=FILTER('KG'), 
                      "Item Category Code"=FILTER(9000));
    layout
    {
        area(content)
        {
            group(Filtering)
            {
                CaptionML = ENU='Filtering',
                            ISL='Afmörkun';
             
                field(GlobalDim2Filter;GlobalDim2Filter)
                {
                    CaptionML = ENU='Vehicle No.',
                                ISL='Bílnúmer';
                    //TableRelation = "Dimension Value".Code WHERE (Code=FILTER('??-???'|('?????'&(<>'*-*'))), "Dimension Code"=CONST(VERKEFNI));

                    trigger OnValidate();
                    begin
                        SETFILTER("Shortcut Dimension 2 Code",GlobalDim2Filter);
                        CurrPage.UPDATE;
                    end;
                }
                field(DateFilter;DateFilter)
                {
                    CaptionML = ENU='Date Filter',
                                ISL='Dags.afmörkun';

                    trigger OnValidate();
                    var
                        ApplicationManagement : Codeunit ApplicationManagement;
                    begin
                        SETFILTER("Shipment Date",FORMAT(DateFilter));
                        CurrPage.UPDATE;
                    end;
                }
            }
            repeater(Group)
            {
                field("Shortcut Dimension 2 Code";"Shortcut Dimension 2 Code")
                {
                }
                field("Vehicle Bin";"Vehicle Bin")
                {
                }
                field("Document No.";"Document No.")
                {
                }
                field(ShipToNameLine;ShipToName)
                {
                    CaptionML = ENU='Ship-to Name',
                                ISL='Sendist-til - Nafn';
                }
                field("No.";"No.")
                {
                }
                field(Description;Description)
                {
                }
                field(Quantity;Quantity)
                {
                }
                field("Unit of Measure";"Unit of Measure")
                {
                }
                field("Document Type";"Document Type")
                {
                }
                field("Sell-to Customer No.";"Sell-to Customer No.")
                {
                }
                field("Line No.";"Line No.")
                {
                }
                field(Type;Type)
                {
                }
                field("Location Code";"Location Code")
                {
                }
                field("Posting Group";"Posting Group")
                {
                }
                field("Shipment Date";"Shipment Date")
                {
                }
                field("Description 2";"Description 2")
                {
                }
                field("Outstanding Quantity";"Outstanding Quantity")
                {
                }
                field("Qty. to Invoice";"Qty. to Invoice")
                {
                }
                field("Qty. to Ship";"Qty. to Ship")
                {
                }
                field("Unit Price";"Unit Price")
                {
                }
                field("Unit Cost (LCY)";"Unit Cost (LCY)")
                {
                }
                field("VAT %";"VAT %")
                {
                }
                field("Line Discount %";"Line Discount %")
                {
                }
                field("Line Discount Amount";"Line Discount Amount")
                {
                }
                field(Amount;Amount)
                {
                }
                field("Amount Including VAT";"Amount Including VAT")
                {
                }
                field("Allow Invoice Disc.";"Allow Invoice Disc.")
                {
                }
                field("Gross Weight";"Gross Weight")
                {
                }
                field("Net Weight";"Net Weight")
                {
                }
                field("Units per Parcel";"Units per Parcel")
                {
                }
                field("Unit Volume";"Unit Volume")
                {
                }
                field("Appl.-to Item Entry";"Appl.-to Item Entry")
                {
                }
                field("Shortcut Dimension 1 Code";"Shortcut Dimension 1 Code")
                {
                }
                field("Customer Price Group";"Customer Price Group")
                {
                }
                field("Job No.";"Job No.")
                {
                }
                field("Work Type Code";"Work Type Code")
                {
                }
                field("Recalculate Invoice Disc.";"Recalculate Invoice Disc.")
                {
                }
                field("Outstanding Amount";"Outstanding Amount")
                {
                }
                field("Qty. Shipped Not Invoiced";"Qty. Shipped Not Invoiced")
                {
                }
                field("Shipped Not Invoiced";"Shipped Not Invoiced")
                {
                }
                field("Quantity Shipped";"Quantity Shipped")
                {
                }
                field("Quantity Invoiced";"Quantity Invoiced")
                {
                }
                field("Shipment No.";"Shipment No.")
                {
                }
                field("Shipment Line No.";"Shipment Line No.")
                {
                }
                field("Profit %";"Profit %")
                {
                }
                field("Bill-to Customer No.";"Bill-to Customer No.")
                {
                }
                field("Inv. Discount Amount";"Inv. Discount Amount")
                {
                }
                field("Purchase Order No.";"Purchase Order No.")
                {
                }
                field("Purch. Order Line No.";"Purch. Order Line No.")
                {
                }
                field("Drop Shipment";"Drop Shipment")
                {
                }
                field("Gen. Bus. Posting Group";"Gen. Bus. Posting Group")
                {
                }
                field("Gen. Prod. Posting Group";"Gen. Prod. Posting Group")
                {
                }
                field("VAT Calculation Type";"VAT Calculation Type")
                {
                }
                field("Transaction Type";"Transaction Type")
                {
                }
                field("Transport Method";"Transport Method")
                {
                }
                field("Attached to Line No.";"Attached to Line No.")
                {
                }
                field("Exit Point";"Exit Point")
                {
                }
                field("Area";Area)
                {
                }
                field("Transaction Specification";"Transaction Specification")
                {
                }
                field("Tax Area Code";"Tax Area Code")
                {
                }
                field("Tax Liable";"Tax Liable")
                {
                }
                field("Tax Group Code";"Tax Group Code")
                {
                }
                field("VAT Clause Code";"VAT Clause Code")
                {
                }
                field("VAT Bus. Posting Group";"VAT Bus. Posting Group")
                {
                }
                field("VAT Prod. Posting Group";"VAT Prod. Posting Group")
                {
                }
                field("Currency Code";"Currency Code")
                {
                }
                field("Outstanding Amount (LCY)";"Outstanding Amount (LCY)")
                {
                }
                field("Shipped Not Invoiced (LCY)";"Shipped Not Invoiced (LCY)")
                {
                }
                field("Reserved Quantity";"Reserved Quantity")
                {
                }
                field(Reserve;Reserve)
                {
                }
                field("Blanket Order No.";"Blanket Order No.")
                {
                }
                field("Blanket Order Line No.";"Blanket Order Line No.")
                {
                }
                field("VAT Base Amount";"VAT Base Amount")
                {
                }
                field("Unit Cost";"Unit Cost")
                {
                }
                field("System-Created Entry";"System-Created Entry")
                {
                }
                field("Line Amount";"Line Amount")
                {
                }
                field("VAT Difference";"VAT Difference")
                {
                }
                field("Inv. Disc. Amount to Invoice";"Inv. Disc. Amount to Invoice")
                {
                }
                field("VAT Identifier";"VAT Identifier")
                {
                }
                field("IC Partner Ref. Type";"IC Partner Ref. Type")
                {
                }
                field("IC Partner Reference";"IC Partner Reference")
                {
                }
                field("Prepayment %";"Prepayment %")
                {
                }
                field("Prepmt. Line Amount";"Prepmt. Line Amount")
                {
                }
                field("Prepmt. Amt. Inv.";"Prepmt. Amt. Inv.")
                {
                }
                field("Prepmt. Amt. Incl. VAT";"Prepmt. Amt. Incl. VAT")
                {
                }
                field("Prepayment Amount";"Prepayment Amount")
                {
                }
                field("Prepmt. VAT Base Amt.";"Prepmt. VAT Base Amt.")
                {
                }
                field("Prepayment VAT %";"Prepayment VAT %")
                {
                }
                field("Prepmt. VAT Calc. Type";"Prepmt. VAT Calc. Type")
                {
                }
                field("Prepayment VAT Identifier";"Prepayment VAT Identifier")
                {
                }
                field("Prepayment Tax Area Code";"Prepayment Tax Area Code")
                {
                }
                field("Prepayment Tax Liable";"Prepayment Tax Liable")
                {
                }
                field("Prepayment Tax Group Code";"Prepayment Tax Group Code")
                {
                }
                field("Prepmt Amt to Deduct";"Prepmt Amt to Deduct")
                {
                }
                field("Prepmt Amt Deducted";"Prepmt Amt Deducted")
                {
                }
                field("Prepayment Line";"Prepayment Line")
                {
                }
                field("Prepmt. Amount Inv. Incl. VAT";"Prepmt. Amount Inv. Incl. VAT")
                {
                }
                field("Prepmt. Amount Inv. (LCY)";"Prepmt. Amount Inv. (LCY)")
                {
                }
                field("IC Partner Code";"IC Partner Code")
                {
                }
                field("Prepmt. VAT Amount Inv. (LCY)";"Prepmt. VAT Amount Inv. (LCY)")
                {
                }
                field("Prepayment VAT Difference";"Prepayment VAT Difference")
                {
                }
                field("Prepmt VAT Diff. to Deduct";"Prepmt VAT Diff. to Deduct")
                {
                }
                field("Prepmt VAT Diff. Deducted";"Prepmt VAT Diff. Deducted")
                {
                }
                field("Dimension Set ID";"Dimension Set ID")
                {
                }
                field("Qty. to Assemble to Order";"Qty. to Assemble to Order")
                {
                }
                field("Qty. to Asm. to Order (Base)";"Qty. to Asm. to Order (Base)")
                {
                }
                field("ATO Whse. Outstanding Qty.";"ATO Whse. Outstanding Qty.")
                {
                }
                field("ATO Whse. Outstd. Qty. (Base)";"ATO Whse. Outstd. Qty. (Base)")
                {
                }
                field("Job Task No.";"Job Task No.")
                {
                }
                field("Job Contract Entry No.";"Job Contract Entry No.")
                {
                }
                field("Posting Date";"Posting Date")
                {
                }
                field("Variant Code";"Variant Code")
                {
                }
                field("Bin Code";"Bin Code")
                {
                }
                field("Qty. per Unit of Measure";"Qty. per Unit of Measure")
                {
                }
                field(Planned;Planned)
                {
                }
                field("Unit of Measure Code";"Unit of Measure Code")
                {
                }
                field("Quantity (Base)";"Quantity (Base)")
                {
                }
                field("Outstanding Qty. (Base)";"Outstanding Qty. (Base)")
                {
                }
                field("Qty. to Invoice (Base)";"Qty. to Invoice (Base)")
                {
                }
                field("Qty. to Ship (Base)";"Qty. to Ship (Base)")
                {
                }
                field("Qty. Shipped Not Invd. (Base)";"Qty. Shipped Not Invd. (Base)")
                {
                }
                field("Qty. Shipped (Base)";"Qty. Shipped (Base)")
                {
                }
                field("Qty. Invoiced (Base)";"Qty. Invoiced (Base)")
                {
                }
                field("Reserved Qty. (Base)";"Reserved Qty. (Base)")
                {
                }
                field("FA Posting Date";"FA Posting Date")
                {
                }
                field("Depreciation Book Code";"Depreciation Book Code")
                {
                }
                field("Depr. until FA Posting Date";"Depr. until FA Posting Date")
                {
                }
                field("Duplicate in Depreciation Book";"Duplicate in Depreciation Book")
                {
                }
                field("Use Duplication List";"Use Duplication List")
                {
                }
                field("Responsibility Center";"Responsibility Center")
                {
                }
                field("Out-of-Stock Substitution";"Out-of-Stock Substitution")
                {
                }
                field("Substitution Available";"Substitution Available")
                {
                }
                field("Originally Ordered No.";"Originally Ordered No.")
                {
                }
                field("Originally Ordered Var. Code";"Originally Ordered Var. Code")
                {
                }
                field("Cross-Reference No.";"Cross-Reference No.")
                {
                }
                field("Unit of Measure (Cross Ref.)";"Unit of Measure (Cross Ref.)")
                {
                }
                field("Cross-Reference Type";"Cross-Reference Type")
                {
                }
                field("Cross-Reference Type No.";"Cross-Reference Type No.")
                {
                }
                field("Item Category Code";"Item Category Code")
                {
                }
                field(Nonstock;Nonstock)
                {
                }
                field("Purchasing Code";"Purchasing Code")
                {
                }
                field("Product Group Code";"Product Group Code")
                {
                }
                field("Special Order";"Special Order")
                {
                }
                field("Special Order Purchase No.";"Special Order Purchase No.")
                {
                }
                field("Special Order Purch. Line No.";"Special Order Purch. Line No.")
                {
                }
                field("Whse. Outstanding Qty.";"Whse. Outstanding Qty.")
                {
                }
                field("Whse. Outstanding Qty. (Base)";"Whse. Outstanding Qty. (Base)")
                {
                }
                field("Completely Shipped";"Completely Shipped")
                {
                }
                field("Requested Delivery Date";"Requested Delivery Date")
                {
                }
                field("Promised Delivery Date";"Promised Delivery Date")
                {
                }
                field("Shipping Time";"Shipping Time")
                {
                }
                field("Outbound Whse. Handling Time";"Outbound Whse. Handling Time")
                {
                }
                field("Planned Delivery Date";"Planned Delivery Date")
                {
                }
                field("Planned Shipment Date";"Planned Shipment Date")
                {
                }
                field("Shipping Agent Code";"Shipping Agent Code")
                {
                }
                field("Shipping Agent Service Code";"Shipping Agent Service Code")
                {
                }
                field("Allow Item Charge Assignment";"Allow Item Charge Assignment")
                {
                }
                field("Qty. to Assign";"Qty. to Assign")
                {
                }
                field("Qty. Assigned";"Qty. Assigned")
                {
                }
                field("Return Qty. to Receive";"Return Qty. to Receive")
                {
                }
                field("Return Qty. to Receive (Base)";"Return Qty. to Receive (Base)")
                {
                }
                field("Return Qty. Rcd. Not Invd.";"Return Qty. Rcd. Not Invd.")
                {
                }
                field("Ret. Qty. Rcd. Not Invd.(Base)";"Ret. Qty. Rcd. Not Invd.(Base)")
                {
                }
                field("Return Rcd. Not Invd.";"Return Rcd. Not Invd.")
                {
                }
                field("Return Rcd. Not Invd. (LCY)";"Return Rcd. Not Invd. (LCY)")
                {
                }
                field("Return Qty. Received";"Return Qty. Received")
                {
                }
                field("Return Qty. Received (Base)";"Return Qty. Received (Base)")
                {
                }
                field("Appl.-from Item Entry";"Appl.-from Item Entry")
                {
                }
                field("BOM Item No.";"BOM Item No.")
                {
                }
                field("Return Receipt No.";"Return Receipt No.")
                {
                }
                field("Return Receipt Line No.";"Return Receipt Line No.")
                {
                }
                field("Return Reason Code";"Return Reason Code")
                {
                }
                field("Allow Line Disc.";"Allow Line Disc.")
                {
                }
                field("Customer Disc. Group";"Customer Disc. Group")
                {
                }
                field(Tank;Tank)
                {
                }
                field("Silo at Customer side";"Silo at Customer side")
                {
                }
                field("Silo date and time";"Silo date and time")
                {
                }
                field("Ferð nr.";"Ferð nr.")
                {
                }
                field(Lota;Lota)
                {
                }
                /*
                field("Store No.";"Store No.")
                {
                }
                field("Current Cust. Price Group";"Current Cust. Price Group")
                {
                }
                field("Current Store Group";"Current Store Group")
                {
                }
                field(Division;Division)
                {
                }
                field("Offer No.";"Offer No.")
                {
                }
                field("Promotion No.";"Promotion No.")
                {
                }
                field("Alloc. Plan Purc. Order No.";"Alloc. Plan Purc. Order No.")
                {
                }
                field("Retail Special Order";"Retail Special Order")
                {
                }
                field("Delivering Method";"Delivering Method")
                {
                }
                field("Vendor Delivers to";"Vendor Delivers to")
                {
                }
                field(Sourcing;Sourcing)
                {
                }
                field("Deliver from";"Deliver from")
                {
                }
                field("Delivery Location Code";"Delivery Location Code")
                {
                }
                field("SPO Prepayment %";"SPO Prepayment %")
                {
                }
                field("Total Payment";"Total Payment")
                {
                }
                field("Whse Process";"Whse Process")
                {
                }
                field(Status;Status)
                {
                }
                field("Delivery Status";"Delivery Status")
                {
                }
                field("Configuration ID";"Configuration ID")
                {
                }
                field("Mandatory Options Exist";"Mandatory Options Exist")
                {
                }
                field("Add.Charge Option";"Add.Charge Option")
                {
                }
                field("Delivery Reference No";"Delivery Reference No")
                {
                }
                field("Delivery/Cancel User ID";"Delivery/Cancel User ID")
                {
                }
                field("Delivery/Cancel Date Time";"Delivery/Cancel Date Time")
                {
                }
                field(Counter;Counter)
                {
                }
                field("Option Value Text";"Option Value Text")
                {
                }
                field("Estimated Delivery Date";"Estimated Delivery Date")
                {
                }
                field("No later than Date";"No later than Date")
                {
                }
                field("Payment-At Order Entry-Limit";"Payment-At Order Entry-Limit")
                {
                }
                field("Payment-At Delivery-Limit";"Payment-At Delivery-Limit")
                {
                }
                field("Return Policy";"Return Policy")
                {
                }
                field("Non Refund Amount";"Non Refund Amount")
                {
                }
                field("Sourcing Status";"Sourcing Status")
                {
                }
                field("Payment-At PurchaseOrder-Limit";"Payment-At PurchaseOrder-Limit")
                {
                }
                field("SPO Document Method";"SPO Document Method")
                {
                }
                field("Store Sales Location";"Store Sales Location")
                {
                }
                field("SPO Whse Location";"SPO Whse Location")
                {
                }
                field("Vendor No.";"Vendor No.")
                {
                }
                field("Item Tracking No.";"Item Tracking No.")
                {
                }
                field("Payment Profile Code";"Payment Profile Code")
                {
                }
                field("Created At POS Term No.";"Created At POS Term No.")
                {
                }
                field("Error in Process";"Error in Process")
                {
                }
                field("Cancel Permitted";"Cancel Permitted")
                {
                }
                field("Linked to Line No.";"Linked to Line No.")
                {
                }
                field("Add.charge code";"Add.charge code")
                {
                }
                field("Reserved By POS No.";"Reserved By POS No.")
                {
                }
                field("Pre Cancel Sourcing Status";"Pre Cancel Sourcing Status")
                {
                }
                field("Cancel Reason Code";"Cancel Reason Code")
                {
                }
                field("Purchase Order Status";"Purchase Order Status")
                {
                }
                field("Infocode Exists";"Infocode Exists")
                {
                }
                */
            }
            group(Information)
            {
                CaptionML = ENU='Information',
                            ISL='Upplýsingar';
                field(ShipToName;ShipToName)
                {
                    CaptionML = ENU='Ship-to Name',
                                ISL='Sendist-til - Nafn';
                }
                field(ShipToAddress;ShipToAddress)
                {
                    CaptionML = ENU='Ship-to Address',
                                ISL='Sendist-til - Heimilisfang';
                }
                field(BarcodeText;BarcodeText)
                {
                    CaptionML = ENU='Barcode',
                                ISL='Strikamerki';
                }
                field(TotalQuantity;TotalQuantity)
                {
                    CaptionML = ENU='Total Quantity',
                                ISL='Heildarmagn';
                }
            }
        }
    }

    actions
    {
        area(navigation)
        {
            action("Sölupöntun")
            {
                CaptionML = ENU='Sales Order',
                            ISL='Sölupöntun';
                Image = "Order";
                RunObject = Page "Sales Order";
                RunPageLink = "Document Type"=FIELD("Document Type"), "No."=FIELD("Document No.");
            }
        }
        area(processing)
        {
            action("Lesa frá Agro")
            {
                CaptionML = ENU='Import from Agro',
                            ISL='Lesa frá Agro';
                Image = Import;
                Promoted = true;
                PromotedCategory = Process;
                PromotedIsBig = true;

                trigger OnAction();
                begin
                    //C50005.ReadAGROEXPORT;
                end;
            }
            action("Afgreiðsluseðill bílstjóra")
            {
                CaptionML = ENU='Drivers Delivery Note',
                            ISL='Afgreiðsluseðill bílstjóra';
                Image = Print;
                Promoted = true;
                PromotedCategory = Category4;
                PromotedIsBig = true;

                trigger OnAction();
                begin
                    SL.SETVIEW(GETVIEW);
                    REPORT.RUNMODAL(50006,TRUE,FALSE,SL);
                end;
            }
            action("Sýnatökumiði")
            {
                CaptionML = ENU='Sample Note',
                            ISL='Sýnatökumiði';
                Image = Print;
                Promoted = true;
                PromotedCategory = Category4;
                PromotedIsBig = true;

                trigger OnAction();
                begin
                    //C50005.ReadAGROEXPORT;

                    SL.SETVIEW(GETVIEW);
                    REPORT.RUNMODAL(50008,TRUE,FALSE,SL);
                end;
            }
            action("Afhendingarseðill")
            {
                CaptionML = ENU='Delivery Note',
                            ISL='Afhendingarseðill';
                Image = Print;
                Promoted = true;
                PromotedCategory = Category4;
                PromotedIsBig = true;

                trigger OnAction();
                begin
                    //C50005.ReadAGROEXPORT;

                    SH.SETRANGE("Document Type",Rec."Document Type");
                    SH.SETRANGE("No.","Document No.");
                    REPORT.RUNMODAL(50007,TRUE,FALSE,SH);
                end;
            }
        }
    }

    trigger OnAfterGetCurrRecord();
    begin
        IF HASFILTER THEN BEGIN
          SL.SETCURRENTKEY(SL."Shortcut Dimension 2 Code",SL."Shipment Date");
          COPYFILTER("Shipment Date",SL."Shipment Date");
          COPYFILTER("Shortcut Dimension 2 Code",SL."Shortcut Dimension 2 Code");
        //  SL.SETFILTER("Unit of Measure Code",'%1','KG');
        //  SL.SETFILTER("Item Category Code",'%1','9000');
          SL.CALCSUMS(SL.Quantity);
          TotalQuantity := SL.Quantity;
        END
        ELSE
          TotalQuantity := 0;

        IF (GETFILTER("Shipment Date") <> '') AND (DateFilter = '') THEN
          DateFilter := GETFILTER("Shipment Date");
    end;

    trigger OnAfterGetRecord();
    begin

        SH.SETRANGE("Document Type","Document Type");
        SH.SETRANGE("No.","Document No.");
        IF SH.FINDFIRST THEN BEGIN
          ShipToName := SH."Ship-to Name";
          ShipToAddress := SH."Ship-to Address";
        END
        ELSE BEGIN
          CLEAR(ShipToName);
          CLEAR(ShipToAddress);
        END;

        BarcodeText := "Document No." + ';' +
                       FORMAT("Line No.") + ';' +
                       "No." + ';' +
                       DELCHR(FORMAT("Quantity (Base)"),'=','.');
    end;

    trigger OnOpenPage();
    begin
        GLSetup.GET;

        DateFilter := FORMAT(TODAY);
        SETFILTER("Shipment Date",(DateFilter));
        DateFilter := GETFILTER("Shipment Date");
    end;

    var
        SH : Record "Sales Header";
        SL : Record "Sales Line";
        GLSetup : Record "General Ledger Setup";
        GlobalDim2Filter : Code[250];
        DateFilter : Text[30];
        ShipToName : Text[50];
        ShipToAddress : Text[50];
        TotalQuantity : Decimal;
        BarcodeText : Text[250];
        //C50005 : Codeunit "Lestur frá AGRO";

    local procedure LookUpDimFilter(Dim : Code[20];var Text : Text[250]) : Boolean;
    var
        DimVal : Record "Dimension Value";
        DimValList : Page "Dimension Value List";
    begin
        IF Dim = '' THEN
          EXIT(FALSE);
        DimValList.LOOKUPMODE(TRUE);
        DimVal.SETRANGE("Dimension Code",Dim);
        DimValList.SETTABLEVIEW(DimVal);
        IF DimValList.RUNMODAL = ACTION::LookupOK THEN BEGIN
          DimValList.GETRECORD(DimVal);
          Text := DimValList.GetSelectionFilter;
          EXIT(TRUE);
        END ELSE
          EXIT(FALSE)
    end;

    procedure SetDimFilter(DimCode : Code[250]);
    begin
        GlobalDim2Filter := DimCode;
        SETFILTER("Shortcut Dimension 2 Code",GlobalDim2Filter);
    end;
}

