pageextension 50 "Purchase Order Extension" extends "Purchase Order"
{
    layout
    {
        // Add changes to page layout here
    }

    actions
    {
        addafter("Speci&al Order")
        {
            /*
            action("Page Split Order")
            {
                CaptionML = ENU = '"Page Split Order"',
                            ISL = 'icelandicText';
                Image = Split;
                RunObject = Page "split order"
                trigger OnAction();
                begin
                end;
            }
            */
            action("Prenta á Ensku")
            {
                CaptionML = ENU = 'Prenta á Ensku',
                            ISL = 'Prenta á Ensku';
                Image = Print;

            
                trigger OnAction();
                begin 
                    tmpRec.COPY(Rec);
                    tmpRec.SETFILTER("No.","No.");
                    //RepOrderEnglish.SETTABLEVIEW(tmpRec);
                    //RepOrderEnglish.RUNMODAL;
                end;
            }
        }
    }
    
    var
        myInt : Integer;
        //RepOrderEnglish : Report "Order - English";
        tmpRec : Record "Purchase Header";
}