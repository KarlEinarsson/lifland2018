pageextension 21 "Customer Card Extension" extends "Customer Card"
{
    layout
    {
        addlast(General)
        {
            field("Social Security No.";"Social Security No.")
            {
            }
        }

        addlast(ContactDetails)
        {
            field("Reikningur með tölvupósti";"Reikningur með tölvupósti")
            {
            }
        }

        addlast(Invoicing)
        {
            field("Discount Item No.";"Discount Item No.")
            {
            }

            field("Eftir á afsláttur %";"Eftir á afsláttur %")
            {
            }
        }

        addlast(Shipping)
        {
            field("Freight Paid By";"Freight Paid By")
            {
            }
        }
    }

    actions
    {
        // Add changes to page actions here
    }
    
    var
        myInt : Integer;
}