page 51000 "Driver Role Center"
{
    // version RDN

    PageType = RoleCenter;

    layout
    {
        area(rolecenter)
        {
            part("Car numbers ";"Driver Role Center Content")
            {
                CaptionML = ENU='Car numbers ',
                            ISL='Bílnúmer (Verkefni)';
                Editable = false;
            }
        }
    }

    actions
    {
        area(processing)
        {
        }
    }
}

