page 59241 "Transport Waybills SAM"
{
    // version SAME
    /*
    CaptionML = ENU='Transport Waybills',
                ISL='Fylgibréfaskrá';
    PageType = Card;
    SourceTable = "Transport Waybill SAM";

    layout
    {
        area(content)
        {
            group(Almennt)
            {
                field("Document Type";"Document Type")
                {
                    Editable = false;
                }
                field("Ship-to Name";"Ship-to Address 2")
                {
                    Caption = 'Viðtakandi';
                    Editable = true;
                    Enabled = true;
                    QuickEntry = true;
                }
                field("Ship-to Address";"Ship-to City")
                {
                    Caption = 'Heimilisfang';
                    Editable = true;
                }
                field("Ship-to City";"Ship-to Contact")
                {
                    Caption = 'Bær';
                    Editable = true;
                }
                field("Registration No.";"Posting Date")
                {
                    Caption = 'Kennitala';
                    Editable = true;
                }
                field("Phone No.";vskm."Phone No.")
                {
                    Caption = 'Sími';
                    Editable = false;
                }
                field("Document No.";"Document No.")
                {
                }
                field("Bill-to Customer No.";"Bill-to Customer No.")
                {
                }
                field("Bill-to Name";"Bill-to Name")
                {
                }
                field("No.";"No.")
                {
                }
                field("Bill-to Contact";"Bill-to Contact")
                {
                }
                field("Ship-to Code";"Ship-to Code")
                {
                    ShowCaption = true;
                }
                field("Ship-to Name";"Ship-to Name")
                {
                }
                field("Ship-to Name 2";"Ship-to Name 2")
                {
                }
                field("Bill-to Name 2";"Bill-to Name 2")
                {
                    Editable = false;
                }
                field("Bill-to City";"Bill-to City")
                {
                }
                field("Your Reference";"Your Reference")
                {
                }
            }
        }
    }

    actions
    {
        area(reporting)
        {
            action("Print Waybill")
            {
                CaptionML = ENU='Print Transport Waybill - Lable',
                            ISL='Prenta fylgibréf';
                Image = "Report";
                Promoted = true;

                trigger OnAction();
                var
                    "SWB (gera per fyrirtæki) aðgerð" : Integer;
                begin
                    //SWB.FylgibréfInput(Rec);
                end;
            }
            action("Print Lables")
            {
                CaptionML = ENU='Print Transport Waybill - Lable',
                            ISL='Prenta Límmiða';
                Image = "Report";
                Promoted = true;

                trigger OnAction();
                var
                    "SWB (gera per fyrirtæki) aðgerð" : Integer;
                begin
                    //SWB.LímmiðaInput2(Rec);
                end;
            }
        }
    }

    trigger OnAfterGetRecord();
    begin
        CASE "Ship-to Address" OF
          0 : BEGIN
                SHrec.INIT;
                IF SHrec.GET("Document Type") THEN
                  vskm.GET(SHrec."Sell-to Customer No.");
              END;
          1 : BEGIN
                SHrec.INIT;
                SHPrec.GET(1,"Document Type");
                vskm.GET(SHPrec."Sell-to Customer No.");
                SHrec."Ship-to Name" := SHPrec."Ship-to Name";
                SHrec."Ship-to Address" := SHPrec."Ship-to Address";
                SHrec."Ship-to City" := SHPrec."Ship-to City";
              END;
        END;
    end;

    var
        SHrec : Record "Sales Invoice Header";
        SHPrec : Record "Sales Header";
        vskm : Record Customer;

    */
}

