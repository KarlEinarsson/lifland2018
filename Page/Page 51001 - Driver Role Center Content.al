page 51001 "Driver Role Center Content"
{
    // version RDN

    CardPageID = "OrderLines Per Truck";
    Editable = false;
    PageType = ListPart;
    SourceTable = "Dimension Value";
    //SourceTableView = WHERE("Dimension Code"=CONST(VERKEFNI), Code=FILTER(??-???));

    layout
    {
        area(content)
        {
            repeater("Car number (Job)")
            {
                CaptionML = ENU='Car number (Job)',
                            ISL='Bílnúmer (Verkefni)';
                field("Code";Code)
                {

                    trigger OnAssistEdit();
                    begin
                        TruckOrderLinesPage.SetDimFilter(Code);
                        TruckOrderLinesPage.RUN;
                    end;
                }
                field(Name;Name)
                {

                    trigger OnAssistEdit();
                    begin
                        TruckOrderLinesPage.SetDimFilter(Code);
                        TruckOrderLinesPage.RUN;
                    end;
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            action("Order Lines")
            {
                CaptionML = ENU='Order Lines',
                            ISL='Pantana línur';
                Image = TransferReceipt;

                trigger OnAction();
                begin
                    TruckOrderLinesPage.SetDimFilter(Code);
                    TruckOrderLinesPage.RUN;
                end;
            }
        }
    }

    var
        TruckOrderLinesPage : Page "OrderLines Per Truck";
}

