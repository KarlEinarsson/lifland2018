page 87700 "Change GL Entry"
{
    // version RDNVIDD100

    // rdn.shb = Rue de Net Reykjavík, Svava Hildur Bjarnadóttir, svava@ruedenet.is
    // 
    // No.  Date        Jira     Developer  Description
    // ---  ----        ----     ---------  -----------
    // #01  28.06.2016  LIF-356  rdn.shb    Copied from RE

    PageType = Card;
    Permissions = TableData "G/L Entry"=rimd;
    SourceTable = "G/L Entry";

    layout
    {
        area(content)
        {
            grid(General)
            {
                CaptionML = ENU='General',
                            ISL='Almennt';
                GridLayout = Columns;
                group(Control1100410016)
                {
                }
                field("G/L Account No.";"G/L Account No.")
                {
                    Editable = false;
                }
                field("Entry No.";"Entry No.")
                {
                    Editable = false;
                }
                field("Posting Date";"Posting Date")
                {
                    Editable = false;
                }
                field(Amount;Amount)
                {
                    Editable = false;
                }
            }
            group(Value)
            {
                CaptionML = ENU='Value',
                            ISL='Gildi';
                field("Global Dimension 1 Code";"Global Dimension 1 Code")
                {
                    Editable = false;
                }
                field("Global Dimension 2 Code";"Global Dimension 2 Code")
                {
                    Editable = false;
                }
                field(Description;Description)
                {
                    Editable = false;
                }
                field("External Document No.";"External Document No.")
                {
                    Editable = false;
                }
            }
            group("New values")
            {
                CaptionML = ENU='New values',
                            ISL='Ný gildi';
                field(NewDim1;NewDim1)
                {
                    CaptionClass = '1,2,1';
                    Caption = '"""Global Dimension 1 Code"""';
                    TableRelation = "Dimension Value".Code WHERE ("Global Dimension No."=CONST(1));
                }
                field(NewDim2;NewDim2)
                {
                    CaptionClass = '1,2,2';
                    Caption = '"""Global Dimension 2 Code"""';
                    TableRelation = "Dimension Value".Code WHERE ("Global Dimension No."=CONST(2));
                }
                field(NewDescription;NewDesc)
                {
                    CaptionML = ENU='Description',
                                ISL='Lýsing';
                }
                field("NewExternal Document No.";NewExtDoc)
                {
                    CaptionML = ENU='External Document No.',
                                ISL='Númer utanaðk. skjals';
                }
            }
        }
    }

    actions
    {
        area(processing)
        {
            group(Dimensions)
            {
                action("Update Values")
                {
                    CaptionML = ENU='Update Values',
                                ISL='Uppfæra gildi';
                    Image = Dimensions;
                    Promoted = true;

                    trigger OnAction();
                    begin
                        UpdateGlEntry;
                    end;
                }
            }
        }
    }

    trigger OnAfterGetCurrRecord();
    begin
        NewDesc := Description;
        NewExtDoc := "External Document No.";
        NewDim1 := "Global Dimension 1 Code";
        NewDim2 := "Global Dimension 2 Code";
    end;

    trigger OnClosePage();
    begin
        IF (Description <> NewDesc) OR
           ("External Document No." <> NewExtDoc) OR
           ("Global Dimension 1 Code" <> NewDim1) OR
           ("Global Dimension 2 Code" <> NewDim2) THEN
          IF CONFIRM(Text001) THEN
            UpdateGlEntry;
    end;

    var
        NewDesc : Text[50];
        NewExtDoc : Code[20];
        NewDim1 : Code[20];
        NewDim2 : Code[20];
        Text001 : TextConst ENU='Update record with new values?',ISL='Uppfæra færslu með nýjum gildum?';

    procedure UpdateGlEntry();
    var
        xGLEntry : Record "G/L Entry";
        ChangeLogMgmt : Codeunit "Change Log Management";
        GeneralLedgerSetup : Record "General Ledger Setup";
        RecModified : Boolean;
        RecRef : RecordRef;
        xRecRef : RecordRef;
        DimId : Integer;
        DimMgt : Codeunit DimensionManagement;
        DimensionSetIDArr : array [10] of Integer;
    begin
        RecModified := FALSE;
        IF "Global Dimension 1 Code" <> NewDim1 THEN BEGIN
          "Global Dimension 1 Code" := NewDim1;
          RecModified := TRUE;
          DimMgt.ValidateShortcutDimValues(1,"Global Dimension 1 Code","Dimension Set ID");
        END;

        IF "Global Dimension 2 Code" <> NewDim2 THEN BEGIN
          "Global Dimension 2 Code" := NewDim2;
          RecModified := TRUE;
          DimMgt.ValidateShortcutDimValues(2,"Global Dimension 2 Code","Dimension Set ID");
        END;

        IF "External Document No." <> NewExtDoc THEN BEGIN
          "External Document No." := NewExtDoc;
          RecModified := TRUE;
        END;

        IF Description <> NewDesc THEN BEGIN
          Description := NewDesc;
          RecModified := TRUE;
        END;


        IF RecModified THEN
          MODIFY(TRUE);
    end;
}

