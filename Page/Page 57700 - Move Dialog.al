page 57700 "Move Dialog"
{
    /*
    PageType = StandardDialog;
    SourceTable = Table10001303;
    SourceTableTemporary = true;

    layout
    {
        area(content)
        {
            group(Control1100410003)
            {
                field("Frá:";"Confirm Codeunit")
                {
                    CaptionML = ENU='From',
                                ISL='Frá';

                    trigger OnLookup(Text : Text) : Boolean;
                    var
                        InventoryMasksPage : Page Page10001306;
                        InventoryMasksRec : Record Table10001303;
                    begin
                        CLEAR(InventoryMasksPage);
                        InventoryMasksPage.SETTABLEVIEW(InventoryMasksRec);
                        InventoryMasksPage.SETRECORD(InventoryMasksRec);
                        IF InventoryMasksPage.RUNMODAL = ACTION::OK THEN BEGIN
                          InventoryMasksPage.GETRECORD(InventoryMasksRec);
                          "Confirm Codeunit" := InventoryMasksRec."Seq. No.";
                          Templates := InventoryMasksRec.Description;
                        END;
                    end;
                }
                field(Templates;Templates)
                {
                    CaptionML = ENU='Templates',
                                ISL='Frá';
                    Editable = false;
                }
            }
            group(Control1100410004)
            {
                field("Til:";"Report Object ID")
                {
                    CaptionML = ENU='To',
                                ISL='Til';

                    trigger OnLookup(Text : Text) : Boolean;
                    var
                        InventoryMasksPage : Page Page10001306;
                        InventoryMasksRec : Record Table10001303;
                    begin
                        CLEAR(InventoryMasksPage);
                        InventoryMasksPage.SETTABLEVIEW(InventoryMasksRec);
                        InventoryMasksPage.SETRECORD(InventoryMasksRec);
                        IF InventoryMasksPage.RUNMODAL = ACTION::OK THEN BEGIN
                          InventoryMasksPage.GETRECORD(InventoryMasksRec);
                          "Report Object ID" := InventoryMasksRec."Seq. No.";
                          "Vendor No." := InventoryMasksRec.Description;
                        END;
                    end;
                }
                field("Vendor No.";"Vendor No.")
                {
                    CaptionML = ENU='Vendor No.',
                                ISL='Til';
                    Editable = false;
                }
            }
        }
    }

    actions
    {
    }

    trigger OnClosePage();
    var
        CollFunctions : Codeunit Codeunit87400;
    begin

        IF ("Confirm Codeunit" <> 0) AND ("Report Object ID" <> 0) THEN
          IF CONFIRM('Flytja línur frá: ' + Templates + ' yfir í ' + "Vendor No." + '?') THEN
            CollFunctions.MoveBetweenStockCountingJournal("Confirm Codeunit","Report Object ID")
          ELSE
            MESSAGE('Hætt við, ekkert flutt.');
    end;

    trigger OnOpenPage();
    begin
        Rec.INSERT;
    end;

    var
        Fra : Integer;
        Til : Integer;
    
    */
}

