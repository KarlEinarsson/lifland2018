pageextension 22 "Customer List Extension" extends "Customer List"
{
    layout
    {
        // Add changes to page layout here
    }

    actions
    {
        addlast(Sales)
        {
            action("Viðskm. eftirá afslættir")
            {
                CaptionML = ENU = 'Customer Late Discount',
                            ISL = 'Viðskm. eftirá afslættir';
                Image = Discount;
                Promoted = true;
                PromotedCategory = Report;
                PromotedIsBig = true;
                RunObject = report "Account Schedule";
            
                trigger OnAction();
                begin
                end;
            }
        }
    }
    
    var
        myInt : Integer;
}