pageextension 300 "Ship-to Address Extension" extends "Ship-to Address"
{
    layout
    {
        addlast(General)
        {
            field("Sendingarkostnaður tegund";"Sendingarkostnaður tegund")
            {
            }

            field("Sendingarkostnaður";"Sendingarkostnaður")
            {
            }
        }
    }

    actions
    {
        // Add changes to page actions here
    }
    
    var
        myInt : Integer;
}