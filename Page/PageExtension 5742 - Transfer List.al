pageextension 5742 "Transfer List Extension" extends "Transfer Orders"
{
    layout
    {
        addlast(Control1)
        {
            field("Transfer-to Bin Code";"Transfer-to Bin Code")
            {
                Visible=false;
            }

            field("Transfer-to Name";"Transfer-to Name")
            {
                Visible=false;
            }
        }
    }

    actions
    {
        // Add changes to page actions here
    }
    
    var
        myInt : Integer;
}