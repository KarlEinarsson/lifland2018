report 50008 "Sýnatökumiði"
{
    // version FóðurAfgr0.00

    // rdnr.kvk= Rue de Net Reykjavík, Karl Valdimar Kristinsson, karlvaldimar@ruedenet.is
    // rdnr.ka= Rue de Net Reykjavík, Kristín Ásgeirsdóttir, kristin@ruedenet.is
    // Num Date       Jira       Developer Description
    // --- ---------- ---------- --------- -------------------
    //     2015-09-29 LIF-59     rdnr.kvk  Færði skýrsluna frá 2009 til 2015. Sleppti header section-inu þar sem það var ekki prentað
    //     2015-12-10 LIF-??     rdn.ka    Lagfærði skýrslu layout
    DefaultLayout = RDLC;
    RDLCLayout = 'Report\rdlc\Sýnatökumiði.rdlc';

    UsageCategory = ReportsAndAnalysis;

    dataset
    {
        dataitem("Sales Line";"Sales Line")
        {
            DataItemTableView = SORTING("Shortcut Dimension 2 Code","Shipment Date") WHERE(Type=FILTER(Item), "Document Type"=FILTER(Order..Invoice), "Unit of Measure Code"=FILTER('KG'), "Item Category Code"=FILTER(9000));
            column(DocumentNo;"Sales Line"."Document No.")
            {
                IncludeCaption = true;
            }
            column(DateAndTime;DateAndTime)
            {
            }
            column(SHShipToName;SH."Ship-to Name")
            {
                IncludeCaption = true;
            }
            column(SHShipToAddress;SH."Ship-to Address")
            {
                IncludeCaption = true;
            }
            column(SiloAtCustomerSide;"Sales Line"."Silo at Customer side")
            {
                //IncludeCaption = true;
            }
            column(No;"Sales Line"."No.")
            {
                IncludeCaption = true;
            }
            column(Description;"Sales Line".Description)
            {
                IncludeCaption = true;
            }
            column(Bill;"Sales Line"."Shortcut Dimension 2 Code")
            {
                IncludeCaption = true;
            }
            column(Holf;"Sales Line"."Vehicle Bin")
            {
                //IncludeCaption = true;
            }
            column(Tank;"Sales Line".Tank)
            {
                //IncludeCaption = true;
            }

            trigger OnAfterGetRecord();
            begin
                SH.SETRANGE("Document Type","Sales Line"."Document Type");
                SH.SETRANGE("No.","Sales Line"."Document No.");
                SH.FINDFIRST;

                IF "Sales Line"."Silo date and time" <> 0DT THEN
                  DateAndTime := FORMAT("Sales Line"."Silo date and time")
                ELSE
                  DateAndTime := FORMAT(TODAY);
            end;
        }
    }

    requestpage
    {

        layout
        {
        }

        actions
        {
        }
    }

    labels
    {
        label(Customer;ENU='Customer',
                       ISL='Viðskiptamaður')
        label(ItemNo;ENU='Item No.',
                     ISL='Vörunr.')
        label(Vehicle;ENU='Vehicle',
                      ISL='Bíll')
    }

    var
        SH : Record "Sales Header";
        DateAndTime : Text[30];
}

