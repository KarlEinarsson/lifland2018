report 59105 "Viðskm. eftirá afslættir"
{
    // version RDN

    // ls.þs = Landsteinar Strengur, Þórey Sigurbjörnsdóttir
    // rdn.ka = Rue de Net Reykjavik, Kristín Ásgeirsdóttir, kristin@ruedenet.net
    // 
    // Num Date       Jira       Developer Description
    // --- ---------- ---------- --------- -------------------
    // 01  09.12.2005            ls.þs     Reitir stækkaðir þar sem komu stjörnur umb. Guðbjörg
    // 02  09.06.2006            ls.sov    Set inn reiti á section skv JR06-0993, heimili, póstnr og bær
    // 03  19.09.2008            ls.gua    Ef notandi má ekki sjá eftirá afslætti má hann heldur ekki keyra skýrsluna
    // #04 05.11.2015 LIF-72     rdn.ka    Created report in 2015 and added create Credit Memo function
    // #05 05.04.2016 LIF-392    rdn.ka    Changed Credit Memo Lines
    DefaultLayout = RDLC;
    RDLCLayout = 'Report\rdlc\Viðskm. eftirá afslættir.rdlc';

    UsageCategory = ReportsAndAnalysis;

    dataset
    {
        dataitem(Customer;Customer)
        {
            RequestFilterFields = "No.","Search Name","Customer Posting Group";
            column(CustNo;Customer."No.")
            {
            }
            column(CustName;Customer.Name)
            {
            }
            column(COMPANYNAME;COMPANYNAME)
            {
            }
            column(CustomerFilter;CustFilter)
            {
            }
            column(ValueEntryFilter;ValueEntryFilter)
            {
            }
            dataitem("Value Entry";"Value Entry")
            {
                DataItemLink = "Source No."=FIELD("No."), "Posting Date"=FIELD("Date Filter"), "Global Dimension 1 Code"=FIELD("Global Dimension 1 Filter"), "Global Dimension 2 Code"=FIELD("Global Dimension 2 Filter");
                DataItemTableView = SORTING("Source Type","Source No.","Item No.","Posting Date") WHERE("Source Type"=CONST(Customer));
                RequestFilterFields = "Item No.","Inventory Posting Group","Posting Date";

                trigger OnAfterGetRecord();
                begin
                    ValueEntryBuffer.SETRANGE("Item No.", "Item No.");

                    IF "Item No." <> '' THEN BEGIN
                      IF NOT ValueEntryBuffer.FIND('-') THEN BEGIN
                        ValueEntryBuffer.INIT;
                        ValueEntryBuffer."Entry No." := NextEntryNo;
                        ValueEntryBuffer."Item No." := "Item No.";
                        ValueEntryBuffer.INSERT;

                        NextEntryNo := NextEntryNo + 1;
                      END;

                      IF Item.GET("Item No.") THEN
                        ValueEntryBuffer.Description := Item.Description;

                      ValueEntryBuffer."Invoiced Quantity" := ValueEntryBuffer."Invoiced Quantity" + "Invoiced Quantity";
                      ValueEntryBuffer."Sales Amount (Actual)" := ValueEntryBuffer."Sales Amount (Actual)" + "Sales Amount (Actual)";
                      ValueEntryBuffer."Cost Amount (Actual)" := ValueEntryBuffer."Cost Amount (Actual)" + "Cost Amount (Actual)";
                      ValueEntryBuffer."Discount Amount" := ValueEntryBuffer."Discount Amount" + "Value Entry"."Discount Amount";
                      ValueEntryBuffer."Cost Amount (Non-Invtbl.)" := ValueEntryBuffer."Cost Amount (Non-Invtbl.)" + "Cost Amount (Non-Invtbl.)";
                      ValueEntryBuffer.MODIFY;
                    END;
                end;

                trigger OnPreDataItem();
                begin
                    ValueEntryBuffer.RESET;
                    ValueEntryBuffer.DELETEALL;

                    "Value Entry".SETFILTER("Sales Amount (Actual)",'<>0'); //rdn.ka breytt úr >0

                    NextEntryNo := 1;
                end;
            }
            dataitem("Integer";"Integer")
            {
                DataItemTableView = SORTING(Number);
                column(VE_ItemNo;ValueEntryBuffer."Item No.")
                {
                }
                column(VE_Descr;ValueEntryBuffer.Description)
                {
                }
                column(VE_InvQty;-ValueEntryBuffer."Invoiced Quantity")
                {
                }
                column(UnitOfMeasure;BaseUnitOfMeasure)
                {
                }
                column(VE_SalesAmountActual;ValueEntryBuffer."Sales Amount (Actual)")
                {
                }
                column(VE_DiscAm;ValueEntryBuffer."Discount Amount")
                {
                }
                column(EAfslPros;Customer."Eftir á afsláttur %")
                {
                }
                column(AccAfsl;AccAfsl)
                {
                }

                trigger OnAfterGetRecord();
                begin
                    IF Number = 1 THEN BEGIN
                      //LineNo := 10000; //#LIF-392 -+
                      ValueEntryBuffer.FIND('-');
                    END ELSE
                      ValueEntryBuffer.NEXT;
                    
                    //#04-
                    BaseUnitOfMeasure := '';
                    Item.RESET;
                    IF Item.GET(ValueEntryBuffer."Item No.") THEN BEGIN
                      BaseUnitOfMeasure := Item."Base Unit of Measure";
                    END;
                    //#04+
                    
                    CalcAccumulatedDisc();
                    
                    TotalAccumulatedDisc := TotalAccumulatedDisc + AccAfsl; //#LIF-392 -+
                    
                    //#LIF-392 -
                    //Búa til kredit reikningslínur
                    //#04-
                    /*IF CreateCreditMemos THEN BEGIN
                      IF CurrentSalesHeaderNo <> '' THEN BEGIN
                        CLEAR(SalesLine);
                        SalesLine."Document Type" := SalesLine."Document Type"::"Credit Memo";
                        SalesLine."Document No." := CurrentSalesHeaderNo;
                        SalesLine."Line No." := LineNo;
                        SalesLine.INSERT(TRUE);
                        LineNo += 10000;
                    
                        SalesLine.VALIDATE(Type,SalesLine.Type::Item);
                        SalesLine.VALIDATE("No.",Customer."Discount Item No.");
                        SalesLine.Description := COPYSTR('('+ValueEntryBuffer."Item No." + ') ' + ValueEntryBuffer.Description,1,MAXSTRLEN(SalesLine.Description));
                        SalesLine.VALIDATE("Unit Price",ValueEntryBuffer."Sales Amount (Actual)" - ValueEntryBuffer."Discount Amount");
                        SalesLine.VALIDATE(Quantity,1);
                        SalesLine.VALIDATE("Line Discount %",100 - Customer."Eftir á afsláttur %");
                        SalesLine.MODIFY(TRUE);
                      END;
                    END;*/
                    //#04+
                    //#LIF-392 +

                end;

                trigger OnPostDataItem();
                begin
                    //#LIF-392 -
                    //Búa til kredit reikningslínur
                    //#04-
                    IF CreateCreditMemos THEN BEGIN
                      IF CurrentSalesHeaderNo <> '' THEN BEGIN
                        CLEAR(SalesLine);
                        SalesLine."Document Type" := SalesLine."Document Type"::"Credit Memo";
                        SalesLine."Document No." := CurrentSalesHeaderNo;
                        SalesLine."Line No." := 10000;
                        SalesLine.INSERT(TRUE);

                        SalesLine.VALIDATE(Type,SalesLine.Type::Item);
                        SalesLine.VALIDATE("No.",Customer."Discount Item No.");
                        SalesLine.VALIDATE("Unit Price",TotalAccumulatedDisc);
                        SalesLine.VALIDATE(Quantity,1);
                        SalesLine.MODIFY(TRUE);
                      END;
                    END;
                    //#04+
                    //#LIF-392 +
                end;

                trigger OnPreDataItem();
                begin
                    ValueEntryBuffer.RESET;
                    SETRANGE(Number, 1, ValueEntryBuffer.COUNT);
                    TotalAccumulatedDisc := 0; //#LIF-392 -+
                end;
            }

            trigger OnAfterGetRecord();
            begin
                //Búa til Sales Credit Memo Header
                //#04-
                CurrentSalesHeaderNo := '';
                SalesHeader.RESET;
                CLEAR(SalesHeader);
                IF CreateCreditMemos THEN BEGIN
                  IF Customer."No." <> '' THEN BEGIN
                    SalesHeader.INIT;
                    SalesHeader."Document Type" := SalesHeader."Document Type"::"Credit Memo";
                    SalesHeader."Sell-to Customer No." := Customer."No.";
                    IF SalesHeader.INSERT(TRUE) THEN BEGIN
                      SalesHeader.VALIDATE("Sell-to Customer No.");
                      SalesHeader."Prices Including VAT" := FALSE;
                      IF SalesHeader.MODIFY THEN;
                    END ELSE
                      ERROR('Ekki tókst að búa til kredit reikning fyrir viðskiptavin: ' + Customer."No.");
                    CurrentSalesHeaderNo := SalesHeader."No.";
                  END;
                END;
                //#04+
            end;

            trigger OnPreDataItem();
            begin
                Customer.SETFILTER("Eftir á afsláttur %",'>0');
                Customer.SETFILTER("Discount Item No.",'<>%1','');
                MESSAGE('Fjöldi viðskiptavina: ' + FORMAT(Customer.COUNT));
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                group("Valkostir:")
                {
                    field(CreateCreditMemos;CreateCreditMemos)
                    {
                        CaptionML = ENU='Create Credit Memos',
                                    ISL='Búa til kreditreikninga';
                    }
                }
            }
        }

        actions
        {
        }
    }

    labels
    {
        label(ReportHeader;ENU='Vendor - Late Discounts',
                           ISL='Viðskiptamenn - Eftirá afslættir')
        label(ItemNoLabel;ENU='Item No.',
                          ISL='Vörunúmer')
        label(DescrLabel;ENU='Description',
                         ISL='Lýsing')
        label(QtyLabel;ENU='Quantity',
                       ISL='Magn')
        label(AmountLabel;ENU='Amount',
                          ISL='Upphæð')
        label(DiscAmountLabel;ENU='Discount Amount ',
                              ISL='Afsláttar upphæð')
        label(LateDiscPrecLabel;ENU='Late Discount %',
                                ISL='Eftirá afsláttar %')
        label(LateDiscAmountLabel;ENU='Late Discount Amount',
                                  ISL='Eftirá afsláttar upphæð')
        label(CustomerFilterLabel;ENU='Customer Filter',
                                  ISL='Viðskiptamanns afmörkun')
        label(ValueEntryFilterLabel;ENU='Value Entry Filter',
                                    ISL='Virðisfærslu afmörkun')
        label(UnitOfMeasureLabel;ENU='Unit Of Measure',
                                 ISL='Mælieining')
        label(ProfitLabel;ENU='Profit',
                          ISL='Framlegð')
        label(ProfitPercLabel;ENU='Profit %',
                              ISL='Framlegðar %')
    }

    trigger OnInitReport();
    begin
        //03-
        IF (NOT UserSetup.GET(USERID)) OR ( NOT UserSetup."Má sjá eftirágefna afslætti") THEN
          ERROR('Ekki réttindi til að keyra þessa skýrslu!');
        //03+
        CreateCreditMemos := FALSE; //#04 -+
    end;

    trigger OnPreReport();
    begin
        CustFilter := Customer.GETFILTERS;
        ValueEntryFilter := "Value Entry".GETFILTERS;
    end;

    var
        Item : Record Item;
        ValueEntryBuffer : Record "Value Entry" temporary;
        ExcelBuf : Record "Excel Buffer" temporary;
        UserSetup : Record "User Setup";
        PeriodText : Text[30];
        NextEntryNo : Integer;
        PrintOnlyOnePerPage : Boolean;
        Profit : Decimal;
        ProfitPct : Decimal;
        PrintToExcel : Boolean;
        AccAfsl : Decimal;
        Text000 : TextConst ENU='Period: %1',ISL='Tímabil: %1';
        Text001 : TextConst ENU='Data',ISL='Gögn';
        Text002 : TextConst ENU='Customer/Item Sales',ISL='Viðskm. - Vörugreining';
        Text003 : TextConst ENU='Company Name',ISL='Heiti fyrirtækis';
        Text004 : TextConst ENU='Report No.',ISL='Skýrsla nr.';
        Text005 : TextConst ENU='Report Name',ISL='Skýrsluheiti';
        Text006 : TextConst ENU='User ID',ISL='Kenni notanda';
        Text007 : TextConst ENU='Date',ISL='Dagsetning';
        Text008 : TextConst ENU='Customer Filters',ISL='Afmarkanir viðskiptamanna';
        Text009 : TextConst ENU='Value Entry Filters',ISL='Afmarkanir á virðisfærslu';
        Text010 : TextConst ENU='Profit',ISL='Framlegð';
        Text011 : TextConst ENU='Profit %',ISL='Framlegðar%';
        CustCount : Integer;
        SalesHeader : Record "Sales Header";
        SalesLine : Record "Sales Line";
        CurrentSalesHeaderNo : Code[20];
        LineNo : Integer;
        ItemDescr : Text[50];
        CreateCreditMemos : Boolean;
        CustFilter : Text[250];
        ValueEntryFilter : Text[250];
        BaseUnitOfMeasure : Code[10];
        TotalAccumulatedDisc : Decimal;

    procedure CalcAccumulatedDisc();
    begin
        IF Customer."Eftir á afsláttur %" <> 0 THEN
          AccAfsl := (ValueEntryBuffer."Sales Amount (Actual)" - ValueEntryBuffer."Discount Amount")
                      * (Customer."Eftir á afsláttur %" / 100)
        ELSE
          AccAfsl := 0;
    end;

    local procedure CalcProfitPct();
    begin
        WITH ValueEntryBuffer DO BEGIN
          IF "Sales Amount (Actual)" <> 0 THEN
            ProfitPct := ROUND(100 * Profit / "Sales Amount (Actual)",0.1)
          ELSE
            ProfitPct := 0;
        END;
    end;
}

