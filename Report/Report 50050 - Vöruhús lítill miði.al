report 50050 "Vöruhús lítill miði"
{
    // version RDN

    // rdnr.ka = Rue de Net Reykjavik, Kristín Ásgeirsdóttir, kristin@ruedenet.net
    // 
    // NO   Date       Developer Jira-Issue  Description
    // -------------------------------------------------------------------------------------------------------------
    // #01  09.12.15   rdn.ka    LIF-??      Report Created

    ProcessingOnly = true;

    dataset
    {
        dataitem(Item;Item)
        {
            RequestFilterFields = "No.";

            trigger OnAfterGetRecord();
            var
                //"HillumiðiCU" : Codeunit "Vöruhús lítill miði";
            begin
                //HillumiðiCU.CreateItemLabel(Item,Qty);
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                group(Options)
                {
                    CaptionML = ENU='Options',
                                ISL='Valmöguleikar';
                    field(Qty;Qty)
                    {
                        CaptionML = ENU='Quantity',
                                    ISL='Fjöldi';
                    }
                }
            }
        }

        actions
        {
        }
    }

    labels
    {
    }

    trigger OnInitReport();
    begin
        Qty := 1;
    end;

    var
        Qty : Integer;
}

