report 59103 "Merkimiði Millifærslu pakka"
{
    // version RDN

    // rdn.kvk  = Rue de Net Reykjavik, Karl Valdimar Kristinsson, karlvaldimar@ruedenet.is
    // 
    // Num Date       Jira       Developer Description
    // --- ---------- ---------- --------- -------------------
    //     26.11.2015 LIF-56     rdn.kvk    Skýrsla flutt yfir úr 2009 yfir í 2015
    DefaultLayout = RDLC;
    RDLCLayout = 'Report\rdlc\Merkimiði Millifærslu pakka.rdlc';

    UsageCategory = ReportsAndAnalysis;

    dataset
    {
        dataitem("Integer";"Integer")
        {
            DataItemTableView = SORTING(Number);
            dataitem("Transfer Header";"Transfer Header")
            {
                DataItemTableView = SORTING("No.");
                RequestFilterFields = "No.";
                column(lina1;Lína[1])
                {
                }
                column(lina2;Lína[2])
                {
                }
                column(lina3;Lína[3])
                {
                }
                column(lina4;Lína[4])
                {
                }
                column(lina5;Lína[5])
                {
                }
                column(lina6;Lína[6])
                {
                }
                column(lina7;Lína[7])
                {
                }
                column(lina8;Lína[8])
                {
                }
                column(lina9;Lína[9])
                {
                }

                trigger OnAfterGetRecord();
                begin
                    //Reikn.GET("Transfer Header"."Transfer-to Code");
                    ShipToAddress.SETCURRENTKEY("Location Code","Bin Code");
                    ShipToAddress.SETRANGE("Location Code","Transfer-to Code");
                    ShipToAddress.SETRANGE("Bin Code","Transfer-to Bin Code");
                    IF NOT ShipToAddress.FINDFIRST THEN
                      EXIT;
                    i := i + 1;
                    //IF Reikn."Ship-to Code" <> '' THEN BEGIN
                      Lína[1] := ShipToAddress.Name;
                      IF ShipToAddress.Contact <> '' THEN
                        Lína[2] := STRSUBSTNO('BT: %1',ShipToAddress.Contact)
                      ELSE
                        Lína[2] := '';
                      Lína[3] := ShipToAddress.Address;
                      Lína[4] := ShipToAddress."Post Code" + ' ' + ShipToAddress.City;
                    //END
                    //  ELSE BEGIN
                    //    Lína[1] := Reikn."Sell-to Customer Name";
                    //    IF "Ship-to Contact" <> '' THEN
                    //      Lína[2] := STRSUBSTNO('BT: %1',Reikn."Sell-to Contact")
                    //    ELSE
                    //      Lína[2] := '';
                    //    Lína[3] := Reikn."Sell-to Address";
                    //    Lína[4] := Reikn."Sell-to Post Code" + ' ' + Reikn."Sell-to City";
                    //  END;
                    /*
                    IF Flutningsaðili.GET(ShipToAddress."Shipping Agent Code") THEN BEGIN
                      Lína[5] := 'Flutningsaðili';
                      Lína[6] := Flutningsaðili.Name;
                    END
                      ELSE BEGIN
                        Lína[5] := '';
                        Lína[6] := '';
                      END;
                    */
                    Lína[7] := STRSUBSTNO('M.Nr.: %1',"No.");
                    //Lína[8] := STRSUBSTNO('P.Nr.: %1',"Order No.");
                    IF MiðiNúmer > FjöldiMiða THEN
                      Lína[9] := ''
                    ELSE
                      Lína[9] := STRSUBSTNO('Pakki %1/%2',MiðiNúmer,FjöldiMiða);

                end;
            }

            trigger OnAfterGetRecord();
            begin
                MiðiNúmer := MiðiNúmer + 1;

                IF MiðiNúmer > FjöldiMiða + "X-miðar" THEN
                  CurrReport.BREAK;
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field("FjöldiMiða";FjöldiMiða)
                {
                    CaptionML = ENU='Number of Labels',
                                ISL='Fjöldi miða';
                }
            }
        }

        actions
        {
        }
    }

    labels
    {
    }

    var
        "FjöldiMiða" : Integer;
        "MiðiNúmer" : Integer;
        "fluttMeð" : Text[50];
        "AfMáti" : Code[10];
        i : Integer;
        ReiknNr : Code[10];
        "Lína" : array [10] of Text[90];
        ShipToAddress : Record "Ship-to Address";
        "Flutningsaðili" : Record "Shipping Agent";
        "X-miðar" : Integer;

    procedure SetjaFjolda(pakkaFjoldi : Integer;"aukamiðar" : Integer);
    begin
        FjöldiMiða := pakkaFjoldi;
        "X-miðar" := aukamiðar;
    end;
}

