report 50015 "Eftirlit með útkeyrslu Sent"
{
    // version FóðurAfgr0.00

    // rdnr.kvk= Rue de Net Reykjavík, Karl Valdimar Kristinsson, karlvaldimar@ruedenet.is
    // sk.hb  := Skýrr hf. Hrafnhildur Brynjólfsdóttir
    // 
    // 
    // Num Date       Jira       Developer Description
    // --- ---------- ---------- --------- -------------------
    // #01 2011-08-25            skyrr.hb  Bætti við dálk ship to code
    // #02 2011-10-14            sk.afj    Tók út Afhendingardags filter á Sales Invoice Line - On PreDataItem
    //     2015-09-29 LIF-59     rdnr.kvk  Færði skýrsluna frá 2009 til 2015
    // 
    // 
    // Shortcut Dimension 2 Code,Shipment Date
    // Type=FILTER(Item),Unit of Measure Code=FILTER(KG),Item Category Code=FILTER(9000)
    DefaultLayout = RDLC;
    RDLCLayout = 'Report\rdlc\Eftirlit með útkeyrslu Sent.rdlc';

    UsageCategory = ReportsAndAnalysis;

    dataset
    {
        dataitem("Sales Invoice Header";"Sales Invoice Header")
        {
            PrintOnlyIfDetail = true;
            RequestFilterFields = "Sell-to Customer No.","Ship-to Code","Shipment Date";
            column(InvoiceFilter;InvoiceFilter)
            {
            }
            dataitem("Sales Invoice Line";"Sales Invoice Line")
            {
                DataItemLink = "Document No."=FIELD("No.");
                DataItemTableView = SORTING("Document No.","Line No.");
                RequestFilterFields = "Shortcut Dimension 2 Code";
                column(DateTime;"Sales Invoice Line"."Silo date and time")
                {
                    //IncludeCaption = true;
                }
                column(DocumentNo;"Sales Invoice Line"."Document No.")
                {
                    IncludeCaption = true;
                }
                column(No;"Sales Invoice Line"."No.")
                {
                    IncludeCaption = true;
                }
                column(Description;"Sales Invoice Line".Description)
                {
                    IncludeCaption = true;
                }
                column(ShipToCode;"Sales Invoice Header"."Ship-to Code")
                {
                    IncludeCaption = true;
                }
                column(Shortcut2Dim;"Sales Invoice Line"."Shortcut Dimension 2 Code")
                {
                    IncludeCaption = true;
                }
                column(VehicleBin;"Sales Invoice Line"."Vehicle Bin")
                {
                    //IncludeCaption = true;
                }
                column(Silo;"Sales Invoice Line"."Silo at Customer side")
                {
                    //IncludeCaption = true;
                }
                column(Quantity;"Sales Invoice Line".Quantity)
                {
                    IncludeCaption = true;
                }

                trigger OnAfterGetRecord();
                begin
                    IF (Type <> Type::Item) OR ("Unit of Measure Code" <> 'KG') OR ("Item Category Code" <> '9000') THEN
                      CurrReport.SKIP;
                end;

                trigger OnPreDataItem();
                begin
                    //#02 "Sales Invoice Line".SETRANGE("Shipment Date","Sales Invoice Header"."Shipment Date");
                end;
            }

            trigger OnAfterGetRecord();
            begin
                InvoiceFilter := 'Afmörkun:' + "Sales Invoice Header".GETFILTERS + "Sales Invoice Line".GETFILTERS;
            end;
        }
    }

    requestpage
    {

        layout
        {
        }

        actions
        {
        }
    }

    labels
    {
        label(DateTimeCap;ENU='Silo Date and Time',
                          ISL='Fóður afgr.dags og tími')
        label(SendTo;ENU='Send-to Residence',
                     ISL='Sendist-til aðsetur')
        label(Vehicle;ENU='Vehicle',
                      ISL='Bíll')
    }

    var
        InvoiceFilter : Text[250];
}

