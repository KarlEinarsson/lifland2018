report 50002 "Núll millifærslur"
{
    // rdn.hs = Rue de Net Reykjavik, Hafrún Sigurðardóttir, hafrun@ruedenet.net
    // 
    // Num Date       Jira       Developer Description
    // --- ---------- ---------- --------- -------------------
    //     22.3.17    LIF-544    rdn.hs    Búa til skýrslu sem sýnir millifærslulínur sem eru 0.
    DefaultLayout = RDLC;
    RDLCLayout = 'Report\rdlc\Núll millifærslur.rdlc';
    
    UsageCategory = ReportsAndAnalysis;

    dataset
    {
        dataitem("Transfer Shipment Header";"Transfer Shipment Header")
        {
            RequestFilterFields = "No.","Transfer Order Date","Transfer-from Code","Transfer-to Code";
            column(COMPANYNAME;COMPANYNAME)
            {
            }
            column(USERID;USERID)
            {
            }
            column(PageLabel;PageLabel)
            {
            }
            column(TransferShipmentHeaderNo;"Transfer Shipment Header"."No.")
            {
            }
            column("NullMillifærslur";NullMillifærslur)
            {
            }
            dataitem("Transfer Shipment Line";"Transfer Shipment Line")
            {
                column(DocumentNo_TransferShipmentLine;"Transfer Shipment Line"."Document No.")
                {
                    IncludeCaption = true;
                }
                column(ItemNo_TransferShipmentLine;"Transfer Shipment Line"."Item No.")
                {
                    IncludeCaption = true;
                }
                column(Quantity_TransferShipmentLine;"Transfer Shipment Line".Quantity)
                {
                    IncludeCaption = true;
                }
                column(UnitofMeasure_TransferShipmentLine;"Transfer Shipment Line"."Unit of Measure")
                {
                    IncludeCaption = true;
                }
                column(Description_TransferShipmentLine;"Transfer Shipment Line".Description)
                {
                    IncludeCaption = true;
                }
                column(ShortcutDimension1Code_TransferShipmentLine;"Transfer Shipment Line"."Shortcut Dimension 1 Code")
                {
                    IncludeCaption = true;
                }
                column(GenProdPostingGroup_TransferShipmentLine;"Transfer Shipment Line"."Gen. Prod. Posting Group")
                {
                    IncludeCaption = true;
                }
                column(TransferfromCode;"Transfer Shipment Line"."Transfer-from Code")
                {
                    IncludeCaption = true;
                }
                column(TransferToCode;"Transfer Shipment Line"."Transfer-to Code")
                {
                    IncludeCaption = true;
                }

                trigger OnAfterGetRecord();
                begin
                    "Transfer Shipment Line".SETFILTER("Transfer Shipment Line"."Document No.", "Transfer Shipment Header"."No.");
                    "Transfer Shipment Line".SETRANGE("Transfer Shipment Line".Quantity, 0);
                end;
            }
        }
    }

    requestpage
    {

        layout
        {
        }

        actions
        {
        }
    }

    labels
    {
    }

    var
        DateFrom : Date;
        DateTo : Date;
        PageLabel : TextConst ENU='Page nr.',ISL='Bls nr.';
        "NullMillifærslur" : TextConst ENU='<Millifærslur sem innihalda núll færslur>',ISL='Millifærslur sem innihalda 0 í magn';
        DateFromText : Text[20];
        DateToText : Text[20];
}

