report 50010 "Innkaup - Verðupplýsingar"
{
    // version RDN

    // rdn.ith = Rue de Net Reykjavik, Ingvi Þór Hermannsson, ingvi@ruedenet.is
    // 
    // Num Date       Jira       Developer Description
    // --- ---------- ---------- --------- -------------------
    //     2017-07-17 LIF-558    rdn.ith   Skýrsla sem birtir verðupplýsingar vara eftir að innlend innkaup hafa verið bókuð
    DefaultLayout = RDLC;
    RDLCLayout = 'Report\rdlc\Innkaup - Verðupplýsingar.rdlc';

    UsageCategory = ReportsAndAnalysis;

    dataset
    {
        dataitem("Purch. Inv. Header";"Purch. Inv. Header")
        {
            column(PurchInvHeader_No;"Purch. Inv. Header"."No.")
            {
            }
            column(PurchInvHeader_BuyfromVendorName;"Purch. Inv. Header"."Buy-from Vendor Name")
            {
            }
            column(PurchInvHeader_BuyfromAddress;"Purch. Inv. Header"."Buy-from Address")
            {
            }
            column(PurchInvHeader_BuyfromCity;"Purch. Inv. Header"."Buy-from City")
            {
            }
            column(PurchInvHeader_BuyfromPostCode;"Purch. Inv. Header"."Buy-from Post Code")
            {
            }
            column(PurchInvHeader_BuyFromCityPostCode;PurchInvHeader_BuyFromCityPostCode)
            {
            }
            column(PurchInvHeader_BuyfromCountry;BuyFromCountry)
            {
            }
            column(PayToVenNoCaption;PaytoVenNoCaptionLbl)
            {
            }
            column(PurchInvHeader_PaytoVendorNo;"Purch. Inv. Header"."Pay-to Vendor No.")
            {
            }
            column(DocDateCaption;DocDateCaptionLbl)
            {
            }
            column(PurchInvHeader_DocumentDate;"Purch. Inv. Header"."Document Date")
            {
            }
            column(ShipmentNoCaption;ShipmentNoCaptionLbl)
            {
            }
            column(NoCaption;NoCaptionLbl)
            {
            }
            column(DescrCaption;DescrCaptionLbl)
            {
            }
            column(OldPriceCaption;OldPriceCaptionLbl)
            {
            }
            column(GrossProfitBeforeCaption;GrossProfitBeforeCaptionLbl)
            {
            }
            column(NewPriceCaption;NewPriceCaptionLbl)
            {
            }
            column(GrossProfitCaption;GrossProfitCaptionLbl)
            {
            }
            column(SellingPriceCaption;SellingPriceCaptionLbl)
            {
            }
            column(SellingPriceVATCaption;SellingPriceVATCaptionLbl)
            {
            }
            column(HeaderInfo;HeaderInfoText)
            {
            }
            column(PageNum;PageNumText)
            {
            }
            column(CompanyInfo_Name;"Company Information".Name)
            {
            }
            column(CompanyInfo_Address;"Company Information".Address)
            {
            }
            column(CompanyInfo_PostCode;"Company Information"."Post Code")
            {
            }
            column(CompanyInfo_City;"Company Information".City)
            {
            }
            column(CompanyInfo_PostCodeCity;CompanyInfo_PostCodeCity)
            {
            }
            column(PhoneNoCaption;PhoneNoCaptionLbl)
            {
            }
            column(CompanyInfo_PhoneNo;"Company Information"."Phone No.")
            {
            }
            column(HomePageCaption;HomePageCaptionLbl)
            {
            }
            column(CompanyInfo_HomePage;"Company Information"."Home Page")
            {
            }
            column(EmailCaption;EmailCaptionLbl)
            {
            }
            column(CompanyInfo_Email;"Company Information"."E-Mail")
            {
            }
            column(VATRegNoCaption;VATRegNoCaptionLbl)
            {
            }
            column(CompanyInfo_VATRegNo;"Company Information"."VAT Registration No.")
            {
            }
            column(GiroNoCaption;GiroNoCaptionLbl)
            {
            }
            column(CompanyInfo_GiroNo;"Company Information"."Giro No.")
            {
            }
            column(BankNameCaption;BankNameCaptionLbl)
            {
            }
            column(CompanyInfo_BankName;"Company Information"."Bank Name")
            {
            }
            column(AccNoCaption;AccNoCaptionLbl)
            {
            }
            column(CompanyInfo_BankAccountNo;"Company Information"."Bank Account No.")
            {
            }
            dataitem("Purch. Inv. Line";"Purch. Inv. Line")
            {
                DataItemLink = "Document No."=FIELD("No.");
                DataItemTableView = WHERE(Type=FILTER(Item));
                column(PurchInvLine_No;"Purch. Inv. Line"."No.")
                {
                }
                column(PurchInvLine_Description;"Purch. Inv. Line".Description)
                {
                }
                column(OldPrice;OldPrice)
                {
                }
                column(NewPrice;NewPrice)
                {
                }
                column(GrossProfit;GrossProfit)
                {
                }
                column(SellingPrice;SellingPrice)
                {
                }
                column(GrossProfitBefore;GrossProfitBefore)
                {
                }
                column(SellingPriceIncVAT;SellingPriceIncVAT)
                {
                }

                trigger OnAfterGetRecord();
                var
                    Item : Record Item;
                    SalesPrice : Record "Sales Price";
                    PurchasePrice : Record "Purchase Price";
                    OldPriceDate : Date;
                    NewPriceRec : Record "Purchase Price";
                begin
                    
                    OldPrice := 0;
                    GrossProfitBefore := 0;
                    NewPrice := 0;
                    GrossProfit := 0;
                    SellingPrice := 0;
                    SellingPriceIncVAT := 0;
                    
                    Item.RESET;
                    Item.SETRANGE("No.", "Purch. Inv. Line"."No.");
                    
                    NewPriceRec.RESET;
                    
                    // Finna nýjasta innkaupsverð
                    PurchasePrice.RESET;
                    PurchasePrice.SETRANGE("Item No.", "Purch. Inv. Line"."No.");
                    PurchasePrice.SETFILTER("Currency Code", '%1|%2', '', 'ISK');
                    PurchasePrice.SETCURRENTKEY("Starting Date");
                    PurchasePrice.ASCENDING(FALSE);
                    // Finnum innkaupsverð með nýjustu dagsetningu (eða eina innkaupsverðið ef svo á við)
                    IF PurchasePrice.FINDFIRST THEN BEGIN
                      NewPrice := PurchasePrice."Direct Unit Cost";
                      NewPriceRec := PurchasePrice;
                    END
                    // Ef ekkert innkaupsverð finnst reynum við að finna það í Item töflunni
                    ELSE IF Item.FINDFIRST THEN
                      NewPrice := Item."Last Direct Cost";
                    
                    // Finnum næstnýjasta innkaupsverð
                    IF PurchasePrice.FINDSET THEN REPEAT
                      IF FORMAT(PurchasePrice) <> FORMAT(NewPriceRec) THEN
                        OldPrice := PurchasePrice."Direct Unit Cost";
                    UNTIL (PurchasePrice.NEXT = 0) OR (OldPrice <> 0);
                    
                    // Finnum söluverð án og með VSK
                    IF Item.FINDFIRST THEN BEGIN
                      SellingPrice := Item."Unit Price";
                      //SellingPriceIncVAT := Item."Unit Price Including VAT"; Extended fall                     
                    END;
                    
                    // Finnum framlegð fyrir og eftir
                    IF SellingPrice <> 0 THEN BEGIN
                      GrossProfit := (SellingPrice - NewPrice) / SellingPrice * 100;
                      IF OldPrice <> 0 THEN
                        GrossProfitBefore := (SellingPrice - OldPrice) / SellingPrice * 100;
                    END;
                    
                    
                    /////////////////////////////////////////////////////////////////////////////////////
                    /////////////////////////////////////////////////////////////////////////////////////
                    // Upprunaleg útfærsla, mun flóknari reikningar en ætlast var til
                    // Held þessu hér þangað til ég fæ staðfest að skýrslan sé í lagi eins og hún er núna
                    /*
                    // Ef það er bara eitt innkaupsverð setjum við það strax sem nýtt verð
                    IF PurchasePrice.COUNT = 1 THEN BEGIN
                      NewPrice := PurchasePrice."Direct Unit Cost"
                    // Annars finnum við nýjasta verð miðað við upphafsdagsetningu
                    END
                    ELSE IF PurchasePrice.FINDSET THEN REPEAT
                      IF PurchasePrice."Starting Date" > PurchaseDate THEN BEGIN
                        PurchaseDate := PurchasePrice."Starting Date";
                        NewPrice := PurchasePrice."Direct Unit Cost";
                      END;
                    UNTIL PurchasePrice.NEXT = 0;
                    // Ef ekkert verð finnst reynum við að finna síðasta kostnaðarverð í Item töflunni
                    IF NewPrice = 0 THEN BEGIN
                      IF Item.FINDFIRST THEN
                        NewPrice := Item."Last Direct Cost";
                    END;
                    
                    
                    
                    // Finna næstnýjasta innkaupsverð
                    PurchasePrice.SETCURRENTKEY("Starting Date");
                    PurchasePrice.ASCENDING(FALSE);
                    IF PurchasePrice.FINDSET THEN REPEAT
                      IF (PurchasePrice."Direct Unit Cost" <> NewPrice) AND (PurchasePrice."Currency Code" = '') THEN BEGIN
                        OldPrice := PurchasePrice."Direct Unit Cost";
                        OldPriceDate := PurchasePrice."Ending Date";
                      END;
                    UNTIL ((PurchasePrice.NEXT = 0) OR (OldPrice <> 0));
                    
                    // Finnum nýjasta söluverð og framlegð
                    SalesDate := 0D;
                    SalesPrice.RESET;
                    SalesPrice.SETRANGE("Item No.", "Purch. Inv. Line"."No.");
                    SalesPrice.SETFILTER("Sales Type", '<>%1', SalesPrice."Sales Type"::Customer);
                    IF SalesPrice.FINDSET THEN REPEAT
                      IF SalesPrice."Starting Date" > SalesDate THEN
                        SalesDate := SalesPrice."Starting Date";
                    UNTIL SalesPrice.NEXT = 0;
                    
                    SalesPrice.SETRANGE("Starting Date", SalesDate);
                    SalesPrice.SETCURRENTKEY("Starting Date");
                    SalesPrice.ASCENDING(FALSE);
                    IF SalesPrice.FINDFIRST THEN BEGIN
                      SellingPrice := SalesPrice."Unit Price";
                      SellingPriceIncVAT := SalesPrice."Unit Price Including VAT";
                      GrossProfit := (SellingPrice - NewPrice) / SellingPrice * 100;
                    END;
                    
                    // Ef ekkert söluverð er í Sales Price töflunni notum við verð úr Item töflunni
                    IF (SellingPrice = 0) OR (SellingPriceIncVAT = 0) THEN BEGIN
                      IF Item.FINDFIRST THEN BEGIN
                        SellingPrice := Item."Unit Price";
                        SellingPriceIncVat := Item."Unit Price Including VAT";
                        GrossProfit := (SellingPrice - NewPrice) / SellingPrice * 100;
                      END;
                    END;
                    
                    // Finnum framlegð áður út frá eldra verði
                    IF OldPrice <> 0 THEN BEGIN
                      SalesPrice.RESET;
                      SalesPrice.SETRANGE("Item No.", "Purch. Inv. Line"."No.");
                      SalesPrice.SETFILTER("Sales Type", '<>%1', SalesPrice."Sales Type"::Customer);
                      IF SalesPrice.COUNT = 1 THEN
                        GrossProfitBefore := (SellingPrice - OldPrice) / SellingPrice * 100
                      ELSE
                        IF OldPriceDate <> 0D THEN BEGIN
                          IF SalesPrice.FINDSET THEN REPEAT
                            IF (SalesPrice."Starting Date" < OldPriceDate) AND (SalesPrice."Ending Date" > OldPriceDate) THEN
                              GrossProfitBefore := (SalesPrice."Unit Price" - OldPrice) / SalesPrice."Unit Price" * 100;
                          UNTIL (SalesPrice.NEXT = 0) OR (GrossProfitBefore <> 0);
                        END
                      ELSE
                        GrossProfitBefore := (SellingPrice - OldPrice) / SellingPrice * 100;
                    END;
                    
                    */
                    
                    
                    
                    
                    
                    
                    
                    

                end;
            }

            trigger OnAfterGetRecord();
            var
                CountryRegion : Record "Country/Region";
            begin

                "Company Information".GET();

                // Get Country based on Country Code (if any)
                IF CountryRegion.GET("Purch. Inv. Header"."Buy-from Country/Region Code") THEN
                  BuyFromCountry := CountryRegion.Name;

                PurchInvHeader_BuyFromCityPostCode := ("Purch. Inv. Header"."Buy-from City" + ', ' + "Purch. Inv. Header"."Buy-from Post Code");
                CompanyInfo_PostCodeCity := ("Company Information"."Post Code" + ' ' + "Company Information".City);
            end;
        }
    }

    requestpage
    {

        layout
        {
        }

        actions
        {
        }
    }

    labels
    {
    }

    var
        HeaderInfoText : TextConst ENU='Purchase - Price Information',ISL='Innkaup - Verðupplýsingar';
        PageNumText : TextConst ENU='Page 1',ISL='Bls. 1';
        PhoneNoCaptionLbl : TextConst ENU='Phone No.',ISL='Sími';
        HomePageCaptionLbl : TextConst ENU='Home Page',ISL='Heimasíða';
        VATRegNoCaptionLbl : TextConst ENU='VAT Registration No.',ISL='VSK-númer';
        GiroNoCaptionLbl : TextConst ENU='Giro No.',ISL='Gírónr.';
        BankNameCaptionLbl : TextConst ENU='Bank',ISL='Banki';
        AccNoCaptionLbl : TextConst ENU='Account No.',ISL='Reikningur nr.';
        ShipmentNoCaptionLbl : TextConst ENU='Order No.',ISL='Pöntunarnúmer';
        PaytoAddrCaptionLbl : TextConst ENU='Pay-to Address',ISL='Borgunaraðsetur';
        DocDateCaptionLbl : TextConst ENU='Document Date',ISL='Dags. fylgiskjals';
        PageCaptionLbl : TextConst ENU='Page',ISL='Bls.';
        DescrCaptionLbl : TextConst ENU='Description',ISL='Lýsing';
        PaytoVenNoCaptionLbl : TextConst ENU='Pay-to Vendor No.',ISL='Greiðist lánardr. nr.';
        EmailCaptionLbl : TextConst ENU='E-Mail',ISL='Tölvupóstur';
        NoCaptionLbl : TextConst ENU='No.',ISL='Nr.';
        OldPriceCaptionLbl : TextConst ENU='Old Price',ISL='Eldra Verð';
        NewPriceCaptionLbl : TextConst ENU='New Price',ISL='Nýtt verð';
        GrossProfitCaptionLbl : TextConst ENU='Gross Profit %',ISL='Framlegð nú %';
        SellingPriceCaptionLbl : TextConst ENU='Selling Price',ISL='Söluverð';
        BuyFromCountry : Text[50];
        OldPrice : Decimal;
        NewPrice : Decimal;
        SellingPrice : Decimal;
        "Company Information" : Record "Company Information";
        GrossProfit : Decimal;
        PurchInvHeader_BuyFromCityPostCode : Text;
        CompanyInfo_PostCodeCity : Text;
        GrossProfitBeforeCaptionLbl : TextConst ENU='Gross Profit Before %',ISL='Framlegð áður %';
        SellingPriceVATCaptionLbl : TextConst ENU='Selling Price inc. VAT',ISL='Söluv. m. vsk';
        GrossProfitBefore : Decimal;
        SellingPriceIncVAT : Decimal;
}

