report 59242 "SSCC barcode"
{
    // version SAME

    // rdn.ke  = Rue de Net Reykjavik, Karl Einarsson, karl@ruedenet.is
    // 
    // Num Date       Jira       Developer Description
    // --- -
    // 03.07.17                  rdn.ke    ZPL printer for Transport Waybill

    CaptionML = ENU='SSCC barcode',
                ISL='SSCC strikamerki';
    ProcessingOnly = true;

    dataset
    {
        /*
        dataitem("Transport Waybill SAM";"Transport Waybill SAM")
        {
            dataitem("SSCC Ref.";"SSCC Ref.")
            {
                DataItemLink = "Document No."=FIELD("Document No.");
                DataItemTableView = SORTING("Document No.");
                dataitem(hillumidi;"Integer")
                {
                    DataItemTableView = SORTING(Number);
                    MaxIteration = 1;
                }
            }

            trigger OnAfterGetRecord();
            begin
                CompanyInformation.GET;
                FromAddr := CompanyInformation."Post Code" + ' ' + CompanyInformation.City;
                FromStr := CompanyInformation.Address;
                ToAddr := "Ship-to Post Code" + ' ' + "Ship-to City";
                ToStr := "Ship-to Address";
                shiptoName := "Ship-to Name";

                IF "Shipping Location" <> '' THEN
                  IF Location.GET("Shipping Location") THEN BEGIN
                    IF (Location."Post Code" + ' ' + Location.City) <> '' THEN
                      FromAddr := Location."Post Code" + ' ' + Location.City;
                    IF Location.Address <> '' THEN
                      FromStr := Location.Address;
                  END;

                IF "Ship-To Location" <> '' THEN
                  IF Location.GET("Ship-To Location") THEN BEGIN
                    IF (Location."Post Code" + ' ' + Location.City) <> '' THEN
                      ToAddr := Location."Post Code" + ' ' + Location.City;
                    IF Location.Address <> '' THEN
                      ToStr := Location.Address;
                  END;

                IF "Document Type" = 5 THEN BEGIN
                  FromStr := "Bill-to Name";
                END;

                SSCCPrint("Document No.");
            end;
        }
        */
    }
    /*
    requestpage
    {

        layout
        {
        }

        actions
        {
        }
    }

    labels
    {
    }

    trigger OnPostReport();
    var
        FSO : Automation "'{420B2830-E718-11CF-893D-00A0C9054228}' 1.0:'{0D43FE01-F093-11CF-8940-00A0C9054228}':''{420B2830-E718-11CF-893D-00A0C9054228}' 1.0'.FileSystemObject";
    begin
    end;

    var
        ClientFileHelper : DotNet "'mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
        FSO : Automation "'{420B2830-E718-11CF-893D-00A0C9054228}' 1.0:'{0D43FE01-F093-11CF-8940-00A0C9054228}':''{420B2830-E718-11CF-893D-00A0C9054228}' 1.0'.FileSystemObject";
        wSHShell : Automation "'{F935DC20-1CF0-11D0-ADB9-00C04FD58A0B}' 1.0:'{72C24DD5-D70A-438B-8A42-98424B88AFB8}':''{F935DC20-1CF0-11D0-ADB9-00C04FD58A0B}' 1.0'.WshShell";
        "------" : Integer;
        TotalQuantity : Integer;
        PrinterLocation2 : Label '\\RIFJARN\GK420d';
        TransportWaybillSAM : Record "Transport Waybill SAM";
        Location : Record Location;
        CompanyInformation : Record "Company Information";
        FromAddr : Text[80];
        FromStr : Text[80];
        ToAddr : Text[80];
        ToStr : Text[80];
        shiptoName : Text;
        PrinterLocation : TextConst ENU='\\VORUHLIFLAND-HP\SamskipFylgibref',ISL='\\VORUHLIFLAND-HP\SamskipFylgibref';

    procedure ZplPrint(Quantity : Integer;SSCCRef : Record "SSCC Ref.");
    var
        InStream : InStream;
        ZPLTemplate : Text;
        ReadyToPrintTemplate : BigText;
        FileName : File;
        OutStream : OutStream;
        wSHShell : Automation "'{F935DC20-1CF0-11D0-ADB9-00C04FD58A0B}' 1.0:'{72C24DD5-D70A-438B-8A42-98424B88AFB8}':''{F935DC20-1CF0-11D0-ADB9-00C04FD58A0B}' 1.0'.WshShell";
        TempFile : Text[250];
        TempfileMgt : Codeunit "File Management";
        WshMode : Variant;
        WaitForEndOfCommand : Variant;
        Output : BigText;
        Cmd : Label 'print \\RIFJARN\GK420d';
        exec : Label 'C:\Windows\system32\cmd.exe';
    begin
        ZPLTemplate := Label3();

        // Interchangeable values we want to put on our label, so many options.
        ZPLTemplate := TextReplace(ZPLTemplate,'#X',SSCCRef."Document No.");
        ZPLTemplate := TextReplace(ZPLTemplate,'#Y',"Transport Waybill SAM"."Source Document");
        ZPLTemplate := TextReplace(ZPLTemplate,'#A',CompanyInformation.Name);
        ZPLTemplate := TextReplace(ZPLTemplate,'#B',FromAddr);
        ZPLTemplate := TextReplace(ZPLTemplate,'#C',shiptoName);
        ZPLTemplate := TextReplace(ZPLTemplate,'#D',ToAddr);
        ZPLTemplate := TextReplace(ZPLTemplate,'#E',ToStr);
        ZPLTemplate := TextReplace(ZPLTemplate,'#F',FORMAT(Quantity));
        ZPLTemplate := TextReplace(ZPLTemplate,'#G',FORMAT(TotalQuantity));
        ZPLTemplate := TextReplace(ZPLTemplate,'#L',SSCCRef."SSCC Reference");


        // We store our finished ZPL code in a temp file
        TempFile := TempfileMgt.ServerTempFileName('txt');
        ReadyToPrintTemplate.ADDTEXT(ZPLTemplate);

        IF FileName.CREATE(TempFile) THEN BEGIN
          FileName.CREATEOUTSTREAM(OutStream);
          IF ReadyToPrintTemplate.WRITE(OutStream) THEN BEGIN
            FileName.CLOSE;
            WshMode := 1;
            WaitForEndOfCommand := TRUE;
            IF ISCLEAR(wSHShell) THEN BEGIN
              CREATE(wSHShell,TRUE,TRUE);
              ClientFileHelper.Copy(TempFile,PrinterLocation,TRUE);
              CLEAR(wSHShell);
            END;

            IF EXISTS(TempFile) THEN
              ERASE(TempFile)
            ELSE ERROR('Could not delete file ' + TempFile);
          END ELSE ERROR('Could not create file ' + TempFile);
        END ELSE ERROR('Could not write to file ' + TempFile);
    end;

    procedure SSCCPrint("Document No." : Code[20]);
    var
        Quantity : Integer;
        SSCCRef : Record "SSCC Ref.";
    begin
        IF TransportWaybillSAM.GET("Document No.") THEN
        SSCCRef.SETCURRENTKEY("Document No.");
        SSCCRef.SETRANGE("Document No.","Document No.");

        TotalQuantity := (SSCCRef.COUNT);
        Quantity := 0;
        IF SSCCRef.FINDSET THEN BEGIN
          REPEAT
            Quantity := Quantity + 1;
            ZplPrint(Quantity,SSCCRef);
          UNTIL SSCCRef.NEXT = 0;
        END;
    end;

    procedure TextReplace(InputTxt : Text[1024];FindTxt : Text[1024];ReplaceTxt : Text[1024]) ReturnString : Text[1024];
    var
        InputLength : Integer;
        WhatLength : Integer;
        FindFirst : Integer;
    begin

        WHILE STRPOS(InputTxt,FindTxt) > 0 DO
          InputTxt := DELSTR(InputTxt,STRPOS(InputTxt,FindTxt)) + ReplaceTxt + COPYSTR(InputTxt,STRPOS(InputTxt,FindTxt) + STRLEN(FindTxt));
        ReturnString := InputTxt;
    end;

    local procedure GetLabelSetup(LabelNo : Integer) : Text;
    begin
        CASE LabelNo OF
          0 : EXIT(Label1);
          1 : EXIT(Label2);
          2 : EXIT(Label3);
        END;
    end;

    local procedure Label1() ReturnValue : Text;
    var
        CR : Char;
        LF : Char;
        CRLF : Text;
    begin
        CR := 13;
        LF := 10;
        CRLF := FORMAT(CR) + FORMAT(LF);

        ReturnValue :=
        '^XA'+CRLF+
        '^CI13'+CRLF+

        '^CFA,45'+CRLF+
        '^FO40,40^FH^FDFylgibréf #X (#Y)^FS'+CRLF+

        '^CFA,45'+CRLF+
        '^FO40,120^FDSendandi.:^FS'+CRLF+
        '^FO350,120^FD#A^FS'+CRLF+
        '^FO350,170^FD#B^FS'+CRLF+

        '^FO20,220^FD_______________________________^FS'+CRLF+

        'CFA,45'+CRLF+
        '^FO40,300^FD#C^FS'+CRLF+
        '^FO40,360^FDMóttakandi.:^FS'+CRLF+
        '^FO410,360^FD#D^FS '+CRLF+
        '^FO410,410^FD#E^FS'+CRLF+

        '^FO20,460^FD_______________________________^FS'+CRLF+

        '^CFA,45'+CRLF+
        '^FO40,550^FH^FDMiði #F/#G^FS'+CRLF+

        '^FWI'+CRLF+
        '^FO20,1060^BY4'+CRLF+
        '^BC,100,Y,N,N,A^FD#L^FS'+CRLF+
        '^X'+CRLF+
        '^XZ';
    end;

    local procedure Label2() ReturnValue : Text;
    var
        CR : Char;
        LF : Char;
        CRLF : Text;
    begin
        CR := 13;
        LF := 10;
        CRLF := FORMAT(CR) + FORMAT(LF);

        ReturnValue :=
        '^XA^FWR'+CRLF+
        '^CI13'+CRLF+

        '^CFA,45'+CRLF+
        '^FO620,40^FH^FDFylgibréf  #X (#Y)^FS'+CRLF+

        '^CFA,45'+CRLF+
        '^FO560,40^FDSendandi.:^FS'+CRLF+
        '^FO560,350^FD#A^FS'+CRLF+
        '^FO510,350^FD#B^FS'+CRLF+

        '^FO470,40^FD_______________________________^FS'+CRLF+

        'CFA,45'+CRLF+
        '^FO400,40^FDMóttakandi.:^FS'+CRLF+
        '^FO400,420^FD#D^FS'+CRLF+
        '^FO340,420^FD#E^FS'+CRLF+

        '^FO240,40^FD_______________________________^FS'+CRLF+

        '^CFA,45'+CRLF+
        '^FO170,40^FH^FDMiði #F/#G^FS'+CRLF+

        '^FWI'+CRLF+
        '^FO25,1050^AD^BY3'+CRLF+
        '^BC,110,Y,N,N'+CRLF+
        '^MD10'+CRLF+
        '^FD#L^FS'+CRLF+

        '^X'+CRLF+
        '^XZ';
    end;

    local procedure Label3() ReturnValue : Text;
    var
        CR : Char;
        LF : Char;
        CRLF : Text;
    begin
        CR := 13;
        LF := 10;
        CRLF := FORMAT(CR) + FORMAT(LF);

        ReturnValue :=
        '^XA^FWR'+CRLF+
        '^CI13'+CRLF+

        '^CFA,45'+CRLF+
        '^FO620,50^FH^FDFylgibréf  #X (#Y)^FS'+CRLF+

        '^CFA,45'+CRLF+
        '^FO560,50^FDSendandi.:^FS'+CRLF+
        '^FO560,360^FD#A^FS'+CRLF+
        '^FO510,360^FD#B^FS'+CRLF+

        '^FO470,40^FD______________________________^FS'+CRLF+
        '^FO470,44^FD______________________________^FS'+CRLF+

        'CFA,45'+CRLF+
        '^FO400,50^FDMóttakandi.:^FS'+CRLF+
        '^FO400,420^FD#C^FS'+CRLF+
        '^FO360,420^FD#E^FS'+CRLF+
        '^FO320,420^FD#D^FS'+CRLF+

        '^FO280,40^FD______________________________^FS'+CRLF+
        '^FO280,44^FD______________________________^FS'+CRLF+

        '^CFA,45'+CRLF+
        '^FO150,60^FH^FDMiði #F/#G^FS'+CRLF+

        '^FWI'+CRLF+
        '^FO120,1060^BY4'+CRLF+
        '^BC,100,Y,N,N,A^FD#L^FS'+CRLF+
        '^X'+CRLF+
        '^XZ';
    end;   
    */
}
