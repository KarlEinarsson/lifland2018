report 59104 "Birgðir og Sala"
{
    // version RDN

    // rdn.ka = Rue de Net Reykjavik, Kristín Ásgeirsdóttir, kristin@ruedenet.net
    // 
    // Num Date       Jira       Developer Description
    // --- ---------- ---------- --------- -------------------
    // #01 03.06.2013 JR13-2454  ad.afj    Added Reorder Point and ReorderpointAcchieved
    // #02 11.11.2015 LIF-84     rdn.ka    Created/Moved report to NAV 2015
    // #03 04.01.2016 LIF-146    rdn.ka    Set Property Save Values to Yes in Request Page
    DefaultLayout = RDLC;
    RDLCLayout = 'Report\rdlc\Birgðir og Sala.rdlc';

    UsageCategory = ReportsAndAnalysis;
    CaptionML = ENU='Inventory And Sales',
                ISL='Birgðir og sala';

    dataset
    {
        dataitem(Item;Item)
        {
            column(Item_No;Item."No.")
            {
                IncludeCaption = true;
            }
            column(Item_Descr;Item.Description)
            {
                IncludeCaption = true;
            }
            column(Item_Inventory;Item.Inventory)
            {
                IncludeCaption = true;
            }
            column(Sala1;"Sala 1")
            {
            }
            column(Sala2;"Sala 2")
            {
            }
            column(Sala3;"Sala 3")
            {
            }
            column(Sala4;"Sala 4")
            {
            }
            column(FromDate1;"1 frá dagsetning")
            {
            }
            column(ToDate1;"1 til dagsetning")
            {
            }
            column(FromDate2;"2 frá dagsetning")
            {
            }
            column(ToDate2;"2 til dagsetning")
            {
            }
            column(FromDate3;"3 frá dagsetning")
            {
            }
            column(ToDate3;"3 til dagsetning")
            {
            }
            column(FromDate4;"4 frá dagsetning")
            {
            }
            column(ToDate4;"4 til dagsetning")
            {
            }
            column(Item_ReorderPoint;Item."Reorder Point")
            {
                IncludeCaption = true;
            }
            column(ReorderpointAchieved;ReorderpointAchieved)
            {
            }
            column(CompanyName;COMPANYNAME)
            {
            }

            trigger OnAfterGetRecord();
            begin
                //Item2.Copyfilters(item);

                ILE.RESET;
                ILE.SETCURRENTKEY("Item No.","Entry Type","Variant Code","Drop Shipment","Location Code","Posting Date");
                ILE.SETRANGE("Entry Type",ILE."Entry Type"::Sale);
                ILE.SETRANGE("Item No.","No.");
                IF GETFILTER("Location Filter") <> '' THEN
                  ILE.SETRANGE("Location Code",GETFILTER("Location Filter"));

                IF ("1 frá dagsetning" <> 0D) AND ("1 til dagsetning" <> 0D) THEN BEGIN
                  ILE.SETRANGE("Posting Date","1 frá dagsetning","1 til dagsetning");
                  ILE.CALCSUMS(Quantity);
                  "Sala 1" := -ILE.Quantity;
                END;

                IF ("2 frá dagsetning"<> 0D) AND ("2 til dagsetning" <> 0D) THEN BEGIN
                  ILE.SETRANGE("Posting Date","2 frá dagsetning","2 til dagsetning");
                  ILE.CALCSUMS(Quantity);
                  "Sala 2" := -ILE.Quantity;;
                END;

                IF ("3 frá dagsetning"<> 0D) AND ("3 til dagsetning" <> 0D) THEN BEGIN
                  ILE.SETRANGE("Posting Date","3 frá dagsetning","3 til dagsetning");
                  ILE.CALCSUMS(Quantity);
                  "Sala 3" := -ILE.Quantity;;
                END;

                IF ("4 frá dagsetning"<> 0D) AND ("4 til dagsetning" <> 0D) THEN BEGIN
                  ILE.SETRANGE("Posting Date","4 frá dagsetning","4 til dagsetning");
                  ILE.CALCSUMS(Quantity);
                  "Sala 4" := -ILE.Quantity;;
                END;

                CALCFIELDS(Inventory);
                IF (Inventory = 0) AND ("Sala 1" = 0) AND ("Sala 2" = 0) AND ("Sala 3" = 0)AND ("Sala 4" = 0) THEN
                  CurrReport.SKIP;

                //#01-
                CLEAR(ReorderpointAchieved);
                IF Item."Reorder Point" <> 0 THEN
                  IF Inventory < Item."Reorder Point" THEN
                    ReorderpointAchieved := TRUE;
                //#01+
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                //CaptionML = ENU='Period',
                            //ISL='Tímabil';
                group(Options)
                {
                    CaptionML = ENU='Options',
                                ISL='Valkostir';
                    grid(Control1100410004)
                    {
                        GridLayout = Rows;
                        group("Period 1")
                        {
                            CaptionML = ENU='Period 1',
                                        ISL='Tímabil 1';
                            field("1 frá dagsetning";"1 frá dagsetning")
                            {
                            }
                            field("1 til dagsetning";"1 til dagsetning")
                            {
                            }
                        }
                        group("Period 2")
                        {
                            CaptionML = ENU='Period 2',
                                        ISL='Tímabil 2';
                            field("2 frá dagsetning";"2 frá dagsetning")
                            {
                            }
                            field("2 til dagsetning";"2 til dagsetning")
                            {
                            }
                        }
                        group("Period 3")
                        {
                            CaptionML = ENU='Period 3',
                                        ISL='Tímabil 3';
                            field("3 frá dagsetning";"3 frá dagsetning")
                            {
                            }
                            field("3 til dagsetning";"3 til dagsetning")
                            {
                            }
                        }
                        group("Period 4")
                        {
                            CaptionML = ENU='Period 4',
                                        ISL='Tímabil 4';
                            field("4 frá dagsetning";"4 frá dagsetning")
                            {
                            }
                            field("4 til dagsetning";"4 til dagsetning")
                            {
                            }
                        }
                    }
                }
            }
        }

        actions
        {
        }
    }

    labels
    {
        label(Dags1TilLabel;ENU='1 From Date',
                            ISL='1 frá dagsetning')
        label(Dags2TilLabel;ENU='2 From Date',
                            ISL='2 frá dagsetning')
        label(Dags3TilLabel;ENU='3 From Date',
                            ISL='3 frá dagsetning')
        label(Dags4TilLabel;ENU='4 From Date',
                            ISL='4 frá dagsetning')
        label(Dags1FraLabel;ENU='1 To Date',
                            ISL='1 til dagsetning')
        label(Dags2FraLabel;ENU='2 To Date',
                            ISL='2 til dagsetning')
        label(Dags3FraLabel;ENU='3 To Date',
                            ISL='3 til dagsetning')
        label(Dags4FraLabel;ENU='4 To Date',
                            ISL='4 til dagsetning')
        label(Sales1Label;ENU='Sales Period 1',
                          ISL='Sala tímabil 1')
        label(Sales2Label;ENU='Sales Period 2',
                          ISL='Sala tímabil 2')
        label(Sales3Label;ENU='Sales Period 3',
                          ISL='Sala tímabil 3')
        label(Sales4Label;ENU='Sales Period 4',
                          ISL='Sala tímabil 4')
        label(ReorderPointAchLabel;ENU='Below Reorder Point',
                                   ISL='Undir endurpöntunarmarki')
        label(ReportName;ENU='Inventory And Sales',
                         ISL='Birgðir og sala')
    }

    var
        ILE : Record "Item Ledger Entry";
        "1 frá dagsetning" : Date;
        "1 til dagsetning" : Date;
        "2 frá dagsetning" : Date;
        "2 til dagsetning" : Date;
        "3 frá dagsetning" : Date;
        "3 til dagsetning" : Date;
        "4 frá dagsetning" : Date;
        "4 til dagsetning" : Date;
        "Sala 1" : Decimal;
        "Sala 2" : Decimal;
        "Sala 3" : Decimal;
        "Sala 4" : Decimal;
        ReorderpointAchieved : Boolean;
}

