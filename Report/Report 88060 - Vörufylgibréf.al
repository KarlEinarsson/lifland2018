report 88060 "Vörufylgibréf"
{
    // version RDN

    // ls.gv = Landsteinar Strengur.Guðrún Valtýsdóttir
    // ls.hb = Landsteinar Strengur hf. Hrafnhildur Brynjólfsdóttir
    // ad.rbg = Advania.Runar Birgir Gislason
    // -----------------------------------------------------------------------------
    // Sérkerfi/Addon :
    //            PFS : Prenta fylgibréf sölusendinga
    // -----------------------------------------------------------------------------
    // 
    // No.  Date      ID      Description
    // ----------------------------------------------------------------------------
    // 
    // #01  12.01.2005 ls.gv  Skýrsla aðlöguð að beiðni Guðbjargar og Mikaels.
    // #02  13.11.2008 ls.hb  Tekið út X sem prentaðist á vörufylgibréf ef grVidtakandiX :='X';
    // #03  21.11.2012 ad.rbg Add x for Freight paid by
    DefaultLayout = RDLC;
    RDLCLayout = 'Report\rdlc\Vörufylgibréf.rdlc';

    UsageCategory = ReportsAndAnalysis;

    dataset
    {
        dataitem("Sales Invoice Header";"Sales Invoice Header")
        {
            DataItemTableView = SORTING("No.");
            RequestFilterFields = "Order No.","No.";
            column(ShiptoName_SalesInvoiceHeader;"Sales Invoice Header"."Ship-to Name")
            {
            }
            column(ShiptoAddress_SalesInvoiceHeader;"Sales Invoice Header"."Ship-to Address")
            {
            }
            column(ShiptoCity_SalesInvoiceHeader;"Sales Invoice Header"."Ship-to City")
            {
            }
            column(SalespersonCode_SalesInvoiceHeader;"Sales Invoice Header"."Salesperson Code")
            {
            }
            column(ShiptoPostCode_SalesInvoiceHeader;"Sales Invoice Header"."Ship-to Post Code")
            {
            }
            column("Flutningsaðili";recFlutningsaðili.Name)
            {
            }
            column(Nafn;recStofng.Name)
            {
            }
            column(Heimili;recStofng.Address)
            {
            }
            column("Bær";recStofng.City)
            {
            }
            column(Postfang;recStofng."Post Code")
            {
            }
            column(Kennitala;recStofng."Registration No.")
            {
            }
            column("Textalína1";Textalína1)
            {
            }
            column("Textalína2";Textalína2)
            {
            }
            column("Þyngd";gvÞyngd)
            {
            }
            column("ViðskmNr";ViðskmNr)
            {
            }
            column(Simi;recStofng."Phone No.")
            {
            }
            column(fjoldiPakka;fjoldiPakka)
            {
            }
            column(VmSimi;recVm."Phone No.")
            {
            }
            column(Magn1;Magn1)
            {
            }
            column(Magn2;Magn2)
            {
            }
            column("FlutningsaðiliNafn";recFlutningsaðili.Name)
            {
            }
            column(Dags;FORMAT(TODAY,0,'<Day,2>.<Month,2>.<Year,2>'))
            {
            }
            column(No_SalesInvoiceHeader;"Sales Invoice Header"."No.")
            {
            }
            dataitem("Sales Invoice Line";"Sales Invoice Line")
            {
                DataItemLink = "Document No."=FIELD("No.");
                DataItemTableView = SORTING("Document No.","Line No.");
                column(giroX;giroX)
                {
                }
                column(StgrX;StgrX)
                {
                }
                column(grSendandaX;grSendandaX)
                {
                }
                column(grVidtakandiX;grVidtakandiX)
                {
                }
                column(giroUpph;giroUpph)
                {
                }

                trigger OnAfterGetRecord();
                begin
                    IF bGiro THEN giroUpph := giroUpph + "Amount Including VAT";

                    IF bGiro THEN BEGIN
                      giroUpph := ROUND(giroUpph,1.0);
                      giroX :='X';
                    END;
                end;

                trigger OnPreDataItem();
                begin
                    CLEAR(giroUpph);
                end;
            }

            trigger OnAfterGetRecord();
            begin
                recStofng.GET;
                recSöluGrunnur.GET;
                //bGiro := (Greiðsluháttarkóti = recSöluGrunnur.Gírógreiðsluháttur);  // Ef greiðslumátinn er gírókrafa
                
                IF NOT recFlutningsaðili.GET("Shipping Agent Code") THEN
                  CLEAR(recFlutningsaðili);
                
                recVm.GET("Sales Invoice Header"."Sell-to Customer No.");
                
                /*IF Kennitala <> '' THEN
                  ViðskmNr := Kennitala
                ELSE*/
                  ViðskmNr := "Sell-to Customer No.";
                
                //03-
                IF "Freight Paid By" = "Freight Paid By"::Recipient THEN
                  grVidtakandiX := 'X'
                ELSE IF "Freight Paid By" = "Freight Paid By"::Prepaid THEN
                  StgrX := 'X'
                ELSE IF "Freight Paid By" = "Freight Paid By"::"Senders Account" THEN
                  grSendandaX := 'X';
                //#03+

            end;

            trigger OnPreDataItem();
            begin
                CLEAR(giroX);
                grVidtakandiX :=' ';
                //#03-
                StgrX := ' ';
                grSendandaX := ' ';
                //#03+
            end;
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field("Fjöldi pakka";fjoldiPakka)
                {
                }
                field("Þyngd";gvÞyngd)
                {
                }
            }
        }

        actions
        {
        }
    }

    labels
    {
        label(REK;ENU='REK/',
                  ISL='REK/')
        label(KG;ENU=' kg',
                 ISL=' kg')
        label("VörurText";ENU='Items according to invoice no. ',
                          ISL='Vörur samkvæmt reikningi nr. ')
    }

    var
        recStofng : Record "Company Information";
        "recSöluGrunnur" : Record "Sales & Receivables Setup";
        "recFlutningsaðili" : Record "Shipping Agent";
        recVm : Record Customer;
        bGiro : Boolean;
        bFlug : Boolean;
        grVidtakandiX : Code[1];
        giroX : Code[1];
        "ViðskmNr" : Code[20];
        giroUpph : Decimal;
        fjoldiPakka : Integer;
        "gvÞyngd" : Decimal;
        "Textalína1" : Text[25];
        "Textalína2" : Text[25];
        Magn1 : Integer;
        Magn2 : Integer;
        StgrX : Code[10];
        grSendandaX : Code[10];
        Text001 : TextConst ENU='Reg. no.:',ISL='Kt.:';
        Text002 : TextConst ENU='VAT-no.:',ISL='VSK-nr.:';
        "Fæll" : File;

    procedure "SetjaPakkaFjölda"(var Pakkafjoldi : Integer);
    begin
        fjoldiPakka := Pakkafjoldi;
    end;

    procedure "SetjaÞyngd"("Þyngd" : Decimal);
    begin
        gvÞyngd := Þyngd;
    end;

    procedure SetjaTextaOgMagn(T1 : Text[25];T2 : Text[25];M1 : Decimal;M2 : Decimal);
    begin
        //SetjaTextaOgMagn

        Textalína1 := T1;
        Textalína2 := T2;
        Magn1 := M1;
        Magn2 := M2;
    end;
}

