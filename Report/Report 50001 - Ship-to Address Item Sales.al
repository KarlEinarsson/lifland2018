report 50001 "Ship-to Address/Item Sales"
{
    // version NAVW17.10

    // rdnr.go = Rue de Net Reykjavík, Guðrún Ólafsdóttir, gudrun@ruedenet.is
    // 
    // Num Date       Jira       Developer Description
    // --- ---------- ---------- --------- -------------------
    // #01 2016-01-19 LIF-231    rdn.go    Skýrsla flutt úr 2009 yfir í 2015
    DefaultLayout = RDLC;
    RDLCLayout = 'Report\rdlc\Ship-to Address Item Sales.rdlc';
    
    UsageCategory = ReportsAndAnalysis;
    CaptionML = ENU='Customer/Item Sales',
                ISL='Viðskm. Sendist til - Vörugreining';

    dataset
    {
        dataitem("Ship-to Address";"Ship-to Address")
        {
            DataItemTableView = SORTING("Customer No.",Code);
            RequestFilterFields = "Customer No.","Code";
            column(ShipToCaption;TABLECAPTION + ': ' + ShipToFilter)
            {
            }
            column(PostCode_ShiptoAddress;"Ship-to Address"."Post Code")
            {
            }
            column(Name_ShiptoAddress;"Ship-to Address".Name)
            {
            }
            column(Address_ShiptoAddress;"Ship-to Address".Address)
            {
            }
            column(Code_ShiptoAddress;"Ship-to Address".Code)
            {
            }
            column(City_ShiptoAddress;"Ship-to Address".City)
            {
            }
            column(CurrReport_PAGENO;CurrReport.PAGENO)
            {
            }
            column(COMPANYNAME;COMPANYNAME)
            {
            }
            column(Customer_Item_SalesCaption;Customer_Item_SalesCaptionLbl)
            {
            }
            column(CurrReport_PAGENOCaption;CurrReport_PAGENOCaptionLbl)
            {
            }
            column(All_amounts_are_in_LCYCaption;All_amounts_are_in_LCYCaptionLbl)
            {
            }
            column(STRSUBSTNO_Text000_PeriodText_;STRSUBSTNO(Text000,PeriodText))
            {
            }
            column(PrintOnlyOnePerPage;PrintOnlyOnePerPage)
            {
            }
            dataitem(Customer;Customer)
            {
                DataItemLink = "No."=FIELD("Customer No.");
                PrintOnlyIfDetail = true;
                RequestFilterFields = "No.","Search Name","Customer Posting Group";
                column(Customer_TABLECAPTION__________CustFilter;TABLECAPTION + ': ' + CustFilter)
                {
                }
                column(CustFilter;CustFilter)
                {
                }
                column(Value_Entry__TABLECAPTION__________ItemLedgEntryFilter;"Value Entry".TABLECAPTION + ': ' + ItemLedgEntryFilter)
                {
                }
                column(ItemLedgEntryFilter;ItemLedgEntryFilter)
                {
                }
                column(Customer__No__;"No.")
                {
                }
                column(Customer_Name;Name)
                {
                }
                column(City_Customer;Customer.City)
                {
                }
                column(PostCode_Customer;Customer."Post Code")
                {
                }
                column(Address_Customer;Customer.Address)
                {
                }
                column(Customer__Phone_No__;"Phone No.")
                {
                }
                column(ValueEntryBuffer__Sales_Amount__Actual__;ValueEntryBuffer."Sales Amount (Actual)")
                {
                }
                column(ValueEntryBuffer__Discount_Amount_;-ValueEntryBuffer."Discount Amount")
                {
                }
                column(Profit;Profit)
                {
                    AutoFormatType = 1;
                }
                column(ProfitPct;ProfitPct)
                {
                    DecimalPlaces = 1:1;
                }
                column(PrintToExcel;PrintToExcel)
                {
                }
                column(ValueEntryBuffer__Item_No__Caption;ValueEntryBuffer__Item_No__CaptionLbl)
                {
                }
                column(Item_DescriptionCaption;Item_DescriptionCaptionLbl)
                {
                }
                column(ValueEntryBuffer__Invoiced_Quantity_Caption;ValueEntryBuffer__Invoiced_Quantity_CaptionLbl)
                {
                }
                column(Item__Base_Unit_of_Measure_Caption;Item__Base_Unit_of_Measure_CaptionLbl)
                {
                }
                column(ValueEntryBuffer__Sales_Amount__Actual___Control44Caption;ValueEntryBuffer__Sales_Amount__Actual___Control44CaptionLbl)
                {
                }
                column(ValueEntryBuffer__Discount_Amount__Control45Caption;ValueEntryBuffer__Discount_Amount__Control45CaptionLbl)
                {
                }
                column(Profit_Control46Caption;Profit_Control46CaptionLbl)
                {
                }
                column(ProfitPct_Control47Caption;ProfitPct_Control47CaptionLbl)
                {
                }
                column(Customer__Phone_No__Caption;FIELDCAPTION("Phone No."))
                {
                }
                column(TotalCaption;TotalCaptionLbl)
                {
                }
                dataitem("Value Entry";"Value Entry")
                {
                    DataItemLink = "Source No."=FIELD("No."), "Posting Date"=FIELD("Date Filter"), "Global Dimension 1 Code"=FIELD("Global Dimension 1 Filter"), "Global Dimension 2 Code"=FIELD("Global Dimension 2 Filter");
                    DataItemTableView = SORTING("Source Type","Source No.","Item No.","Variant Code","Posting Date") WHERE("Source Type"=CONST(Customer));
                    RequestFilterFields = "Item No.","Posting Date";

                    trigger OnAfterGetRecord();
                    var
                        SalesInvHdr : Record "Sales Invoice Header";
                    begin
                        //#04-
                        IF NOT (SalesInvHdr.GET("Value Entry"."Document No.") AND
                           (SalesInvHdr."Ship-to Code" = "Ship-to Address".Code)) THEN
                          CurrReport.SKIP
                        ELSE BEGIN
                        //#04+
                        ValueEntryBuffer.SETRANGE("Item No.","Item No.");

                        IF NOT ValueEntryBuffer.FIND('-') THEN BEGIN
                          ValueEntryBuffer.INIT;
                          ValueEntryBuffer."Entry No." := NextEntryNo;
                          ValueEntryBuffer."Item No." := "Item No.";
                          ValueEntryBuffer.INSERT;

                          NextEntryNo := NextEntryNo + 1;
                        END;


                        ValueEntryBuffer."Invoiced Quantity" := ValueEntryBuffer."Invoiced Quantity" + "Invoiced Quantity";
                        ValueEntryBuffer."Sales Amount (Actual)" := ValueEntryBuffer."Sales Amount (Actual)" + "Sales Amount (Actual)";
                        ValueEntryBuffer."Cost Amount (Actual)" := ValueEntryBuffer."Cost Amount (Actual)" + "Cost Amount (Actual)";
                        ValueEntryBuffer."Discount Amount" := ValueEntryBuffer."Discount Amount" + "Value Entry"."Discount Amount";
                        ValueEntryBuffer."Cost Amount (Non-Invtbl.)" := ValueEntryBuffer."Cost Amount (Non-Invtbl.)" + "Cost Amount (Non-Invtbl.)";
                        ValueEntryBuffer.MODIFY;
                        END;
                    end;

                    trigger OnPreDataItem();
                    begin
                        ValueEntryBuffer.RESET;
                        ValueEntryBuffer.DELETEALL;

                        NextEntryNo := 1;
                    end;
                }
                dataitem("Integer";"Integer")
                {
                    DataItemTableView = SORTING(Number);
                    column(ValueEntryBuffer__Item_No__;ValueEntryBuffer."Item No.")
                    {
                    }
                    column(Item_Description;Item.Description)
                    {
                    }
                    column(ValueEntryBuffer__Invoiced_Quantity_;-ValueEntryBuffer."Invoiced Quantity")
                    {
                        DecimalPlaces = 0:5;
                    }
                    column(ValueEntryBuffer__Sales_Amount__Actual___Control44;ValueEntryBuffer."Sales Amount (Actual)")
                    {
                        AutoFormatType = 1;
                    }
                    column(ValueEntryBuffer__Discount_Amount__Control45;-ValueEntryBuffer."Discount Amount")
                    {
                        AutoFormatType = 1;
                    }
                    column(Profit_Control46;Profit)
                    {
                        AutoFormatType = 1;
                    }
                    column(ProfitPct_Control47;ProfitPct)
                    {
                        DecimalPlaces = 1:1;
                    }
                    column(Item__Base_Unit_of_Measure_;Item."Base Unit of Measure")
                    {
                    }

                    trigger OnAfterGetRecord();
                    begin
                        IF Number = 1 THEN
                          ValueEntryBuffer.FIND('-')
                        ELSE
                          ValueEntryBuffer.NEXT;

                        Profit :=
                          ValueEntryBuffer."Sales Amount (Actual)" +
                          ValueEntryBuffer."Cost Amount (Actual)" +
                          ValueEntryBuffer."Cost Amount (Non-Invtbl.)";

                        IF PrintToExcel AND Item.GET(ValueEntryBuffer."Item No.") THEN BEGIN
                          CalcProfitPct;
                          MakeExcelDataBody;
                        END;
                    end;

                    trigger OnPreDataItem();
                    begin
                        CurrReport.CREATETOTALS(
                          ValueEntryBuffer."Sales Amount (Actual)",
                          ValueEntryBuffer."Discount Amount",
                          Profit);

                        ValueEntryBuffer.RESET;
                        SETRANGE(Number,1,ValueEntryBuffer.COUNT);
                    end;
                }

                trigger OnPreDataItem();
                begin
                    CurrReport.NEWPAGEPERRECORD := PrintOnlyOnePerPage;

                    CurrReport.CREATETOTALS(
                      ValueEntryBuffer."Sales Amount (Actual)",
                      ValueEntryBuffer."Discount Amount",
                      Profit);
                end;
            }

            trigger OnPreDataItem();
            begin
                CurrReport.CREATETOTALS(
                  ValueEntryBuffer."Sales Amount (Actual)",
                  ValueEntryBuffer."Discount Amount",
                  Profit);
            end;
        }
    }

    requestpage
    {
        SaveValues = true;

        layout
        {
            area(content)
            {
                group(Options)
                {
                    CaptionML = ENU='Options',
                                ISL='Valkostir';
                    field(PrintOnlyOnePerPage;PrintOnlyOnePerPage)
                    {
                        CaptionML = ENU='New Page per Customer',
                                    ISL='Ný blaðsíða á hvern viðskiptamann';
                    }
                    field(PrintToExcel;PrintToExcel)
                    {
                        CaptionML = ENU='Print to Excel',
                                    ISL='Prenta í Excel';
                    }
                }
            }
        }

        actions
        {
        }

        trigger OnOpenPage();
        begin
            PrintToExcel := FALSE;
        end;
    }

    labels
    {
        ReportName = 'Aðsetur Viðskm. - Vörugreining';}

    trigger OnPostReport();
    begin
        IF PrintToExcel THEN
          CreateExcelbook;
    end;

    trigger OnPreReport();
    begin
        CustFilter := Customer.GETFILTERS;
        ItemLedgEntryFilter := "Value Entry".GETFILTERS;
        PeriodText := "Value Entry".GETFILTER("Posting Date");
        ShipToFilter := "Ship-to Address".GETFILTERS;  //#04

        IF PrintToExcel THEN
          MakeExcelInfo;
    end;

    var
        Text000 : TextConst ENU='Period: %1',ISL='Tímabil: %1';
        Item : Record Item;
        ValueEntry : Record "Value Entry";
        ValueEntryBuffer : Record "Value Entry" temporary;
        ExcelBuf : Record "Excel Buffer" temporary;
        CustFilter : Text;
        ItemLedgEntryFilter : Text;
        PeriodText : Text[30];
        NextEntryNo : Integer;
        PrintOnlyOnePerPage : Boolean;
        Profit : Decimal;
        ProfitPct : Decimal;
        Text001 : TextConst ENU='Data',ISL='Gögn';
        Text002 : TextConst ENU='Customer/Item Sales',ISL='Viðskm. - Vörugreining';
        Text003 : TextConst ENU='Company Name',ISL='Heiti fyrirtækis';
        Text004 : TextConst ENU='Report No.',ISL='Skýrsla nr.';
        Text005 : TextConst ENU='Report Name',ISL='Skýrsluheiti';
        Text006 : TextConst ENU='User ID',ISL='Kenni notanda';
        Text007 : TextConst ENU='Date',ISL='Dagsetning';
        Text008 : TextConst ENU='Customer Filters',ISL='Afmarkanir viðskiptamanna';
        Text009 : TextConst ENU='Value Entry Filters',ISL='Afmarkanir á virðisfærslu';
        PrintToExcel : Boolean;
        Text010 : TextConst ENU='Profit',ISL='Framlegð';
        Text011 : TextConst ENU='Profit %',ISL='Framlegðar%';
        Customer_Item_SalesCaptionLbl : TextConst ENU='Customer/Item Sales',ISL='Viðskm. - Vörugreining';
        CurrReport_PAGENOCaptionLbl : TextConst ENU='Page',ISL='Bls.';
        All_amounts_are_in_LCYCaptionLbl : TextConst ENU='All amounts are in LCY',ISL='Allar upphæðir eru í SGM';
        ValueEntryBuffer__Item_No__CaptionLbl : TextConst ENU='Item No.',ISL='Vörunr.';
        Item_DescriptionCaptionLbl : TextConst ENU='Description',ISL='Lýsing';
        ValueEntryBuffer__Invoiced_Quantity_CaptionLbl : TextConst ENU='Invoiced Quantity',ISL='Reikningsfært magn';
        Item__Base_Unit_of_Measure_CaptionLbl : TextConst ENU='Unit of Measure',ISL='Mælieining';
        ValueEntryBuffer__Sales_Amount__Actual___Control44CaptionLbl : TextConst ENU='Amount',ISL='Upphæð';
        ValueEntryBuffer__Discount_Amount__Control45CaptionLbl : TextConst ENU='Discount Amount',ISL='Afsl.upphæð';
        Profit_Control46CaptionLbl : TextConst ENU='Profit',ISL='Framlegð';
        ProfitPct_Control47CaptionLbl : TextConst ENU='Profit %',ISL='Framlegðar%';
        TotalCaptionLbl : TextConst ENU='Total',ISL='Samtals';
        ShipToFilter : Text[250];

    local procedure CalcProfitPct();
    begin
        WITH ValueEntryBuffer DO BEGIN
          IF "Sales Amount (Actual)" <> 0 THEN
            ProfitPct := ROUND(100 * Profit / "Sales Amount (Actual)",0.1)
          ELSE
            ProfitPct := 0;
        END;
    end;

    procedure MakeExcelInfo();
    begin
        ExcelBuf.SetUseInfoSheet;
        ExcelBuf.AddInfoColumn(FORMAT(Text003),FALSE,TRUE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddInfoColumn(COMPANYNAME,FALSE,FALSE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.NewRow;
        ExcelBuf.AddInfoColumn(FORMAT(Text005),FALSE,TRUE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddInfoColumn(FORMAT(Text002),FALSE,FALSE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.NewRow;
        ExcelBuf.AddInfoColumn(FORMAT(Text004),FALSE,TRUE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddInfoColumn(REPORT::"Customer/Item Sales",FALSE,FALSE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Number);
        ExcelBuf.NewRow;
        ExcelBuf.AddInfoColumn(FORMAT(Text006),FALSE,TRUE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddInfoColumn(USERID,FALSE,FALSE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.NewRow;
        ExcelBuf.AddInfoColumn(FORMAT(Text007),FALSE,TRUE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddInfoColumn(TODAY,FALSE,FALSE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Date);
        ExcelBuf.NewRow;
        ExcelBuf.AddInfoColumn(FORMAT(Text008),FALSE,TRUE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddInfoColumn(Customer.GETFILTERS,FALSE,FALSE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.NewRow;
        ExcelBuf.AddInfoColumn(FORMAT(Text009),FALSE,TRUE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddInfoColumn("Value Entry".GETFILTERS,FALSE,FALSE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.ClearNewRow;
        MakeExcelDataHeader;
    end;

    local procedure MakeExcelDataHeader();
    begin
        ExcelBuf.NewRow;
        ExcelBuf.AddColumn(Customer.FIELDCAPTION("No."),FALSE,'',TRUE,FALSE,TRUE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn(Customer.FIELDCAPTION(Name),FALSE,'',TRUE,FALSE,TRUE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn("Ship-to Address".FIELDCAPTION(Code),FALSE,'',TRUE,FALSE,TRUE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn(ValueEntryBuffer.FIELDCAPTION("Item No."),FALSE,'',TRUE,FALSE,TRUE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn(Item.FIELDCAPTION(Description),FALSE,'',TRUE,FALSE,TRUE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn(ValueEntryBuffer.FIELDCAPTION("Invoiced Quantity"),FALSE,'',TRUE,FALSE,TRUE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn(Item.FIELDCAPTION("Base Unit of Measure"),FALSE,'',TRUE,FALSE,TRUE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn(
          ValueEntryBuffer.FIELDCAPTION("Sales Amount (Actual)"),FALSE,'',TRUE,FALSE,TRUE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn(ValueEntryBuffer.FIELDCAPTION("Discount Amount"),FALSE,'',TRUE,FALSE,TRUE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn(FORMAT(Text010),FALSE,'',TRUE,FALSE,TRUE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn(FORMAT(Text011),FALSE,'',TRUE,FALSE,TRUE,'',ExcelBuf."Cell Type"::Text);
    end;

    procedure MakeExcelDataBody();
    begin
        ExcelBuf.NewRow;
        ExcelBuf.AddColumn(Customer."No.",FALSE,'',FALSE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn(Customer.Name,FALSE,'',FALSE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn("Ship-to Address".Code,FALSE,'',FALSE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn(ValueEntryBuffer."Item No.",FALSE,'',FALSE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn(Item.Description,FALSE,'',FALSE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn(-ValueEntryBuffer."Invoiced Quantity",FALSE,'',FALSE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Number);
        ExcelBuf.AddColumn(Item."Base Unit of Measure",FALSE,'',FALSE,FALSE,FALSE,'',ExcelBuf."Cell Type"::Text);
        ExcelBuf.AddColumn(ValueEntryBuffer."Sales Amount (Actual)",FALSE,'',FALSE,FALSE,FALSE,'#,##0.00',ExcelBuf."Cell Type"::Number);
        ExcelBuf.AddColumn(-ValueEntryBuffer."Discount Amount",FALSE,'',FALSE,FALSE,FALSE,'#,##0.00',ExcelBuf."Cell Type"::Number);
        ExcelBuf.AddColumn(Profit,FALSE,'',FALSE,FALSE,FALSE,'#,##0.00',ExcelBuf."Cell Type"::Number);
        ExcelBuf.AddColumn(ProfitPct,FALSE,'',FALSE,FALSE,FALSE,'#,##0.00',ExcelBuf."Cell Type"::Number);
    end;

    procedure CreateExcelbook();
    begin
        //ExcelBuf.CreateBookAndOpenExcel(Text001,Text002,COMPANYNAME,USERID);
        ERROR('Not supported');
    end;

    procedure InitializeRequest(NewPagePerCustomer : Boolean;PrintToExcelFile : Boolean);
    begin
        PrintOnlyOnePerPage := NewPagePerCustomer;
        PrintToExcel := PrintToExcelFile;
    end;
}

