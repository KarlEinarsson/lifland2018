report 50005 "Eftirlit með útkeyrslu"
{
    // version FóðurAfgr0.00

    DefaultLayout = RDLC;
    RDLCLayout = 'Report\rdlc\Eftirlit með útkeyrslu.rdlc';

    UsageCategory = ReportsAndAnalysis;
    
    dataset
    {
        dataitem("Sales Invoice Line";"Sales Invoice Line")
        {
            DataItemTableView = SORTING("Shortcut Dimension 2 Code","Shipment Date") ORDER(Ascending) WHERE("Unit of Measure Code"=FILTER('KG'), "Item Category Code"=FILTER(9000));
            RequestFilterFields = "Shortcut Dimension 2 Code","Shipment Date";
            column(SiloDateAndTime;"Sales Invoice Line"."Silo date and time")
            {
                //IncludeCaption = true;
            }
            column(DocumentNo;"Sales Invoice Line"."Document No.")
            {
                IncludeCaption = true;
            }
            column(No;"Sales Invoice Line"."No.")
            {
                IncludeCaption = true;
            }
            column(Description;"Sales Invoice Line".Description)
            {
                IncludeCaption = true;
            }
            column(VehicleBin;"Sales Invoice Line"."Vehicle Bin")
            {
                //IncludeCaption = true;
            }
            column(SiloAtCustomerSite;"Sales Invoice Line"."Silo at Customer side")
            {
                //IncludeCaption = true;
            }
            column(ShortcutDimension2Code;"Sales Invoice Line"."Shortcut Dimension 2 Code")
            {
                IncludeCaption = true;
            }
            column(Quantity;"Sales Invoice Line".Quantity)
            {
                IncludeCaption = true;
            }
        }
    }

    requestpage
    {

        layout
        {
        }

        actions
        {
        }
    }

    labels
    {
        label(Car;ENU='Car',
                  ISL='Bíll')
    }
}

