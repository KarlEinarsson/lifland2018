report 59241 "Zebra LP2824  Label SAM"
{
    // version SAME

    // I8,1,001      Character Set Selection
    // OD            Option Select,
    // FK"MYTEST"    Delete form
    // FS"MYTEST"    Store form
    // V00,12,L,""   Define variable,Max length, Justification, Prompt
    // V01,18,L,""
    // V02,10,L,""
    // q344          Set form width
    // Q199,24+0     Set Form Length, Gap length, Offset length
    // S2            Speed select
    // D8            Density
    // ZT            Print direction
    // TTh:m         Define time layout
    // TDy2.mn.dd
    // B14           BarCode
    // A26           ASCII Text
    // A47
    // A112
    // FE            End Form Store

    CaptionML = ENU='Whse. Zebra LP2824  Label',
                ISL='Vöruhús Zebra LP2824  Label';
    ProcessingOnly = true;

    dataset
    {
        dataitem("Transport Waybill SAM";"Transport Waybill SAM")
        {
            DataItemTableView = SORTING("Document Type","Ship-to Address");
            RequestFilterFields = "Document Type";
            /*
            dataitem("Integer";"Integer")
            {
                DataItemTableView = SORTING(Number);

                trigger OnAfterGetRecord();
                begin
                    NoStDone += 1;

                    NoOfNo := FORMAT(NoStDone) + ' af ' + FORMAT("Transport Waybill SAM"."Bill-to Name 2");

                    TextOnLabel[2] := 'A525,25,0,3,2,2,N,"' + NoOfNo + '"';

                    Skrifa_Línur(zlp001);
                    Skrifa_Línur(zlp002);
                    Skrifa_Línur(zlp003);
                    Skrifa_Línur(zlp004);
                    Skrifa_Línur(zlp005);
                    Skrifa_Línur(zlp006);
                    Skrifa_Línur(zlp007);
                    Skrifa_Línur(zlp008);


                    Skrifa_Línur(TextOnLabel[1]);
                    Skrifa_Línur(TextOnLabel[2]);
                    Skrifa_Línur(TextOnLabel[3]);
                    Skrifa_Línur(TextOnLabel[9]);
                    Skrifa_Línur(TextOnLabel[4]);
                    Skrifa_Línur(TextOnLabel[5]);
                    Skrifa_Línur(TextOnLabel[6]);
                    Skrifa_Línur(TextOnLabel[7]);
                    Skrifa_Línur(TextOnLabel[8]);

                    Skrifa_Línur(zlp009);
                end;

                trigger OnPreDataItem();
                begin
                    NoStDone := 0;
                    //Integer.SETRANGE(Number,1,"Transport Waybill SAM"."Bill-to Name 2");
                end;
            }

            trigger OnAfterGetRecord();
            begin
                CLEAR(TextOnLabel);
                
                CASE "Ship-to Address" OF
                  0 : BEGIN
                        IF NOT SH.GET("Document Type") THEN
                          SH.INIT;
                      END;
                  1 : BEGIN
                        SH.INIT;
                        IF SHP.GET(1,"Document Type") THEN
                          SH.TRANSFERFIELDS(SHP);
                      END;
                END;
                
                SHipAddr2 := '';
                SHipAddr := COPYSTR(SH."Ship-to Address",1,24);
                
                
                //IF STRLEN(SH."Ship-to Address") > 22 THEN BEGIN
                  //SHipAddr := COPYSTR(SH."Ship-to Address",1,22);
                  //SHipAddr2 := COPYSTR(SH."Ship-to Address",23);
                //END;
                
                
                TextOnLabel[1] := 'A50,25,0,3,2,2,N,"' + FORMAT(TODAY)+'"';
                
                TextOnLabel[3] := 'A50,90,0,3,2,2,N,"' + COPYSTR(SH."Ship-to Name",1,24) + '"';
                TextOnLabel[4] := 'A50,140,0,3,2,2,N,"' + SHipAddr + '"';
                TextOnLabel[5] := 'A50,190,0,3,2,2,N,"' + SH."Ship-to Post Code" + ' ' + SH."Ship-to City" + '"';
                TextOnLabel[6] := 'A50,400,0,3,2,2,N,"' + SH."No."+'"';
                TextOnLabel[7] := 'A50,450,0,3,2,3,N,"' + CompanyInfo.Name+'"';
                TextOnLabel[8] := 'A50,525,0,3,1,1,N,"' + CompanyInfo.Address + ' ' + CompanyInfo."Post Code" + ' S:' + CompanyInfo."Phone No."+'"';

            end;

            trigger OnPreDataItem();
            begin
                CompanyInfo.GET();

                zlp001 := ' ';
                zlp002 := 'I8,1';
                zlp003 := 'N';
                zlp004 := 'q560';
                zlp005 := 'Q800,24';
                zlp006 := 'S2';
                zlp007 := 'D8';
                zlp008 := 'ZT';
                zlp009 := 'P1';
            end;
        */
        }
    }

    requestpage
    {

        layout
        {
        }

        actions
        {
        }
    }

    labels
    {
    }
    /*
    trigger OnPostReport();
    var
        FSO : Automation "'{420B2830-E718-11CF-893D-00A0C9054228}' 1.0:'{0D43FE01-F093-11CF-8940-00A0C9054228}':''{420B2830-E718-11CF-893D-00A0C9054228}' 1.0'.FileSystemObject";
    begin
        //Skrá_f.CREATE('C:\temp\midinn.txt');

        Skráarnafn := '';
        Skrá_f.CREATEINSTREAM(IStream);
        CREATE(FSO,TRUE,TRUE);
        DOWNLOADFROMSTREAM(IStream,'','<TEMP>', '',Skráarnafn);
        FSO.CopyFile(Skráarnafn,'\\BJORN\PC7',TRUE);
        //FSO.CopyFile(Skráarnafn,'C:\TEMP\MIDI.TXT',TRUE);
        Skrá_f.CLOSE();
    end;

    trigger OnPreReport();
    begin
        Skrifa_Skrá();
        Setup.GET;
    end;

    var
        SH : Record "Sales Invoice Header";
        SHP : Record "Sales Header";
        TextOnLabel : array [9] of Text[80];
        TextLabel : array [9] of Text[80];
        RecCount : Integer;
        i : Integer;
        Text000 : TextConst ENU='Print labels on Zebra LP2824',ISL='Prenta límmiða á Zebra LP2824';
        BlankNo : Integer;
        CompanyInfo : Record "Company Information";
        NoStDone : Integer;
        NoOfNo : Text[20];
        "Skrá_f" : File;
        "Skrá_t" : Text[80];
        zlp001 : Code[10];
        zlp002 : Code[10];
        zlp003 : Code[10];
        zlp004 : Code[10];
        zlp005 : Code[10];
        zlp006 : Code[10];
        zlp007 : Code[10];
        zlp008 : Code[10];
        zlp009 : Code[10];
        IStream : InStream;
        "Skráarnafn" : Text[100];
        SHipAddr : Text[50];
        SHipAddr2 : Text[50];
        Setup : Record "Transport Setup";

    procedure "Skrifa_Skrá"();
    var
        FJgrunnur : Record "General Ledger Setup";
        "Skráarslóði" : Text[100];
    begin
        Skrá_f.TEXTMODE(TRUE);
        Skrá_f.WRITEMODE(TRUE);
        FJgrunnur.GET();

        //ATH uppsetning
        //Skráarnafn := FJgrunnur."Zebra Folder";
        //Skrá_f.CREATE(TEMPORARYPATH+'\' + "Transport Waybill"."Sales Invoice");
        Skrá_f.CREATE(TEMPORARYPATH+'\MIDI.TXT');

        //Skrá_f.CREATE('c:\temp2\midinn.txt');

        //IF Skrá_f.CREATE(Skráarnafn) THEN;
    end;

    procedure "Skrifa_Línur"("LínuFilt" : Text[80]);
    begin
        Skrá_f.WRITE(LínuFilt);
    end;
    */
}

