report 50007 "Afhendingarseðill - Lífland"
{
    // version FóðurAfgr0.00

    DefaultLayout = RDLC;
    RDLCLayout = 'Report\rdlc\Afhendingarseðill - Lífland.rdlc';

    UsageCategory = ReportsAndAnalysis;
    
    dataset
    {
        dataitem("Sales Header";"Sales Header")
        {
            column(CustAddr1;CustAddr[1])
            {
            }
            column(CustAddr2;CustAddr[2])
            {
            }
            column(CustAddr3;CustAddr[3])
            {
            }
            column(CustAddr4;CustAddr[4])
            {
            }
            column(CustAddr5;CustAddr[5])
            {
            }
            column(CustAddr6;CustAddr[6])
            {
            }
            column(CustAddr7;CustAddr[7])
            {
            }
            column(CustAddr8;CustAddr[8])
            {
            }
            column(CompanyInfoName;CompanyInfo.Name)
            {
                IncludeCaption = true;
            }
            column(CompanyInfoAddress;CompanyInfo.Address)
            {
                IncludeCaption = true;
            }
            column(CompanyInfoPostCode;CompanyInfo."Post Code")
            {
                IncludeCaption = true;
            }
            column(CompanyInfoCity;CompanyInfo.City)
            {
                IncludeCaption = true;
            }
            column(CompanyInfoPhoneNo;CompanyInfo."Phone No.")
            {
                IncludeCaption = true;
            }
            column(BillToCustomerNo;"Sales Header"."Bill-to Customer No.")
            {
                IncludeCaption = true;
            }
            column(No;"Sales Header"."No.")
            {
                IncludeCaption = true;
            }
            column(ShipmentDate;"Sales Header"."Shipment Date")
            {
                IncludeCaption = true;
            }
            dataitem("Sales Line";"Sales Line")
            {
                DataItemLink = "Document Type"=FIELD("Document Type"), "Document No."=FIELD("No.");
                DataItemTableView = SORTING("Document Type","Document No.","Line No.") WHERE(Type=FILTER(Item), "Document Type"=FILTER(Order..Invoice), "Item Category Code"=FILTER(<>2000));
                column(SL_DocumentType;"Sales Line"."Document Type")
                {
                    IncludeCaption = true;
                }
                column(SL_DocumentNo;"Sales Line"."Document No.")
                {
                    IncludeCaption = true;
                }
                column(SL_LineNo;"Sales Line"."Line No.")
                {
                    IncludeCaption = true;
                }
                column(SL_Lota;"Sales Line".Lota)
                {
                    //IncludeCaption = true;
                }
                column(SL_No;"Sales Line"."No.")
                {
                    IncludeCaption = true;
                }
                column(SL_Description;"Sales Line".Description)
                {
                    IncludeCaption = true;
                }
                column(SL_VehicleBin;"Sales Line"."Vehicle Bin")
                {
                    //IncludeCaption = true;
                }
                column(SL_ShortcutDimensionCode2;"Sales Line"."Shortcut Dimension 2 Code")
                {
                    IncludeCaption = true;
                }
                column(SL_Tank;"Sales Line".Tank)
                {
                    //IncludeCaption = true;
                }
                column(SL_Quantity;"Sales Line".Quantity)
                {
                    IncludeCaption = true;
                }
                column(SL_SiloAtCustomer;"Sales Line"."Silo at Customer side")
                {
                    //IncludeCaption = true;
                }
            }
            dataitem("Sales Line Akstur";"Sales Line")
            {
                DataItemLink = "Document Type"=FIELD("Document Type"), "Document No."=FIELD("No.");
                DataItemTableView = SORTING("Document Type","Document No.","Line No.") WHERE(Type=FILTER(Item), "Document Type"=FILTER(Order..Invoice), "Item Category Code"=FILTER(2000));
                column(SLA_DocumentType;"Sales Line Akstur"."Document Type")
                {
                    IncludeCaption = true;
                }
                column(SLA_DocumentNo;"Sales Line Akstur"."Document No.")
                {
                    IncludeCaption = true;
                }
                column(SLA_LineNo;"Sales Line Akstur"."Line No.")
                {
                    IncludeCaption = true;
                }
                column(SLA_No;"Sales Line Akstur"."No.")
                {
                    IncludeCaption = true;
                }
                column(SLA_Description;"Sales Line Akstur".Description)
                {
                    IncludeCaption = true;
                }
                column(SLA_VehicleBin;"Sales Line Akstur"."Vehicle Bin")
                {
                    //IncludeCaption = true;
                }
                column(SLA_ShortcutDimensionCode2;"Sales Line Akstur"."Shortcut Dimension 2 Code")
                {
                    IncludeCaption = true;
                }
                column(SLA_Tank;"Sales Line Akstur".Tank)
                {
                    //IncludeCaption = true;
                }
                column(SLA_Quantity;"Sales Line Akstur".Quantity)
                {
                    IncludeCaption = true;
                }
                column(SLA_SiloAtCustomer;"Sales Line Akstur"."Silo at Customer side")
                {
                    //IncludeCaption = true;
                }
            }

            trigger OnAfterGetRecord();
            begin
                //FormatAddr.SalesHeaderShipTo(CustAddr,"Sales Header"); BREYTT í 2018

                SL.SETRANGE(SL."Document Type","Sales Header"."Document Type");
                SL.SETRANGE(SL."Document No.","Sales Header"."No.");
                IF SL.FINDFIRST THEN
                REPEAT
                IF (SL.Type = SL.Type::Item) AND (SL.Quantity <> 0) THEN
                  IF SL."Silo date and time" = 0DT THEN
                    ConfirmUpdate := TRUE;
                  IF (SL."Item Category Code" = '9000') AND (SL."Unit of Measure Code" = 'KG') THEN
                    IF SL."Vehicle Bin" = '' THEN
                      ERROR(Text002);
                UNTIL (SL.NEXT = 0) OR ConfirmUpdate;

                IF ConfirmUpdate THEN
                  MESSAGE(Text001);
            end;
        }
    }

    requestpage
    {

        layout
        {
        }

        actions
        {
        }
    }

    labels
    {
        label(DeliveryNote;ENU='Delivery Note',
                           ISL='Afhendingarseðill')
        label(BillToCustomer;ENU='Bill-to Customer',
                             ISL='Reikn.færist á viðskm')
        label("Page";ENU='Page',
                     ISL='Bls.')
        label(TelPhone;ENU='Telephone',
                       ISL='Sími')
        label(Mast;ENU='Starfsleyfi MAST nr.',
                   ISL='Starfsleyfi MAST nr.')
        label(DeliveryDate;ENU='Delivery Date',
                           ISL='Afh.dags')
    }

    trigger OnInitReport();
    begin
        CompanyInfo.GET;
    end;

    var
        FormatAddr : Codeunit "Format Address";
        CompanyInfo : Record "Company Information";
        CustAddr : array [8] of Text[50];
        SL : Record "Sales Line";
        ConfirmUpdate : Boolean;
        Text001 : TextConst ENU='Quantity from silo has not been updated automatically',ISL='Ath.magn frá tanki hefur ekki verið uppfært rafrænt';
        Text002 : Label 'Það er ekki búið að skrá hólf\Skráðu hólf';
}

