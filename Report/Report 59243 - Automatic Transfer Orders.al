report 59243 "Automatic Transfer Orders"
{
    /*
    ProcessingOnly = true;

    dataset
    {
        dataitem("<TransactionSalesEntry>";Table99001473)
        {
            RequestFilterFields = "Store No.",Date,"Item Category Code";
        }
    }

    requestpage
    {

        layout
        {
            area(content)
            {
                field(VendorNo;VendorNo)
                {
                    Caption = 'Númer lánadrottins';
                    Lookup = true;
                    LookupPageID = "Vendor List";
                    TableRelation = Vendor;
                }
            }
        }

        actions
        {
        }
    }

    labels
    {
    }

    trigger OnPreReport();
    var
        TransSalesCopy : Record Table99001473;
    begin
        TransFilter := "<TransactionSalesEntry>".GETFILTERS;

        IF "<TransactionSalesEntry>".FINDSET THEN
         AutoTransOrders.CreateTransferOrders("<TransactionSalesEntry>", VendorNo);
    end;

    var
        Counter : Integer;
        TransFilter : Text;
        AutoTransOrders : Codeunit Codeunit87502;
        Store : Record Table99001470;
        PosTerminal : Record Table99001471;
        VendorNo : Code[10];
    */
}

