table 59244 "Transport-Item Type"
{
    // version SAME

    // Samskip Fylgibréf
    // EY 06.12.2011 -

    CaptionML = ENU='Transport-Item Type',
                ISL='Eiginlekar flutnings vöru';

    fields
    {
        field(1;"Item Type";Option)
        {
            CaptionML = ENU='Item Type',
                        ISL='Eðli vöru';
            OptionCaptionML = ENU='Normal,Chemical,Freeze,Cooler',
                              ISL='Þurrvara,Efnavara,Frystivara,Kælivara';
            OptionMembers = Normal,Chemical,Freeze,Cooler;
        }
        field(2;"Item Category Code";Code[10])
        {
            CaptionML = ENU='Item Category Code',
                        ISL='Kóti yfirflokks vöru';
            TableRelation = "Item Category";

            trigger OnValidate();
            begin
                if "Item Category Code" <> xRec."Item Category Code" then begin
                  if not ProductGrp.GET("Item Category Code","Product Group Code") then
                    VALIDATE("Product Group Code",'')
                  else
                    VALIDATE("Product Group Code");
                end;
            end;
        }
        field(3;"Product Group Code";Code[10])
        {
            CaptionML = ENU='Product Group Code',
                        ISL='Kóti framleiðsluflokks';
            TableRelation = "Product Group".Code WHERE ("Item Category Code"=FIELD("Item Category Code"));
        }
        field(4;Description;Text[30])
        {
        }
        field(5;"Default Chemical Group";Code[10])
        {
            CaptionML = ENU='Default Chemical Group',
                        ISL='Sjálfgefinn efnavöruflokkur';
            TableRelation = "Chemical Group";
        }
    }

    keys
    {
        key(Key1;"Item Type","Item Category Code","Product Group Code")
        {
        }
    }

    fieldgroups
    {
    }

    var
        ProductGrp : Record "Product Group";
}

