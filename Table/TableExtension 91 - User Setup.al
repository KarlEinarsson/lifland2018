tableextension 91 "User Setup Extension" extends "User Setup"
{
    fields
    {
        field(50000;"Má sjá eftirágefna afslætti";Boolean)
        {
            CaptionML = ENU = 'Má sjá eftirágefna afslætti',
                        ISL = 'Má sjá eftirágefna afslætti';
        }

        field(50001;"Má opna vörur";Boolean)
        {
            CaptionML = ENU = 'Can activate Items',
                        ISL = 'Má virkja vörur';
            Description = 'LIF-115';
        }

        field(50002;"Má opna viðskiptamann";Boolean)
        {
            CaptionML = ENU = 'Má opna viðskiptamann',
                        ISL = 'icelandicText';
            Description = 'LIF389';
        }
    }
}