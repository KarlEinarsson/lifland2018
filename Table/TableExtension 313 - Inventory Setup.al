tableextension 313 "Inventory Setup Extension" extends "Inventory Setup"
{
    fields
    {
        field(50011;"Confirm Changes for Item";Boolean)
        {
            CaptionML = ENU = '"Confirm Changes for Item"',
                        ISL = 'Staðfesta birgðarspjaldsbreytingar';
        }
    }
}