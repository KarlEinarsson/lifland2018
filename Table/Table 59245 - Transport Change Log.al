table 59245 "Transport Change Log"
{
    // version SAME

    // Samskip Fylgibréf
    // EY 06.12.2011 -

    CaptionML = ENU='Transport Change Log',
                ISL='Breytingaskrár Sendinga';
    Permissions = TableData "Transport Change Log"=rimd;

    fields
    {
        field(1;"Entry No.";BigInteger)
        {
            AutoIncrement = true;
            CaptionML = ENU='Entry No.',
                        ISL='Færslunr.';
        }
        field(2;"Date and Time";DateTime)
        {
            CaptionML = ENU='Date and Time',
                        ISL='Dag- og tímasetning';
        }
        field(3;"Creation Time";Time)
        {
            CaptionML = ENU='Time',
                        ISL='Tími';
        }
        field(4;"User ID";Code[20])
        {
            CaptionML = ENU='User ID',
                        ISL='Kenni notanda';
        }
        field(5;"Type of Change";Option)
        {
            CaptionML = ENU='Type of Change',
                        ISL='Tegund breytingar';
            OptionCaptionML = ENU=' ,Sent,Recieved,Confirmed,Cancelled',
                              ISL=' ,Sent,Móttekið,Staðfest,Hætt við';
            OptionMembers = " ",Sent,Recieved,Confirmed,Cancelled;
        }
        field(6;"Document No.";Code[20])
        {
            CaptionML = ENU='Document No.',
                        ISL='Sendingarnúmer';
        }
        field(7;"File name";Text[250])
        {
            CaptionML = ENU='File Name',
                        ISL='Heiti skráar';
        }
        field(8;Description;Text[50])
        {
            CaptionML = ENU='Description',
                        ISL='Lýsing';
        }
        field(10;ath;Code[10])
        {
        }
    }

    keys
    {
        key(Key1;"Entry No.")
        {
        }
        key(Key2;"Type of Change")
        {
        }
        key(Key3;"Document No.")
        {
        }
    }

    fieldgroups
    {
    }

    trigger OnInsert();
    begin
        if "Entry No." = 0 then
          if CHrec.FIND('+') then
            "Entry No." := CHrec."Entry No." + 1
          else
            "Entry No." := 1;

        "Date and Time" := CREATEDATETIME(TODAY,TIME);
        "Creation Time" := TIME;
        "User ID" := USERID;
    end;

    var
        CHrec : Record "Transport Change Log";
}

