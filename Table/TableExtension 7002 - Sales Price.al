tableextension 7002 "Sales Price" extends "Sales Price"
{
    fields
    {
        // Add changes to table fields here
    }

    procedure CheckDate();
    var
        myInt : Integer;
    begin        
        IF "Sales Type" = "Sales Type"::Customer THEN
        IF ("Starting Date" = 0D) OR ("Ending Date" = 0D) THEN
            ERROR('If %1 = %2, then you must specify %3 and %4.',FIELDCAPTION("Sales Type"),FORMAT("Sales Type"),FIELDCAPTION("Starting Date"),FIELDCAPTION("Ending Date"));
    end;

    trigger OnBeforeInsert();
    begin
        CheckDate();
    end;

    trigger OnBeforeModify();
    begin
        CheckDate();
    end;     
}