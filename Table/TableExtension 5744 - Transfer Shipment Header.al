tableextension 5744 "Transfer Shipment Header Ext" extends "Transfer Shipment Header"
{
    fields
    {
        field(50001;"Freight Paid By";Option)
        {
            CaptionML = ENU = 'Freight Paid By',
                        ISL = 'Flutningsgjöld greiðast af';
            OptionMembers = Recipient,Prepaid,"Senders Account";
            OptionCaptionML = 	ENU = 'Recipient,Prepaid,Senders Account',
                                ISL = 'Viðtakanda,Staðgreitt,Í reikning sendanda';             
        }
    }
}