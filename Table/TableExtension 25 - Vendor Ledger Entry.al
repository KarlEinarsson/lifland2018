tableextension 25 "Vendor Ledger Entry Extension" extends "Vendor Ledger Entry"
{
    fields
    {
        field(50000;"Order No.";Code[20])
        {
            CaptionML = ENU = 'Order No.',
                        ISL = 'Pöntunarnúmer';
            FieldClass = FlowField;
            CalcFormula = lookup("Purch. Inv. Header"."Order No." where ("No."=field("Document No.")));
        }
    }
}