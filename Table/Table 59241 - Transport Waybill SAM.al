table 59241 "Transport Waybill SAM"
{
    // version SAME

    // Reitur sem þarf að gera í töflu 291
    // 
    // PropertyValue
    // Field No.50000
    // NameTransport Waybill
    // CaptionFylgibréf
    // CaptionMLENU=Transport Waybill;ISL=Fylgibréf
    // DescriptionÁÓ016
    // Data TypeOption
    // Enabled<Yes>
    // InitValue<Undefined>
    // FieldClass<Normal>
    // AltSearchField<Undefined>
    // OptionString ,Flytjandi,Landflutningar
    // OptionCaption<Undefined>
    // OptionCaptionML<Undefined>
    // BlankNumbers<DontBlank>
    // BlankZero<No>
    // SignDisplacement<0>
    // AutoFormatType<0>
    // AutoFormatExpr<>
    // CaptionClass<>
    // Editable<Yes>
    // MinValue<>
    // MaxValue<>
    // NotBlank<No>
    // ValuesAllowed<>
    // TableRelation<Undefined>
    // ValidateTableRelation<Yes>
    // TestTableRelation<Yes>
    // ExtendedDatatype<None>
    // 
    // //sett í aðgerð 5763
    //   //eymi 11.02.2015 - stofna fylgibréf
    //   IF "Source Document" = "Source Document"::"Sales Order" THEN BEGIN
    //     SalesInvHeader."No." := SalesHeader."Last Posting No.";
    //     TranportWaybill.CreateTB2(SalesInvHeader);
    //   END;
    // 
    // //setja Page Action í söluhaus, sölureikn etc
    //   TranportWaybill.CreateTB(2)(SalesInvHeader);
    //   TranportWaybill.CreateTBSP(SalesHeader);

    CaptionML = ENU='Transport Waybill',
                ISL='Fylgibréf';

    fields
    {
        field(1;"Document Type";Option)
        {
            CaptionML = ENU='Document Type',
                        ISL='Tegund fylgiskjals';
            OptionCaptionML = ENU='Quote,Order,Invoice,Shipment,Posted Invoice,Transfer Shipment,Posted Whs. Shipment',
                              ISL='Tilboð,Pöntun,Reikningur,Afhending,Bókaður reikningur,Millifærsla,Bókuð vöruhúsaafhending';
            OptionMembers = Quote,"Order",Invoice,Shipment,"Posted Invoice","Transfer Shipment","Posted Whs. Shipment";
        }
        field(2;"Document No.";Code[20])
        {
            CaptionML = ENU='Document No.',
                        ISL='Sendingarnúmer';
        }
        field(3;"No.";Code[20])
        {
            CaptionML = ENU='No.',
                        ISL='Nr.';

            trigger OnValidate();
            begin
                if "No." <> xRec."No." then begin
                  SalesSetup.GET;
                  NoSeriesMgt.TestManual(GetNoSeriesCode);
                  "No. Series" := '';
                end;
            end;
        }
        field(4;"Bill-to Customer No.";Code[20])
        {
            CaptionML = ENU='Bill-to Customer No.',
                        ISL='Reikn.færist á viðskm.';
            NotBlank = true;
            TableRelation = Customer;

            trigger OnValidate();
            var
                ContBusRel : Record "Contact Business Relation";
                Cont : Record Contact;
            begin

                if Status <> 3 then
                  TESTFIELD(Status,Status::Open);
                if (xRec."Bill-to Customer No." <> "Bill-to Customer No.") and
                   (xRec."Bill-to Customer No." <> '')
                then begin
                  if HideValidationDialog then
                    Confirmed := true
                  else
                    Confirmed := CONFIRM(Text004,false,FIELDCAPTION("Bill-to Customer No."));
                  if Confirmed then begin
                    SalesLine.SETRANGE("Document Type","Document Type");
                    SalesLine.SETRANGE("Document No.","No.");


                    SalesLine.RESET
                  end else begin
                    "Bill-to Customer No." := xRec."Bill-to Customer No.";
                  end;
                end;

                Cust.GET ("Bill-to Customer No.");
                Cust.CheckBlockedCustOnDocs(Cust,"Document Type",false,false);
                Cust.TESTFIELD("Customer Posting Group");

                "Bill-to Name" := Cust.Name;
                "Bill-to Name 2" := Cust."Name 2";
                "Bill-to Address" := Cust.Address;
                "Bill-to Address 2" := Cust."Address 2";
                "Bill-to City" := Cust.City;
                "Bill-to Post Code" := Cust."Post Code";
                "Bill-to County" := Cust.County;
                "Bill-to Country Code" := Cust."Country/Region Code";
                if not SkipBillToContact then
                  "Bill-to Contact" := Cust.Contact;


                "Salesperson Code" := Cust."Salesperson Code";
            end;
        }
        field(5;"Bill-to Name";Text[50])
        {
            CaptionML = ENU='Bill-to Name',
                        ISL='Reikningsfærslunafn';
        }
        field(6;"Bill-to Name 2";Text[50])
        {
            CaptionML = ENU='Bill-to Name 2',
                        ISL='Reikningsfærslunafn 2';
        }
        field(7;"Bill-to Address";Text[50])
        {
            CaptionML = ENU='Bill-to Address',
                        ISL='Reikningsfærsluaðsetur';
        }
        field(8;"Bill-to Address 2";Text[50])
        {
            CaptionML = ENU='Bill-to Address 2',
                        ISL='Reikningsfærsluaðsetur 2';
        }
        field(9;"Bill-to City";Text[50])
        {
            CaptionML = ENU='Bill-to City',
                        ISL='Reikningsfærslubær';

            trigger OnLookup();
            begin
                //PostCode.LookUpCity("Bill-to City","Bill-to Post Code",TRUE);
            end;
        }
        field(10;"Bill-to Contact";Text[50])
        {
            CaptionML = ENU='Bill-to Contact',
                        ISL='Reikningsfærslutengiliður';
        }
        field(11;"Your Reference";Text[30])
        {
            CaptionML = ENU='Your Reference',
                        ISL='Tilvísun yðar';
        }
        field(12;"Ship-to Code";Code[20])
        {
            CaptionML = ENU='Ship-to Code',
                        ISL='Sendist-til - kóti';
            TableRelation = "Ship-to Address".Code WHERE ("Customer No."=FIELD("Bill-to Customer No."));

            trigger OnValidate();
            begin

                if "Ship-to Code" <> '' then begin
                  if xRec."Ship-to Code" <> '' then
                  begin
                    Cust.GET("Bill-to Customer No.");
                    if Cust."Location Code" <> '' then
                      VALIDATE("Ship-From Location",Cust."Location Code");
                  end;
                  ShipToAddr.GET("Bill-to Customer No.","Ship-to Code");
                  "Ship-to Name" := ShipToAddr.Name;
                  "Ship-to Name 2" := ShipToAddr."Name 2";
                  "Ship-to Address" := ShipToAddr.Address;
                  "Ship-to Address 2" := ShipToAddr."Address 2";
                  "Ship-to City" := ShipToAddr.City;
                  "Ship-to Post Code" := ShipToAddr."Post Code";
                  "Ship-to County" := ShipToAddr.County;
                  VALIDATE("Ship-to Country Code",ShipToAddr."Country/Region Code");
                  "Ship-to Contact" := ShipToAddr.Contact;
                  "Shipment Method Code" := ShipToAddr."Shipment Method Code";
                  if ShipToAddr."Location Code" <> '' then
                    VALIDATE("Ship-From Location",ShipToAddr."Location Code");
                  "Shipping Agent Code" := ShipToAddr."Shipping Agent Code";
                  "Shipping Agent Service Code" := ShipToAddr."Shipping Agent Service Code";
                end else begin
                  Cust.GET("Bill-to Customer No.");
                  "Ship-to Name" := Cust.Name;
                  "Ship-to Name 2" := Cust."Name 2";
                  "Ship-to Address" := Cust.Address;
                  "Ship-to Address 2" := Cust."Address 2";
                  "Ship-to City" := Cust.City;
                  "Ship-to Post Code" := Cust."Post Code";
                  "Ship-to County" := Cust.County;
                  VALIDATE("Ship-to Country Code",Cust."Country/Region Code");
                  "Ship-to Contact" := Cust.Contact;
                  "Shipment Method Code" := Cust."Shipment Method Code";
                  if Cust."Location Code" <> '' then
                    VALIDATE("Ship-From Location",Cust."Location Code");
                  "Shipping Agent Code" := Cust."Shipping Agent Code";
                  "Shipping Agent Service Code" := Cust."Shipping Agent Service Code";
                end;
                GetShippingTime(FIELDNO("Ship-to Code"));
            end;
        }
        field(13;"Ship-to Name";Text[50])
        {
            CaptionML = ENU='Ship-to Name',
                        ISL='Sendist-til - heiti';
        }
        field(14;"Ship-to Name 2";Text[50])
        {
            CaptionML = ENU='Ship-to Name 2',
                        ISL='Sendist-til - heiti 2';
        }
        field(15;"Ship-to Address";Text[50])
        {
            CaptionML = ENU='Ship-to Address',
                        ISL='Sendist-til - aðsetur';
        }
        field(16;"Ship-to Address 2";Text[50])
        {
            CaptionML = ENU='Ship-to Address 2',
                        ISL='Sendist-til - aðsetur 2';
        }
        field(17;"Ship-to City";Text[50])
        {
            CaptionML = ENU='Ship-to City',
                        ISL='Sendist-til - bær';

            trigger OnLookup();
            begin
                //PostCode.LookUpCity("Ship-to City","Ship-to Post Code",TRUE);
            end;

            trigger OnValidate();
            begin
                //IF "Date Received" = 0D THEN
                //  PostCode.ValidateCity("Ship-to City","Ship-to Post Code",' ','',true);
            end;
        }
        field(18;"Ship-to Contact";Text[50])
        {
            CaptionML = ENU='Ship-to Contact',
                        ISL='Sendist-til - tengiliður';
        }
        field(20;"Posting Date";Date)
        {
            CaptionML = ENU='Posting Date',
                        ISL='Bókunardags.';

            trigger OnValidate();
            var
                NoSeries : Record "No. Series";
            begin
                /*
                IF ("Posting No." <> '') AND ("Posting No. Series" <> '') THEN BEGIN
                  NoSeries.GET("Posting No. Series");
                  IF NoSeries."Date Order" THEN
                    ERROR(
                      Text045,
                      FIELDCAPTION("Posting Date"),FIELDCAPTION("Posting No. Series"),"Posting No. Series",
                      NoSeries.FIELDCAPTION("Date Order"),NoSeries."Date Order","Document Type",
                      FIELDCAPTION("Posting No."),"Posting No.");
                END;
                */
                VALIDATE("Document Date","Posting Date");

            end;
        }
        field(21;"Shipment Date";Date)
        {
            CaptionML = ENU='Shipment Date',
                        ISL='Afh.dags';

            trigger OnValidate();
            begin
                UpdateLines(FIELDCAPTION("Shipment Date"),CurrFieldNo <> 0);
            end;
        }
        field(27;"Shipment Method Code";Code[10])
        {
            CaptionML = ENU='Shipment Method Code',
                        ISL='Kóti afhendingarmáta';
            TableRelation = "Shipment Method";

            trigger OnValidate();
            begin
                if Status <> 3 then
                  TESTFIELD(Status,Status::Open);
            end;
        }
        field(28;"Ship-From Location";Code[10])
        {
            CaptionML = ENU='Ship-From Location',
                        ISL='Afhendingarstaður';
            TableRelation = Location WHERE ("Use As In-Transit"=CONST(false));

            trigger OnValidate();
            begin
                if Status <> 3 then
                  TESTFIELD(Status,Status::Open);
                if ("Ship-From Location" <> xRec."Ship-From Location") and
                  (xRec."Bill-to Customer No." = "Bill-to Customer No.")
                then
                  MessageIfLinesExist(FIELDCAPTION("Ship-From Location"));
            end;
        }
        field(29;"Shortcut Dimension 1 Code";Code[20])
        {
            CaptionClass = '1,2,1';
            CaptionML = ENU='Shortcut Dimension 1 Code',
                        ISL='Flýtivídd 1 - kóti';
            TableRelation = "Dimension Value".Code WHERE ("Global Dimension No."=CONST(1));
        }
        field(30;"Shortcut Dimension 2 Code";Code[20])
        {
            CaptionClass = '1,2,2';
            CaptionML = ENU='Shortcut Dimension 2 Code',
                        ISL='Flýtivídd 2 - kóti';
            TableRelation = "Dimension Value".Code WHERE ("Global Dimension No."=CONST(2));
        }
        field(43;"Salesperson Code";Code[10])
        {
            CaptionML = ENU='Salesperson Code',
                        ISL='Kóti sölumanns';
            TableRelation = "Salesperson/Purchaser";
        }
        field(46;Comment;Boolean)
        {
            CalcFormula = Exist("Sales Comment Line" WHERE ("Document Type"=FIELD("Document Type"),
                                                            "No."=FIELD("No.")));
            CaptionML = ENU='Comment',
                        ISL='Athugasemd';
            Editable = false;
            FieldClass = FlowField;
        }
        field(51;"On Hold";Code[3])
        {
            CaptionML = ENU='On Hold',
                        ISL='Bið';
        }
        field(62;"Shipping No.";Code[20])
        {
            CaptionML = ENU='Shipping No.',
                        ISL='Afhendinga nr.';
        }
        field(77;"Transport Method";Code[10])
        {
            CaptionML = ENU='Transport Method',
                        ISL='Flutningsmáti';
            TableRelation = "Transport Method";

            trigger OnValidate();
            begin
                UpdateLines(FIELDCAPTION("Transport Method"),false);
            end;
        }
        field(85;"Bill-to Post Code";Code[20])
        {
            CaptionML = ENU='Bill-to Post Code',
                        ISL='Reikningsfærslupóstnúmer';
            TableRelation = "Post Code";
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;

            trigger OnLookup();
            begin
                //PostCode.LookUpPostCode("Bill-to City","Bill-to Post Code",TRUE);
            end;

            trigger OnValidate();
            begin
                //IF "Date Received" = 0D THEN
                //  PostCode.ValidatePostCode("Bill-to City","Bill-to Post Code");
            end;
        }
        field(86;"Bill-to County";Text[30])
        {
            CaptionML = ENU='Bill-to County',
                        ISL='Reikningsfærslusýsla';
        }
        field(87;"Bill-to Country Code";Code[10])
        {
            CaptionML = ENU='Bill-to Country Code',
                        ISL='Reikningsfærslulandskóti';
            TableRelation = "Country/Region";
        }
        field(91;"Ship-to Post Code";Code[20])
        {
            CaptionML = ENU='Ship-to Post Code',
                        ISL='Sendist-til - póstnúmer';
            TableRelation = "Post Code";
            //This property is currently not supported
            //TestTableRelation = false;
            ValidateTableRelation = false;

            trigger OnLookup();
            begin
                //PostCode.LookUpPostCode("Ship-to City","Ship-to Post Code",TRUE);
            end;

            trigger OnValidate();
            begin
                //IF "Date Received" = 0D THEN
                //  PostCode.ValidatePostCode("Ship-to City","Ship-to Post Code");
            end;
        }
        field(92;"Ship-to County";Text[30])
        {
            CaptionML = ENU='Ship-to County',
                        ISL='Sendist-til - sýsla';
        }
        field(93;"Ship-to Country Code";Code[10])
        {
            CaptionML = ENU='Ship-to Country Code',
                        ISL='Sendist-til - landskóti';
            TableRelation = "Country/Region";
        }
        field(97;"Exit Point";Code[10])
        {
            CaptionML = ENU='Exit Point',
                        ISL='Brottfararstaður';
            TableRelation = "Entry/Exit Point";

            trigger OnValidate();
            begin
                UpdateLines(FIELDCAPTION("Exit Point"),false);
            end;
        }
        field(99;"Document Date";Date)
        {
            CaptionML = ENU='Document Date',
                        ISL='Dags. fylgiskjals';
        }
        field(100;"External Document No.";Code[20])
        {
            CaptionML = ENU='External Document No.',
                        ISL='Númer utanaðk. skjals';
        }
        field(105;"Shipping Agent Code";Code[10])
        {
            CaptionML = ENU='Shipping Agent Code',
                        ISL='Flutningsaðilakóti';
            TableRelation = "Shipping Agent";

            trigger OnValidate();
            var
                SHagent : Record "Shipping Agent";
            begin
                if Status <> 3 then
                  TESTFIELD(Status,Status::Open);
                //IF xRec."Shipping Agent Code" = "Shipping Agent Code" THEN
                //  EXIT;

                "Shipping Agent Service Code" := '';
                GetShippingTime(FIELDNO("Shipping Agent Code"));
                UpdateLines(FIELDCAPTION("Shipping Agent Code"),CurrFieldNo<>0);

                if SHagent.GET("Shipping Agent Code") then begin
                  //"Transport Method" := SHagent."Transport Method";
                  "Shiping Agent Name" := SHagent.Name;
                  //"Account Type" := SHagent."Account Type";
                  //"Acc. No." := SHagent."Acc. No.";
                end;
            end;
        }
        field(107;"No. Series";Code[10])
        {
            CaptionML = ENU='No. Series',
                        ISL='Númeraröð';
            Editable = false;
            TableRelation = "No. Series";
        }
        field(120;Status;Option)
        {
            CaptionML = ENU='Status',
                        ISL='Staða';
            Editable = false;
            OptionCaptionML = ENU='Open,Released,Finished,Cancelled',
                              ISL='Opin,Útgefin,Lokið,Hætt við';
            OptionMembers = Open,Released,Finished,Cancelled;
        }
        field(5752;"Completely Shipped";Boolean)
        {
            CaptionML = ENU='Completely Shipped',
                        ISL='Afhent á bíl';
            Editable = true;
        }
        field(5790;"Requested Delivery Date";Date)
        {
            CaptionML = ENU='Requested Delivery Date',
                        ISL='Umbeðin afgreiðsludagsetning';

            trigger OnValidate();
            begin
                if Status <> 3 then
                  TESTFIELD(Status,Status::Open);
                if "Promised Delivery Date" <> 0D then
                  ERROR(
                    Text028,
                    FIELDCAPTION("Requested Delivery Date"),
                    FIELDCAPTION("Promised Delivery Date"));

                if "Requested Delivery Date" <> xRec."Requested Delivery Date" then
                  UpdateLines(FIELDCAPTION("Requested Delivery Date"),CurrFieldNo<>0);
            end;
        }
        field(5791;"Promised Delivery Date";Date)
        {
            CaptionML = ENU='Promised Delivery Date',
                        ISL='Lofuð afgreiðsludagsetning';

            trigger OnValidate();
            begin
                if Status <> 3 then
                  TESTFIELD(Status,Status::Open);
                if "Promised Delivery Date" <> xRec."Promised Delivery Date" then
                  UpdateLines(FIELDCAPTION("Promised Delivery Date"),CurrFieldNo<>0);
            end;
        }
        field(5792;"Shipping Time";DateFormula)
        {
            CaptionML = ENU='Shipping Time',
                        ISL='Afhendingartími';

            trigger OnValidate();
            begin
                if Status <> 3 then
                  TESTFIELD(Status,Status::Open);
                if "Shipping Time" <> xRec."Shipping Time" then
                  UpdateLines(FIELDCAPTION("Shipping Time"),CurrFieldNo<>0);
            end;
        }
        field(5794;"Shipping Agent Service Code";Code[10])
        {
            CaptionML = ENU='Shipping Agent Service Code',
                        ISL='Flutningaþjónustukóti';
            TableRelation = "Shipping Agent Services".Code WHERE ("Shipping Agent Code"=FIELD("Shipping Agent Code"));

            trigger OnValidate();
            begin
                if Status <> 3 then
                  TESTFIELD(Status,Status::Open);
                GetShippingTime(FIELDNO("Shipping Agent Service Code"));
                UpdateLines(FIELDCAPTION("Shipping Agent Service Code"),CurrFieldNo<>0);
            end;
        }
        field(50013;"Shiping Agent Name";Text[30])
        {
            CaptionML = ENU='Shiping Agent Name',
                        ISL='Nafn flutningsaðila';
        }
        field(50018;"Shiping Agent Contact";Text[30])
        {
            CaptionML = ENU='Shiping Agent Contact',
                        ISL='Tengiliður flutningsaðila';
        }
        field(50019;"Ship-to Customer No.";Code[20])
        {
            CaptionML = ENU='Bill-to Customer No.',
                        ISL='Reikn.færist á viðskm.';
            NotBlank = true;
            TableRelation = Customer;

            trigger OnValidate();
            var
                ContBusRel : Record "Contact Business Relation";
                Cont : Record Contact;
            begin
                if Status <> 3 then
                  TESTFIELD(Status,Status::Open);
                if (xRec."Ship-to Customer No." <> "Ship-to Customer No.") and
                   (xRec."Ship-to Customer No." <> '')
                then begin
                  if HideValidationDialog then
                    Confirmed := true
                  else
                    Confirmed := CONFIRM(Text004,false,FIELDCAPTION("Ship-to Customer No."));
                  if Confirmed then begin
                    SalesLine.SETRANGE("Document Type","Document Type");
                    SalesLine.SETRANGE("Document No.","No.");

                    SalesLine.RESET
                  end else begin
                    "Ship-to Customer No." := xRec."Ship-to Customer No.";
                  end;
                end;

                Cust.GET ("Ship-to Customer No.");
                Cust.CheckBlockedCustOnDocs(Cust,"Document Type",false,false);
                Cust.TESTFIELD("Customer Posting Group");


                "Ship-to Name" := Cust.Name;
                "Ship-to Name 2" := Cust."Name 2";
                "Ship-to Address" := Cust.Address;
                "Ship-to Address 2" := Cust."Address 2";
                "Ship-to City" := Cust.City;
                "Ship-to Post Code" := Cust."Post Code";
                "Ship-to County" := Cust.County;
                "Ship-to Country Code" := Cust."Country/Region Code";
                "Ship-to Contact" := Cust.Contact;
                if Cust."Location Code" <> '' then
                 "Ship-To Location" := Cust."Location Code";

                "Salesperson Code" := Cust."Salesperson Code";
            end;
        }
        field(50026;"Ship-To Location";Code[10])
        {
            CaptionML = ENU='Ship-To Location',
                        ISL='Móttökustaður';
            TableRelation = Location WHERE ("Use As In-Transit"=CONST(false));

            trigger OnValidate();
            begin
                if Status <> 3 then
                  TESTFIELD(Status,Status::Open);
                MessageIfLinesExist(FIELDCAPTION("Ship-To Location"));
            end;
        }
        field(59241;"Account Type";Option)
        {
            CaptionML = ENU='Account Type',
                        ISL='Tegund reiknings';
            OptionCaptionML = ENU='Customer,Vendor',
                              ISL='Viðskiptamaður,Lánardrottinn';
            OptionMembers = Customer,Vendor;
        }
        field(59242;"Acc. No.";Code[20])
        {
            CaptionML = ENU='Account No.',
                        ISL='Flutningsaðili';
            TableRelation = IF ("Account Type"=CONST(Customer)) Customer
                            ELSE IF ("Account Type"=CONST(Vendor)) Vendor;
        }
        field(59243;"Delivery Location";Code[10])
        {
            CaptionML = ENU='Delivery Location',
                        ISL='Afhleðslustaður';
            TableRelation = Location WHERE ("Use As In-Transit"=CONST(false));

            trigger OnValidate();
            begin
                if Status <> 3 then
                  TESTFIELD(Status,Status::Open);
                MessageIfLinesExist(FIELDCAPTION("Ship-To Location"));
            end;
        }
        field(59244;"Shipping Location";Code[10])
        {
            CaptionML = ENU='Shipping Location',
                        ISL='Hleðslustaður';
            TableRelation = Location WHERE ("Use As In-Transit"=CONST(false));

            trigger OnValidate();
            begin
                if Status <> 3 then
                  TESTFIELD(Status,Status::Open);
                MessageIfLinesExist(FIELDCAPTION("Ship-To Location"));
            end;
        }
        field(59245;"Post Giro";Boolean)
        {
            CaptionML = ENU='Post Giro',
                        ISL='Gíirókrafa';
        }
        field(59246;"Post Giro Amount";Decimal)
        {
            CaptionML = ENU='Post Giro Amount',
                        ISL='Upphæð grírókröfu';
        }
        field(59247;Fargile;Boolean)
        {
            CaptionML = ENU='Item Treatment',
                        ISL='Brothætt';
        }
        field(59248;"Item Type";Option)
        {
            CaptionML = ENU='Item Type',
                        ISL='Gerð vöru';
            OptionCaptionML = ENU='Normal,Chemical,Freeze,Cooler',
                              ISL='Þurrvara,Efnavara,Frystivara,Kælivara';
            OptionMembers = Normal,Chemical,Freeze,Cooler;
        }
        field(59249;"Source Document";Code[20])
        {
            Caption = 'Fylgiskjal uppruna';
        }
        field(59250;"Paid by sender";Boolean)
        {
            Caption = 'Sendandi greiðir';
        }
        field(59251;"Total Units";Decimal)
        {
            CalcFormula = Sum("Transport Line".Quantity WHERE ("Document No."=FIELD("Document No.")));
            Caption = 'Einingar alls';
            Description = 'Einingar í sendingu';
            Editable = false;
            FieldClass = FlowField;
        }
        field(59252;"Total Volume";Decimal)
        {
            CalcFormula = Sum("Transport Line"."Unit Volume" WHERE ("Document No."=FIELD("Document No.")));
            Caption = 'Rúmmál alls';
            Editable = false;
            FieldClass = FlowField;
        }
        field(59253;"Total Gross Weight";Decimal)
        {
            CalcFormula = Sum("Transport Line"."Gross Weight" WHERE ("Document No."=FIELD("Document No.")));
            CaptionML = ENU='Gross Weight',
                        ISL='Brúttóþyngd alls';
            DecimalPlaces = 0:5;
            Editable = false;
            FieldClass = FlowField;
        }
        field(59254;"Total Net Weight";Decimal)
        {
            CalcFormula = Sum("Transport Line"."Net Weight" WHERE ("Document No."=FIELD("Document No.")));
            CaptionML = ENU='Net Weight',
                        ISL='Nettóþyngd alls';
            DecimalPlaces = 0:5;
            Editable = false;
            FieldClass = FlowField;
        }
        field(59255;"Total Units per Parcel";Decimal)
        {
            CalcFormula = Sum("Transport Line"."Units per Parcel" WHERE ("Document No."=FIELD("Document No.")));
            CaptionML = ENU='Units per Parcel',
                        ISL='Fjöldi í pakkningu alls';
            DecimalPlaces = 0:5;
            Description = 'Fj í pakkn';
            Editable = false;
            FieldClass = FlowField;
        }
        field(59256;"Units - Transporter";Decimal)
        {
            Caption = 'Einingar farmflytjanda';
        }
        field(59257;"Total Volume - Transporter";Decimal)
        {
            Caption = 'Rúmmál alls farmflytjanda';
            Editable = true;
        }
        field(59258;"Total Gross Weight - Transp.";Decimal)
        {
            CaptionML = ENU='Gross Weight',
                        ISL='Brúttóþyngd alls farmflytjanda';
            DecimalPlaces = 0:5;
            Editable = true;
        }
        field(59259;"Total Net Weight - Transp.";Decimal)
        {
            CaptionML = ENU='Net Weight',
                        ISL='Nettóþyngd alls farmflytjanda';
            DecimalPlaces = 0:5;
            Editable = true;
        }
        field(59260;"Dangerous Item";Boolean)
        {
            Caption = 'Hættuleg vara';
        }
        field(59261;"Fast Delivery";Boolean)
        {
            Caption = 'Hraðsending';
        }
        field(59262;"Total Amount - Transp.";Decimal)
        {
            Caption = 'Sendingarkostn. farmflytjanda';
        }
        field(59263;"SSCC Reference";Code[20])
        {
            CaptionML = ENU='SSCC Reference',
                        ISL='SSCC strikamerki';
            Description = 'Serial Shipping Container Code';
            NotBlank = false;
            TableRelation = "SSCC Ref.";

            trigger OnLookup();
            var
                //ssccBarcodeReport : Report "SSCC barcode";
                ssccMsg3 : Label 'Viltu prenta þetta SSCC strikamerki?';
            begin
                SSCCrefREC.SETCURRENTKEY("Document No.");
                SSCCrefREC.SETFILTER("Document No.",'=%1|%2','',"Document No.");
                if not SSCCrefREC.FIND('-') then
                  SSCCrefREC.CreateBarCode(1,false);
                COMMIT;
                SSCCrefREC.FILTERGROUP(2);
                if PAGE.RUNMODAL(/*PAGE::"SSCC Reference List"*/0,SSCCrefREC) = ACTION::LookupOK then begin
                  "SSCC Reference" := SSCCrefREC."SSCC Reference";
                  SSCCrefREC.SETRANGE("SSCC Reference",SSCCrefREC."SSCC Reference");
                  /*
                  ssccBarcodeReport.USEREQUESTFORM(FALSE);
                  ssccBarcodeReport.SETTABLEVIEW(SSCCrefREC);
                  IF CONFIRM(ssccMsg3,TRUE) THEN
                    ssccBarcodeReport.RUN;
                  */
                  SSCCrefREC.FIND('+');
                  VALIDATE("SSCC Reference");
                  MODIFY;
                end;
                SSCCrefREC.RESET;

            end;

            trigger OnValidate();
            var
                ssccMsg : TextConst ENU='This Bar-Code has not been printed!',ISL='Það á eftir að prenta út þetta strikamerki!';
                ssccMsg2 : TextConst ENU='This Bar-Code belongs to another Transport!',ISL='Þetta strikamerki tilheyrir annarri sendingu!';
                ssccMsg3 : Label 'Viltu prenta þetta SSCC strikamerki?';
            begin
                if "SSCC Reference" <> xRec."SSCC Reference" then begin
                  if SSCCrefREC.GET(xRec."SSCC Reference") then begin
                    SSCCrefREC."Document No." := '';
                    SSCCrefREC.MODIFY;
                  end;
                end;

                if "SSCC Reference" <> '' then begin
                  SSCCrefREC.GET("SSCC Reference");
                  if CurrFieldNo = FIELDNO("SSCC Reference") then
                    //IF SSCCrefREC.Printed = 0 THEN
                    //  ERROR(ssccMsg);
                  if (SSCCrefREC."Document No." <> '') and (SSCCrefREC."Document No." <> "SSCC Reference") then
                    ERROR(ssccMsg2);
                  SSCCrefREC."Document No." := "Document No.";
                  SSCCrefREC.MODIFY;
                end;
            end;
        }
        field(59901;"Total Units Input";Decimal)
        {
            Caption = 'Einingar alls innsl.';
            Description = 'Innslegið';
            Editable = false;
        }
        field(59902;"Total Volume Input";Decimal)
        {
            Caption = 'Rúmmál alls innsl.';
            Description = 'Innslegið';
            Editable = false;
        }
        field(59903;"Total Gross Weight Input";Decimal)
        {
            CaptionML = ENU='Gross Weight',
                        ISL='Brúttóþyngd alls innsl.';
            DecimalPlaces = 0:5;
            Description = 'Innslegið';
            Editable = false;
        }
        field(59904;"Deliver Now";Boolean)
        {
            CaptionML = ENU='Deliver Now',
                        ISL='Afhenda núna';
            InitValue = true;
        }
        field(59905;"Shipping List Printed";Integer)
        {
            CaptionML = ENU='Shipping List Printed',
                        ISL='Prentun afgr.seðils';
        }
        field(59906;Collie;Integer)
        {
            CalcFormula = Count("SSCC Ref." WHERE ("Document No."=FIELD("Document No.")));
            CaptionML = ENU='Collie',
                        ISL='Fjöldi kollía';
            Editable = false;
            FieldClass = FlowField;
        }
        field(99008500;"Date Received";Date)
        {
            CaptionML = ENU='Date Received',
                        ISL='Dagsetning móttöku';
        }
        field(99008501;"Time Received";Time)
        {
            CaptionML = ENU='Time Received',
                        ISL='Tími móttöku';
        }
        field(99008509;"Date Sent";Date)
        {
            CaptionML = ENU='Date Sent',
                        ISL='Dags. sendingar';
        }
        field(99008510;"Time Sent";Time)
        {
            CaptionML = ENU='Time Sent',
                        ISL='Hvenær sent';
        }
    }

    keys
    {
        key(Key1;"Document No.")
        {
        }
        key(Key2;Status)
        {
        }
        key(Key3;"Document Type","Source Document")
        {
        }
    }

    fieldgroups
    {
    }

    trigger OnDelete();
    begin

        SalesLine.SETRANGE("Document No.","Document No.");
        SalesLine.DELETEALL(true);
        SSCCrefREC.SETRANGE("Document No.","Document No.");
        SSCCrefREC.MODIFYALL("Document No.",'');
    end;

    trigger OnInsert();
    begin
        SalesSetup.GET;

        if "Document No." = '' then begin
          TestNoSeries;
          NoSeriesMgt.InitSeries(GetNoSeriesCode,xRec."No. Series","Posting Date","Document No.","No. Series");
        end;

        InitRecord;
    end;

    trigger OnRename();
    begin

        if Status <> 3 then
          TESTFIELD(Status,Status::Open);
    end;

    var
        Text004 : TextConst ENU='Do you want to change %1?',ISL='Á að breyta %1?';
        Text006 : TextConst ENU='You cannot change %1 because the order is associated with one or more purchase orders.',ISL='Ekki er hægt að breyta %1 því pöntunin tengist einni eða fleiri innkaupapöntunum.';
        Text031 : TextConst ENU='You have modified %1.\\',ISL='Notandi hefur breytt %1.\\';
        Text032 : TextConst ENU='Do you want to update the lines?',ISL='Á að uppfæra línurnar?';
        Text018 : TextConst ENU='You have changed %1 on the sales header, but it has not been changed on the existing sales lines.\',ISL='%1 hefur verið breytt í sendingarhausnum, en því hefur ekki verið breytt í línum sem fyrir eru.\';
        Text019 : TextConst ENU='You must update the existing sales lines manually.',ISL='Uppfæra verður þær sendingarlínur sem til eru handvirkt.';
        Text028 : TextConst ENU='You cannot change the %1 when the %2 has been filled in.',ISL='Ekki er hægt að breyta %1 þegar %2 hefur verið slegið inn.';
        SendTXT : TextConst ENU='File sent by using Navision',ISL='Skrá send með Navision';
        SendTXT2 : Label 'Skrá móttekinn frá farmflytjanda';
        SalesSetup : Record "Transport Setup";
        SalesHeader : Record "Transport Waybill SAM";
        SalesLine : Record "Transport Line";
        Cust : Record Customer;
        PostCode : Record "Post Code";
        Location : Record Location;
        ShippingAgentService : Record "Shipping Agent Services";
        ShipToAddr : Record "Ship-to Address";
        DocDim : Integer;
        NoSeriesMgt : Codeunit NoSeriesManagement;
        DimMgt : Codeunit DimensionManagement;
        Confirmed : Boolean;
        SkipSellToContact : Boolean;
        SkipBillToContact : Boolean;
        HideValidationDialog : Boolean;
        SSCCrefREC : Record "SSCC Ref.";
        QTYgl : Decimal;
        VOLgl : Decimal;
        Weigl : Decimal;
        ExternalInputx : Boolean;
        PSshipmExtraFilter : Text[250];
        fjoldiPakka : Integer;
        "gvÞyngd" : Decimal;
        "Textalína1" : Text[25];
        "Textalína2" : Text[25];
        Magn1 : Integer;
        Magn2 : Integer;
        gvType : Option Sales,Transfer;

    local procedure TestNoSeries() : Boolean;
    begin
        SalesSetup.TESTFIELD("Shipping No. Series");
    end;

    procedure InitRecord();
    begin
        NoSeriesMgt.SetDefaultSeries("No. Series",SalesSetup."Shipping No. Series");

        "Shipment Date" := WORKDATE;

        "Posting Date" := WORKDATE;
        "Document Date" := WORKDATE;
    end;

    procedure AssistEdit(OldSalesHeader : Record "Transport Waybill SAM") : Boolean;
    begin
        with SalesHeader do begin
          SalesHeader := Rec;
          SalesSetup.GET;
          TestNoSeries;
          if NoSeriesMgt.SelectSeries(GetNoSeriesCode,OldSalesHeader."No. Series","No. Series") then begin
            SalesSetup.GET;
            TestNoSeries;
            NoSeriesMgt.SetSeries("No.");
            Rec := SalesHeader;
            exit(true);
          end;
        end;
    end;

    local procedure GetNoSeriesCode() : Code[10];
    begin
        exit(SalesSetup."Shipping No. Series");
    end;

    procedure CreateDim(Type1 : Integer;No1 : Code[20];Type2 : Integer;No2 : Code[20];Type3 : Integer;No3 : Code[20];Type4 : Integer;No4 : Code[20];Type5 : Integer;No5 : Code[20];Type6 : Integer;No6 : Code[20]);
    var
        SourceCodeSetup : Record "Source Code Setup";
        TableID : array [10] of Integer;
        No : array [10] of Code[20];
    begin
        /*
        SourceCodeSetup.GET;
        TableID[1] := Type1;
        No[1] := No1;
        TableID[2] := Type2;
        No[2] := No2;
        TableID[3] := Type3;
        No[3] := No3;
        TableID[4] := Type4;
        No[4] := No4;
        TableID[5] := Type5;
        No[5] := No5;
        TableID[6] := Type6;
        No[6] := No6;
        "Shortcut Dimension 1 Code" := '';
        "Shortcut Dimension 2 Code" := '';
        DimMgt.GetDefaultDim(
          TableID,No,SourceCodeSetup.Sales,
          "Shortcut Dimension 1 Code","Shortcut Dimension 2 Code");
        IF "No." <> '' THEN
          DimMgt.UpdateDocDefaultDim(
            DATABASE::"Sales Header","Document Type","No.",0,
            "Shortcut Dimension 1 Code","Shortcut Dimension 2 Code");
        */

    end;

    procedure GetShippingTime(CalledByFieldNo : Integer);
    begin
        if (CalledByFieldNo <> CurrFieldNo) and (CurrFieldNo <> 0) then
          exit;

        if ShippingAgentService.GET("Shipping Agent Code","Shipping Agent Service Code") then
          "Shipping Time" := ShippingAgentService."Shipping Time"
        else begin
          if Cust.GET("Bill-to Customer No.") then;
          "Shipping Time" := Cust."Shipping Time"
        end;
        VALIDATE("Shipping Time");
    end;

    procedure UpdateLines(ChangedFieldName : Text[100];AskQuestion : Boolean);
    var
        Question : Text[250];
        UpdateLines : Boolean;
    begin
        if LinesExist and AskQuestion then begin
          Question := STRSUBSTNO(
            Text031 +
            Text032,ChangedFieldName);
          if not DIALOG.CONFIRM(Question,true) then
            exit
          else
            UpdateLines := true;
        end;
        if LinesExist then begin
          //DocDim.LOCKTABLE;
          SalesLine.LOCKTABLE;
          MODIFY;

          SalesLine.RESET;
          SalesLine.SETRANGE("Document Type","Document Type");
          SalesLine.SETRANGE("Document No.","No.");
          if SalesLine.FIND('-') then
            repeat
              case ChangedFieldName of
                FIELDCAPTION("Shipment Date") :
                if SalesLine."No." <> '' then
                  SalesLine.VALIDATE("Shipment Date","Shipment Date");
                FIELDCAPTION("Transport Method") :
                  SalesLine.VALIDATE("Transport Method","Transport Method");
                FIELDCAPTION("Exit Point") :
                  SalesLine.VALIDATE("Exit Point","Exit Point");
                FIELDCAPTION("Shipping Agent Code") :
                  SalesLine.VALIDATE("Shipping Agent Code","Shipping Agent Code");
                FIELDCAPTION("Shipping Agent Service Code") :
                  if SalesLine."No." <> '' then
                    SalesLine.VALIDATE("Shipping Agent Service Code","Shipping Agent Service Code");
                FIELDCAPTION("Shipping Time") :
                  if SalesLine."No." <> '' then
                    SalesLine.VALIDATE("Shipping Time","Shipping Time");
                FIELDCAPTION("Requested Delivery Date") :
                  if SalesLine."No." <> '' then
                    SalesLine.VALIDATE("Requested Delivery Date","Requested Delivery Date");
                FIELDCAPTION("Promised Delivery Date") :
                  if SalesLine."No." <> '' then
                    SalesLine.VALIDATE("Promised Delivery Date","Promised Delivery Date");
              end;
              SalesLine.MODIFY(true);
            until SalesLine.NEXT = 0;
        end;
    end;

    procedure LinesExist() : Boolean;
    begin
        SalesLine.RESET;
        SalesLine.SETRANGE("Document No.","Document No.");
        exit(SalesLine.FIND('-'));
    end;

    procedure MessageIfLinesExist(ChangedFieldName : Text[100]);
    begin
        if LinesExist and not HideValidationDialog then
          MESSAGE(
            Text018 +
            Text019,
            ChangedFieldName);
    end;

    procedure CreateTransportHead(DocType : Option " ",Quote,"Order",Invoice,Shipment,"Posted Invoice","Transfer Shipment Header";DocNo : Code[20];PaidSender : Boolean) : Boolean;
    var
        THrec : Record "Transport Waybill SAM";
        SH : Record "Sales Header";
        SHP : Record "Sales Invoice Header";
        SHS : Record "Sales Shipment Header";
        SHSP : Record "Posted Whse. Shipment Header";
        SL : Record "Sales Line";
        SLP : Record "Sales Invoice Line";
        SLS : Record "Sales Shipment Line";
        SLSP : Record "Posted Whse. Shipment Line";
        ItemR : Record Item;
        IOMrec : Record "Item Unit of Measure";
        text0001 : TextConst ENU='Transport Header %1 already exists! Do you want to continue?',ISL='Það er til sendingarhaus fyrir fylgiskjal %1! Viltu halda áfram?';
        CreateNew : Boolean;
        text0002 : TextConst ENU='Function aborted!',ISL='Hætt við aðgerð!';
        TrShipmentHeader : Record "Transfer Shipment Header";
        Location1 : Record Location;
        Location1Cust : Record Customer;
        Location2 : Record Location;
        SetupREC : Record "Company Information";
        SSCCrefREC : Record "SSCC Ref.";
        //SSCCreport : Report "SSCC barcode";
        TRsetup : Record "Transport Setup";
    begin
        

    end;


    
}

