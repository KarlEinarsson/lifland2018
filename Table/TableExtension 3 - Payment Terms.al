tableextension 3 "Payment Terms Extension" extends "Payment Terms"
{
    fields
    {
        field(88060;"Gírókrafa";Boolean)
        {
            CaptionML = ENU = 'Gírókrafa',
                        ISL = 'Gírókrafa';
            Description = 'PFS';                                  
        }
    }
}