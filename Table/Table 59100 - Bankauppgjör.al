table 59100 "Bankauppgjör"
{
    // version BSAM3.70,SB3.70


    fields
    {
        field(1;"Færslunúmer";Integer)
        {
            Editable = false;
        }
        field(2;"Tákn";Code[3])
        {
            Editable = false;
        }
        field(3;Banki;Code[4])
        {
            Editable = false;
        }
        field(4;"Höfuðbók";Code[2])
        {
            Editable = false;
        }
        field(5;"Reikningsnúmer";Code[6])
        {
            Editable = false;
        }
        field(6;"Færslulykill";Code[2])
        {
            Editable = false;
        }
        field(7;"Tilvísun";Code[16])
        {
        }
        field(8;Vaxtadagur;Code[2])
        {
            Editable = false;
        }
        field(9;"Seðilsnúmer";Code[7])
        {
            Editable = false;
        }
        field(10;Hreyfingardagur;Date)
        {
            Editable = false;
        }
        field(11;"Upphæð";Decimal)
        {
            Editable = false;
        }
        field(12;"Greiðslubanki";Code[4])
        {
            Editable = false;
        }
        field(13;"Bunkanúmer";Code[4])
        {
            Editable = false;
        }
        field(20;"Færslunúmer bankahreyfingar";Integer)
        {
            Editable = false;
        }
        field(21;"Búið að bóka";Boolean)
        {
            Editable = true;
        }
        field(22;"Staða afstemmingar";Option)
        {
            OptionMembers = "Óafstemmt",Villulisti,"Afstemmt sjálfvirkt","Afstemmt handvirkt";
        }
        field(23;"Kröfunúmer";Code[6])
        {
        }
        field(24;Vanskilagjald;Decimal)
        {
        }
        field(25;"Dráttarvextir";Decimal)
        {
        }
        field(26;"Fjármagnstekjuskattur";Decimal)
        {
        }
        field(27;"Bankakostnaður";Decimal)
        {
        }
        field(28;"Kennitala greiðanda";Code[10])
        {
        }
        field(10008150;Vaxtadagsetning;Date)
        {
            Description = 'LÍ XML';
        }
        field(10008151;Textalykill;Code[2])
        {
            Description = 'LÍ XML';
        }
        field(10008152;"Skýring textalykils";Text[30])
        {
            Description = 'LÍ XML';
        }
    }

    keys
    {
        key(Key1;"Færslunúmer")
        {
        }
        key(Key2;"Færslulykill","Búið að bóka")
        {
        }
        key(Key3;Banki,"Höfuðbók","Reikningsnúmer",Hreyfingardagur)
        {
            SumIndexFields = "Upphæð";
        }
        key(Key4;"Staða afstemmingar",Hreyfingardagur)
        {
        }
        key(Key5;"Staða afstemmingar",Banki,"Höfuðbók","Reikningsnúmer",Hreyfingardagur)
        {
        }
    }

    fieldgroups
    {
    }

    trigger OnDelete();
    begin
        //ERROR('Ekki má eyða færslum í þessa skrá');
    end;

    trigger OnInsert();
    begin
        ERROR('Ekki má færa inn færslur í þessa skrá');
    end;

    trigger OnModify();
    begin
        ERROR('Ekki má breyta færslum í þessa skrá');
    end;

    trigger OnRename();
    begin
        ERROR('Ekki má breyta færslum í þessa skrá');
    end;
}

