tableextension 27 "Item Extension" extends Item
{
    fields
    {
        field(50000;"Vendor description";Text[30])
        {
            CaptionML = ENU = 'Vendor description',
                        ISL = 'Vörulýsing lánardr.';
        }

        field(73900;"Show On Web";Boolean)
        {
            CaptionML = ENU = 'Show On Web',
                        ISL = 'Sýna á vef';
        }

    }

    trigger OnBeforeInsert();
    begin
        Validate(Blocked,true); // LIF-115
    end;  
}