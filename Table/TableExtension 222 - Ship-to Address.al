tableextension 222 "Ship-to Address Extension" extends "Ship-to Address"
{
    fields
    {
        field(50000;"Sendingarkostnaður tegund";Option)
        {
            CaptionML = ENU = 'Type',
                        ISL = 'Sendingarkostnaður tegund';
            OptionMembers = " ","G/L Account",Item,Resource,"Fixed Asset","Charge (Item)";
            OptionCaptionML = 	ENU = ' ,,Item,Resource',
                                ISL = ' ,,Vara,Forði';
        }

        field(50001;"Sendingarkostnaður";Code[20])
        {
            CaptionML = ENU = 'Sendingarkostnaður',
                        ISL = 'Sendingarkostnaður';
            TableRelation = IF ("Sendingarkostnaður tegund"=const(Item)) Item."No." ELSE IF ("Sendingarkostnaður tegund"=const(Resource)) Resource."No.";
        }

        field(50002;"Bin Code";Code[20])
        {
            CaptionML = ENU = 'Bin Code',
                        ISL = 'Hólfa Kóti';
        }
    }
}