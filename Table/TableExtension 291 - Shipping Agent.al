tableextension 291 "Shipping Agent Extension" extends "Shipping Agent"
{
    fields
    {
        field(50000;"Transport Waybill";Option)
        {
            CaptionML = ENU = 'Transport Waybill',
                        ISL = 'Transport Waybill';
            OptionMembers =  " ",Flytjandi,Landflutningar;
            OptionCaptionML = 	ENU = ' ,Flytjandi,Landflutningar',
                                ISL = ' ,Flytjandi,Landflutningar';
        }
    }
}