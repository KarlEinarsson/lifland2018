tableextension 5740 "Transfer Header" extends "Transfer Header"
{
    fields
    {
        field(50001;"Transfer-to Bin Code";Code[20])
        {
            CaptionML = ENU = '"Transfer-to Bin Code"',
                        ISL = 'Kóti hólfs sem millif. er í';
            TableRelation = if("Transfer-to Code"=filter(<>'')) Bin.Code WHERE ("Location Code"=FIELD("Transfer-to Code"));
        }
    } 
}