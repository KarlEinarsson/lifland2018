tableextension 37 "Sales Line Extension" extends "Sales Line"
{
    fields
    {
        field(60001;"Vehicle Bin";Code[10])
        {
            CaptionML = ENU = 'Vehicle Bin',
                        ISL = 'Hólf';
        }

        field(60002;Tank;Code[10])
        {
            CaptionML = ENU = 'Tank',
                        ISL = 'Tankur';
        }

        field(60003;"Silo at Customer side";Code[10])
        {
            CaptionML = ENU = 'Silo at Customer side',
                        ISL = 'Síló viðskiptamanns';
        }

        field(60004;"Silo date and time";DateTime)
        {
            CaptionML = ENU = 'Silo date and time',
                        ISL = 'Fóður afgreiðsludags og tími';
        }

        field(60005;"Ferð nr.";Integer)
        {
            CaptionML = ENU = 'Ferð nr.',
                        ISL = 'Ferð Nr.';
            InitValue = 1;
        }

        field(60006;"Lota";Code[10])
        {
            CaptionML = ENU = 'Lota',
                        ISL = 'Lota';
        }
    }
}