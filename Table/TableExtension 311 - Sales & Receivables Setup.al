tableextension 311 "Sales & Receivables Setup Ext" extends "Sales & Receivables Setup"
{
    fields
    {
        field(50011;"Confirm Changes for Customer";Boolean)
        {
            CaptionML = ENU = '"Confirm Changes for Customer"',
                        ISL = 'Staðfesta Viðskiptamannabreytingar';
        }
    }
}