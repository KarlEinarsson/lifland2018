table 80102 "TaxFree Setup"
{
    // version TaxFree


    fields
    {
        field(1;Type;Option)
        {
            InitValue = " ";
            OptionMembers = Setup," ";
        }
        field(2;"VAT %";Decimal)
        {
            MaxValue = 99;
            MinValue = 0;
        }
        field(3;"Sales Amount From";Decimal)
        {
        }
        field(4;"Sales Amount To";Decimal)
        {
        }
        field(5;"Refund Amount";Decimal)
        {
        }
        field(6;"Refund %";Decimal)
        {
            Description = 'ef upphæð fer uppfyrir hæsta gildi töflunnar';
            MaxValue = 99;
            MinValue = 0;
        }
        field(11;"Min Amount";Decimal)
        {
        }
    }

    keys
    {
        key(Key1;Type,"VAT %","Sales Amount From")
        {
        }
    }

    fieldgroups
    {
    }
}

