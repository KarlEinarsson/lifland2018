table 59246 "Chemical Group"
{
    // version SAME

    // Samskip Fylgibréf
    // EY 06.12.2011 -

    CaptionML = ENU='Chemical Group',
                ISL='Efnavöruflokkar';
    //DrillDownPageID = 59248;
    //LookupPageID = 59248;

    fields
    {
        field(1;"UN Code";Code[10])
        {
            CaptionML = ENU='UN Code',
                        ISL='UN númer';
        }
        field(2;Description;Text[80])
        {
            CaptionML = ENU='Description',
                        ISL='Lýsing';
        }
        field(3;"Document file and folder";Text[250])
        {
            CaptionML = ENU='Document file and folder',
                        ISL='Slóð og skráarheiti skjals';
        }
        field(4;"ADR Group";Text[10])
        {
            CaptionML = ENU='ADR Group',
                        ISL='ADR flokkur';
        }
        field(5;Letter;Text[10])
        {
            CaptionML = ENU='Letter',
                        ISL='Bókstafur';
        }
        field(6;"Fixed Code";Text[10])
        {
            CaptionML = ENU='Fixed Code',
                        ISL='Fasti';
            InitValue = 'ADR';
        }
        field(7;"Active Weight";Decimal)
        {
            CaptionML = ENU='Active Weight',
                        ISL='Viðmiðunarþyngd';
        }
        field(8;"Item No.";Code[20])
        {
            CaptionML = ENU='Item No.',
                        ISL='Vörunr.';
            TableRelation = Item;
        }
        field(9;"Packing Group";Code[10])
        {
            CaptionML = ENU='Packing Group',
                        ISL='Pökkunarflokkur';
        }
    }

    keys
    {
        key(Key1;"UN Code","Item No.")
        {
        }
    }

    fieldgroups
    {
    }

    procedure GetUNcodeREC(var UnCoderec : Record "Chemical Group");
    var
        UnREC : Record "Chemical Group";
    begin
        if UnCoderec."UN Code" <> '' then begin
          UnREC.SETRANGE("UN Code",UnCoderec."UN Code");
          UnREC.FIND('-');
        end else begin
          UnREC.SETRANGE("Item No.",UnCoderec."Item No.");
          UnREC.FIND('-');
        end;

        UnCoderec := UnREC;
    end;
}

