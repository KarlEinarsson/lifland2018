tableextension 112 "Sales Invoice Header Extension" extends "Sales Invoice Header"
{
    fields
    {
        field(50000;"Staðf. móttaka greiðslu";boolean)
        {
            CaptionML = ENU = 'Conf. Payment Receipt',
                        ISL = 'Staðf. móttaka greiðslu';
        }

        field(50001;"Freight Paid By";Option)
        {
            CaptionML = ENU = 'Freight Paid By',
                        ISL = 'Flutningsgjöld greiðast af';
            OptionMembers = Recipient,Prepaid,"Senders Account";
            OptionCaptionML = 	ENU = 'Recipient,Prepaid,Senders Account',
                                ISL = 'Viðtakanda,Staðgreitt,Í reikning sendanda';             
        }
    }
}