table 59247 "SSCC Ref."
{
    // version SAME

    // 
    // Structure of a SSCC
    // -----------------------------------------------------------------------------------
    // Extension
    // digit     GS1 Company Prefix -------> <----------Serial Reference      Check Digit
    // N1        N2 N3 N4 N5 N6 N7 N8 N9 N10 N11 N12 N13 N14 N15 N16 N17      N18
    // 
    // Extension digit:
    // The Extension digit has no defined logic, and is available to the member
    // company to increase the capacity of the Serial Reference.
    // 
    // GS1 Company Prefix:
    // The globally unique number assigned to a company by GS1 US(TM) or another
    // GS1 Member Organization. GS1 Company Prefixes are assigned to companies
    // in varying lengths. Some GS1 US BarCodes and eCom(TM) members may have a
    // membership certificate that shows only a U.P.C. Company Prefix (formerly a
    // UCC Company Prefix). A U.P.C. Company Prefix must be converted to a GS1
    // Company Prefix for use in the SSCC. This is accomplished by adding a zero to
    // the front of the U.P.C. Company Prefix. The following table illustrates the
    // conversion of a U.P.C. Company Prefix to a GS1 Company Prefix for use with
    // the SSCC.
    // 
    // U.P.C. Company Prefix                 Converted to GS1 Company Prefix
    //       801234                                     0801234
    //       8412340                                   08412340
    //      81123400                                   081123400
    // 
    // Serial Reference:
    // The number assigned by the holder of the GS1 Company
    // Prefix to uniquely identify the logistic unit. The Serial Reference varies in length
    // as a function of the GS1 Company Prefix length. Note: The combined length of
    // the GS1 Company Prefix and Serial Reference is always 16 digits.
    // 
    // Check Digit:
    // A calculated one-digit number used to ensure data integrity. To
    // understand how this digit is calculated, refer to www.gs1us.org/checkdig.
    // 
    // EAN128 sjá GS1.is

    CaptionML = ENU='SSCC Ref.',
                ISL='SSCC Strikamerki';

    fields
    {
        field(1;"SSCC Reference";Code[20])
        {
            CaptionML = ENU='SSCC Reference',
                        ISL='SSCC strikamerki';
            Description = 'Serial Shipping Container Code';
            NotBlank = false;
        }
        field(2;User;Text[20])
        {
            CaptionML = ENU='User',
                        ISL='Notandi';
        }
        field(3;Printed;Integer)
        {
            CaptionML = ENU='Printed',
                        ISL='Prentað';
        }
        field(4;"Document No.";Code[20])
        {
            CaptionML = ENU='Document No.',
                        ISL='Sendingarnúmer';
        }
    }

    keys
    {
        key(Key1;"SSCC Reference")
        {
        }
        key(Key2;Printed)
        {
        }
        key(Key3;"Document No.")
        {
        }
    }

    fieldgroups
    {
    }

    trigger OnInsert();
    begin
        Setup.GET;

        if "SSCC Reference" = '' then begin
          NoSeriesMgt.InitSeries(Setup."SSCC No. Series",Setup."SSCC No. Series",TODAY,"SSCC Reference",Setup."SSCC No. Series");
        end;

        "SSCC Reference" := Setup."SSCC Company Code" + "SSCC Reference";
        "SSCC Reference" := "SSCC Reference" + FORMAT(CheckDigitCalculation(COPYSTR("SSCC Reference",3)));

        User := USERID;
        Printed := 0;
        "Document No." := DocInput;
    end;

    var
        Setup : Record "Transport Setup";
        NoSeriesMgt : Codeunit NoSeriesManagement;
        DocInput : Code[20];

    procedure CreateBarCode(NoOfTicks : Integer;PrintIt : Boolean);
    var
        BarcodeREC : Record "SSCC Ref.";
    begin
        repeat
          BarcodeREC.INIT;
          BarcodeREC."SSCC Reference" := '';
          NoOfTicks := NoOfTicks - 1;
          BarcodeREC.SetDocNo(DocInput);
          BarcodeREC.INSERT(true);
          //if PrintIt then
            //REPORT.RUN

        until NoOfTicks <= 0;
    end;

    procedure CheckDigitCalculation(ssccNo : Code[17]) : Integer;
    var
        I : Integer;
        digitsplit : array [17] of Integer;
        sumDig1 : Decimal;
        sumDig2 : Decimal;
        sumDig3 : Decimal;
        strlenmsg : Label 'SSCC númerasería verður að vera 17 stafir!';
        sumDig4 : Decimal;
        sumDig5 : Decimal;
        sumDig6 : Decimal;
    begin

        if STRLEN(ssccNo) < 17 then
          ERROR(strlenmsg);

        //step 1 - 2
        I := 0;
        repeat
          I += 1;
          EVALUATE(digitsplit[I],COPYSTR(ssccNo,I,1));
          sumDig1 += digitsplit[I];
          I += 1;
        until I >= 17;

        sumDig2 := (sumDig1 * 3);


        //step 1 - 2
        I := 1;
        repeat
          I += 1;
          EVALUATE(digitsplit[I],COPYSTR(ssccNo,I,1));
          sumDig3 += digitsplit[I];
          I += 1;
        until I >= 17;

        sumDig4 := sumDig2 + sumDig3;

        sumDig5 := sumDig4 / 10;

        sumDig6 := ROUND(sumDig5,1.0,'>');

        sumDig1 := (sumDig6 - sumDig5) * 10;


        exit(sumDig1);
    end;

    procedure SetDocNo(DocumentInput : Code[20]);
    begin
        DocInput := DocumentInput;
    end;
}

