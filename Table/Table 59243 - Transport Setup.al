table 59243 "Transport Setup"
{
    // version SAME

    CaptionML = ENU='Transport Setup',
                ISL='Uppsetning flutningskerfis';

    fields
    {
        field(1;"Key";Code[10])
        {
            CaptionML = ENU='Key',
                        ISL='Lykill';
        }
        field(2;"Default Transport Method";Code[10])
        {
            CaptionML = ENU='Default Transport Method',
                        ISL='Sjálfgefinn Flutningsmáti';
            TableRelation = "Transport Method";
        }
        field(3;"Default Shipping Agent Code";Code[10])
        {
            CaptionML = ENU='Default Shipping Agent Code',
                        ISL='Sjálfgefinn Flutningsaðilakóti';
            TableRelation = "Shipping Agent";

            trigger OnValidate();
            var
                SHagent : Record "Shipping Agent";
            begin
            end;
        }
        field(4;"User ID";Text[15])
        {
            CaptionML = ENU='User ID',
                        ISL='Notandanafn';
        }
        field(5;Password;Text[30])
        {
            CaptionML = ENU='Password',
                        ISL='Lykilorð';
        }
        field(6;"Shippin Supplier URL";Text[250])
        {
            CaptionML = ENU='Shippin Supplier URL',
                        ISL='Vefslóð sendingarkerfis';
        }
        field(7;"File Folder Outgoing";Text[250])
        {
            CaptionML = ENU='File Folder Outgoing',
                        ISL='Skráasafn á útleið';
        }
        field(8;"File Folder Recieved";Text[250])
        {
            CaptionML = ENU='File Folder Recieved',
                        ISL='Skráasafn á innleið';
        }
        field(9;"File Folder Starage";Text[250])
        {
            CaptionML = ENU='File Folder Starage',
                        ISL='Skráasafn geymsla';
        }
        field(10;"Shipping No. Series";Code[10])
        {
            CaptionML = ENU='Shipping No. Series',
                        ISL='Númeraröð sendinga';
            TableRelation = "No. Series";
        }
        field(11;"Message No. Series";Code[10])
        {
            CaptionML = ENU='Message No. Series',
                        ISL='Númeraröð skilaboða';
            TableRelation = "No. Series";
        }
        field(12;"SSCC No. Series";Code[10])
        {
            CaptionML = ENU='SSCC No. Series',
                        ISL='SSCC teljari';
            TableRelation = "No. Series";
        }
        field(13;"SSCC Company Code";Code[10])
        {
            CaptionML = ENU='SSCC Company Code',
                        ISL='SSCC fyrirtækjasería';
        }
        field(50001;"File Folder Outgoing Fl";Text[250])
        {
            CaptionML = ENU='File Folder Outgoing',
                        ISL='Skráasafn á útleið';
        }
        field(50002;"File Folder Recieved Fl";Text[250])
        {
            CaptionML = ENU='File Folder Recieved',
                        ISL='Skráasafn á innleið';
        }
        field(50003;"File Folder Starage FL";Text[250])
        {
            CaptionML = ENU='File Folder Starage',
                        ISL='Skráasafn geymsla';
        }
        field(50004;"Default Shipping Agent Code 2";Code[10])
        {
            CaptionML = ENU='Default Shipping Agent Code 2',
                        ISL='Sjálfgefinn Flutningsaðilakóti 2';
            TableRelation = "Shipping Agent";

            trigger OnValidate();
            var
                SHagent : Record "Shipping Agent";
            begin
            end;
        }
    }

    keys
    {
        key(Key1;"Key")
        {
        }
    }

    fieldgroups
    {
    }
}

