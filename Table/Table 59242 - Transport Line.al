table 59242 "Transport Line"

{
    // version SAME
    // Samskip Fylgibréf
    // EY 06.12.2011 -

    CaptionML = ENU='Transport Line',
                ISL='Flutningslína';
    PasteIsValid = false;

    fields
    {
        field(1;"Document Type";Option)
        {
            CaptionML = ENU='Document Type',
                        ISL='Tegund fylgiskjals';
            OptionCaptionML = ENU='Quote,Order,Invoice,Shipment,Posted Invoice',
                              ISL='Tilboð,Pöntun,Reikningur,Afhending,Bókaður reikningur';
            OptionMembers = Quote,"Order",Invoice,Shipment,"Posted Invoice";
        }
        field(3;"Document No.";Code[20])
        {
            CaptionML = ENU='Document No.',
                        ISL='Sendingarnúmer';
        }
        field(4;"Line No.";Integer)
        {
            CaptionML = ENU='Line No.',
                        ISL='Línunr.';
        }
        field(6;"No.";Code[20])
        {
            CaptionML = ENU='No.',
                        ISL='Nr.';
            TableRelation = Item;

            trigger OnValidate();
            begin
                TestStatusOpen;

                TESTFIELD("Shipment No.",'');

                TempSalesLine := Rec;
                INIT;
                "No." := TempSalesLine."No.";
                if "No." = '' then
                  exit;

                "System-Created Entry" := TempSalesLine."System-Created Entry";
                GetSalesHeader;

                "Transport Method" := SalesHeader."Transport Method";
                "Bill-to Customer No." := SalesHeader."Bill-to Customer No.";
                "Exit Point" := SalesHeader."Exit Point";

                "Shipping Agent Code" := SalesHeader."Shipping Agent Code";
                "Shipping Agent Service Code" := SalesHeader."Shipping Agent Service Code";
                "Shipping Time" :=SalesHeader."Shipping Time";

                "Promised Delivery Date" := SalesHeader."Promised Delivery Date";
                "Requested Delivery Date" := SalesHeader."Requested Delivery Date";
                "Shipment Date" :=
                  CalendarMgmt.CalcDateBOC(
                    '',
                    SalesHeader."Shipment Date",
                    CalChange."Source Type"::Location,'',
                    '',
                    CalChange."Source Type"::"Shipping Agent",
                    "Shipping Agent Code",
                    "Shipping Agent Service Code",
                    false);
                UpdateDates;

                      GetItem;
                      Item.TESTFIELD(Blocked,false);
                      Item.TESTFIELD("Inventory Posting Group");
                      Item.TESTFIELD("Gen. Prod. Posting Group");

                      Description := Item.Description;
                      "Description 2" := Item."Description 2";
                      GetUnitCost;
                      "Units per Parcel" := Item."Units per Parcel";
                       "Item Category Code" := Item."Item Category Code";
                      "Product Group Code" := Item."Product Group Code";

                      "Unit of Measure Code" := Item."Sales Unit of Measure";

                      TransportItmeType.SETRANGE("Item Category Code","Item Category Code");
                      TransportItmeType.SETRANGE("Product Group Code","Product Group Code");
                      if TransportItmeType.FIND('+') then begin
                        "Chemical Group" := TransportItmeType."Default Chemical Group";
                        "Item Type" := TransportItmeType."Item Type";
                      end;

                  VALIDATE("Unit of Measure Code");
                  UpdateUnitPrice(FIELDNO("No."));
            end;
        }
        field(10;"Shipment Date";Date)
        {
            CaptionML = ENU='Shipment Date',
                        ISL='Afh.dags';

            trigger OnValidate();
            var
                CheckDateConflict : Codeunit "Reservation-Check Date Confl.";
            begin
            end;
        }
        field(11;Description;Text[50])
        {
            CaptionML = ENU='Description',
                        ISL='Lýsing';
        }
        field(12;"Description 2";Text[50])
        {
            CaptionML = ENU='Description 2',
                        ISL='Lýsing 2';
        }
        field(13;"Unit of Measure";Text[10])
        {
            CaptionML = ENU='Unit of Measure',
                        ISL='Mælieining';
        }
        field(15;Quantity;Decimal)
        {
            CaptionML = ENU='Quantity',
                        ISL='Magn';
            DecimalPlaces = 0:5;

            trigger OnValidate();
            begin
                TestStatusOpen;

                "Quantity (Base)" := CalcBaseQty(Quantity);

                VALIDATE("Line Discount %");
                if CurrFieldNo = FIELDNO(Quantity) then
                  VALIDATE("Unit of Measure Code");
            end;
        }
        field(16;"Outstanding Quantity";Decimal)
        {
            CaptionML = ENU='Outstanding Quantity',
                        ISL='Útistandandi magn';
            DecimalPlaces = 0:5;
            Editable = false;
        }
        field(18;"Qty. to Ship";Decimal)
        {
            CaptionML = ENU='Qty. to Ship',
                        ISL='Magn til afhendingar';
            DecimalPlaces = 0:5;

            trigger OnValidate();
            begin

                if ("Qty. to Ship" * Quantity < 0) or
                   (ABS("Qty. to Ship") > ABS("Outstanding Quantity")) or
                   (Quantity * "Outstanding Quantity" < 0)
                then
                  ERROR(
                    Text007,
                    "Outstanding Quantity");
            end;
        }
        field(22;"Unit Price";Decimal)
        {
            AutoFormatType = 2;
            CaptionML = ENU='Unit Price',
                        ISL='Ein.verð';

            trigger OnValidate();
            begin
                TestStatusOpen;
                VALIDATE("Line Discount %");
            end;
        }
        field(23;"Unit Cost (LCY)";Decimal)
        {
            AutoFormatType = 2;
            CaptionML = ENU='Unit Cost (LCY)',
                        ISL='Kostn.verð (ISK)';

            trigger OnValidate();
            begin
                GetSalesHeader;
                "Unit Cost" := "Unit Cost (LCY)";
            end;
        }
        field(25;"VAT %";Decimal)
        {
            CaptionML = ENU='VAT %',
                        ISL='VSK%';
            DecimalPlaces = 0:5;
            Editable = false;
        }
        field(27;"Line Discount %";Decimal)
        {
            CaptionML = ENU='Line Discount %',
                        ISL='Línuafsl.%';
            DecimalPlaces = 0:5;
            MaxValue = 100;
            MinValue = 0;

            trigger OnValidate();
            begin
                TestStatusOpen;
                "Line Discount Amount" :=
                  ROUND(
                    ROUND(Quantity * "Unit Price",Currency."Amount Rounding Precision") *
                    "Line Discount %" / 100,Currency."Amount Rounding Precision");
                "Inv. Discount Amount" := 0;
                UpdateAmounts;
            end;
        }
        field(28;"Line Discount Amount";Decimal)
        {
            AutoFormatType = 1;
            CaptionML = ENU='Line Discount Amount',
                        ISL='Afsl.upphæð línu';

            trigger OnValidate();
            begin
                TestStatusOpen;
                TESTFIELD(Quantity);
                if ROUND(Quantity * "Unit Price",Currency."Amount Rounding Precision") <> 0 then
                  "Line Discount %" :=
                    ROUND(
                     "Line Discount Amount" / ROUND(Quantity * "Unit Price",Currency."Amount Rounding Precision") * 100,
                      0.00001)
                else
                  "Line Discount %" := 0;
                "Inv. Discount Amount" := 0;
                UpdateAmounts;
            end;
        }
        field(29;Amount;Decimal)
        {
            AutoFormatType = 1;
            CaptionML = ENU='Amount',
                        ISL='Upphæð';
            Editable = false;

            trigger OnValidate();
            begin
                Amount := ROUND(Amount,Currency."Amount Rounding Precision");
            end;
        }
        field(30;"Amount Including VAT";Decimal)
        {
            AutoFormatType = 1;
            CaptionML = ENU='Amount Including VAT',
                        ISL='Upphæð með VSK';
            Editable = false;
        }
        field(34;"Gross Weight";Decimal)
        {
            CaptionML = ENU='Gross Weight',
                        ISL='Brúttóþyngd';
            DecimalPlaces = 0:5;
        }
        field(35;"Net Weight";Decimal)
        {
            CaptionML = ENU='Net Weight',
                        ISL='Nettóþyngd';
            DecimalPlaces = 0:5;
        }
        field(36;"Units per Parcel";Decimal)
        {
            CaptionML = ENU='Units per Parcel',
                        ISL='Fjöldi í pakkningu';
            DecimalPlaces = 0:5;
            Description = 'Fj í pakkn';
        }
        field(37;"Unit Volume";Decimal)
        {
            CaptionML = ENU='Unit Volume',
                        ISL='Rúmmál';
            DecimalPlaces = 0:5;
            Description = 'rúmmál';
        }
        field(57;"Outstanding Amount";Decimal)
        {
            AutoFormatType = 1;
            CaptionML = ENU='Outstanding Amount',
                        ISL='Upphæð eftirstöðva';
            Editable = false;

            trigger OnValidate();
            var
                Currency2 : Record Currency;
            begin
            end;
        }
        field(63;"Shipment No.";Code[20])
        {
            CaptionML = ENU='Shipment No.',
                        ISL='Afhendingarnr.';
            Editable = false;
        }
        field(64;"Shipment Line No.";Integer)
        {
            CaptionML = ENU='Shipment Line No.',
                        ISL='Línunr. afhendingar';
            Editable = false;
        }
        field(68;"Bill-to Customer No.";Code[20])
        {
            CaptionML = ENU='Bill-to Customer No.',
                        ISL='Reikn.færist á viðskm.';
            Editable = false;
            TableRelation = Customer;
        }
        field(69;"Inv. Discount Amount";Decimal)
        {
            AutoFormatType = 1;
            CaptionML = ENU='Inv. Discount Amount',
                        ISL='Reikningsafsl.upphæð';
            Editable = false;

            trigger OnValidate();
            begin
                TESTFIELD(Quantity);
                CalcInvDiscToInvoice;
                UpdateAmounts;
            end;
        }
        field(73;"Drop Shipment";Boolean)
        {
            CaptionML = ENU='Drop Shipment',
                        ISL='Bein afhending';
            Editable = true;
        }
        field(78;"Transaction Type";Code[10])
        {
            CaptionML = ENU='Transaction Type',
                        ISL='Tegund viðskipta';
            TableRelation = "Transaction Type";
        }
        field(79;"Transport Method";Code[10])
        {
            CaptionML = ENU='Transport Method',
                        ISL='Flutningsmáti';
            TableRelation = "Transport Method";
        }
        field(80;"Attached to Sales Line No.";Integer)
        {
            CaptionML = ENU='Attached to Line No.',
                        ISL='Tengt við línu nr.';
            Editable = false;
            TableRelation = "Sales Line"."Line No." WHERE ("Document Type"=FIELD("Document Type"),
                                                           "Document No."=FIELD("Document No."));
        }
        field(81;"Exit Point";Code[10])
        {
            CaptionML = ENU='Exit Point',
                        ISL='Brottfararstaður';
            TableRelation = "Entry/Exit Point";
        }
        field(82;"Area";Code[10])
        {
            CaptionML = ENU='Area',
                        ISL='Svæði';
            TableRelation = Area;
        }
        field(97;"Blanket Order No.";Code[20])
        {
            CaptionML = ENU='Blanket Order No.',
                        ISL='Standandi pöntun nr.';
            TableRelation = "Sales Header"."No." WHERE ("Document Type"=CONST("Blanket Order"));
            //This property is currently not supported
            //TestTableRelation = false;

            trigger OnValidate();
            begin
                if "Blanket Order No." = '' then
                  "Blanket Order Line No." := 0
                else
                  VALIDATE("Blanket Order Line No.");
            end;
        }
        field(98;"Blanket Order Line No.";Integer)
        {
            CaptionML = ENU='Blanket Order Line No.',
                        ISL='Standandi pöntunarlína nr.';
            TableRelation = "Sales Line"."Line No." WHERE ("Document Type"=CONST("Blanket Order"),
                                                           "Document No."=FIELD("Blanket Order No."));
            //This property is currently not supported
            //TestTableRelation = false;
        }
        field(99;"VAT Base Amount";Decimal)
        {
            AutoFormatType = 1;
            CaptionML = ENU='VAT Base Amount',
                        ISL='Upphæð VSK-stofns';
            Editable = false;
        }
        field(100;"Unit Cost";Decimal)
        {
            AutoFormatType = 2;
            CaptionML = ENU='Unit Cost',
                        ISL='Kostn.verð';
            Editable = false;
        }
        field(101;"System-Created Entry";Boolean)
        {
            CaptionML = ENU='System-Created Entry',
                        ISL='Kerfisfærsla';
            Editable = false;
        }
        field(103;"Line Amount";Decimal)
        {
            AutoFormatType = 1;
            CaptionClass = GetCaptionClass(FIELDNO("Line Amount"));
            CaptionML = ENU='Line Amount',
                        ISL='Línuupphæð';

            trigger OnValidate();
            begin
                TESTFIELD(Quantity);
                TESTFIELD("Unit Price");
                GetSalesHeader;
                "Line Amount" := ROUND("Line Amount",Currency."Amount Rounding Precision");
                VALIDATE(
                  "Line Discount Amount",ROUND(Quantity * "Unit Price",Currency."Amount Rounding Precision") - "Line Amount");
            end;
        }
        field(5404;"Qty. per Unit of Measure";Decimal)
        {
            CaptionML = ENU='Qty. per Unit of Measure',
                        ISL='Magn á mælieiningu';
            DecimalPlaces = 0:5;
            Editable = false;
            InitValue = 1;
        }
        field(5407;"Unit of Measure Code";Code[10])
        {
            CaptionML = ENU='Unit of Measure Code',
                        ISL='Mælieiningarkóti';
            TableRelation = "Item Unit of Measure".Code WHERE ("Item No."=FIELD("No."));

            trigger OnValidate();
            var
                UnitOfMeasureTranslation : Record "Unit of Measure Translation";
                ResUnitofMeasure : Record "Resource Unit of Measure";
            begin
                TestStatusOpen;

                if "Unit of Measure Code" = '' then
                  "Unit of Measure" := ''
                else begin
                  if not UnitOfMeasure.GET("Unit of Measure Code") then
                    UnitOfMeasure.INIT;
                  "Unit of Measure" := UnitOfMeasure.Description;
                  GetSalesHeader;
                end;

                GetItem;
                GetUnitCost;
                UpdateUnitPrice(FIELDNO("Unit of Measure Code"));


                IOMrec.GET(Item."No.","Unit of Measure Code");

                if Quantity <> 0 then begin
                if "Unit of Measure Code" <> Item."Base Unit of Measure" then begin
                  "Gross Weight" := IOMrec.Weight * Quantity;
                  "Net Weight" := Item."Net Weight" * ("Qty. per Unit of Measure" * Quantity);
                  "Unit Volume" := IOMrec.Cubage * Quantity;
                  "Units per Parcel" := ROUND(Item."Units per Parcel" / ("Qty. per Unit of Measure" * Quantity),0.00001);

                end else begin
                  if IOMrec.Weight <> 0 then
                    Item."Gross Weight" := IOMrec.Weight;
                  if IOMrec.Cubage <> 0 then
                    Item."Unit Volume" := IOMrec.Cubage;

                  "Gross Weight" := Item."Gross Weight" * ("Qty. per Unit of Measure" * Quantity);
                  "Net Weight" := Item."Net Weight" * ("Qty. per Unit of Measure" * Quantity);
                  "Unit Volume" := Item."Unit Volume" * ("Qty. per Unit of Measure" * Quantity);
                  "Units per Parcel" := ROUND(Item."Units per Parcel" / ("Qty. per Unit of Measure" * Quantity),0.00001);
                end;
                end;
                if CurrFieldNo = FIELDNO("Unit of Measure Code") then
                  VALIDATE(Quantity);
            end;
        }
        field(5415;"Quantity (Base)";Decimal)
        {
            CaptionML = ENU='Quantity (Base)',
                        ISL='Magn (stofn)';
            DecimalPlaces = 0:5;

            trigger OnValidate();
            begin
                TESTFIELD("Qty. per Unit of Measure",1);
                VALIDATE(Quantity,"Quantity (Base)");
            end;
        }
        field(5709;"Item Category Code";Code[10])
        {
            CaptionML = ENU='Item Category Code',
                        ISL='Kóti yfirflokks vöru';
            TableRelation = "Item Category";
        }
        field(5712;"Product Group Code";Code[10])
        {
            CaptionML = ENU='Product Group Code',
                        ISL='Kóti framleiðsluflokks';
            TableRelation = "Product Group".Code WHERE ("Item Category Code"=FIELD("Item Category Code"));
        }
        field(5752;"Completely Shipped";Boolean)
        {
            CaptionML = ENU='Completely Shipped',
                        ISL='Afhent að fullu';
            Editable = false;
        }
        field(5790;"Requested Delivery Date";Date)
        {
            CaptionML = ENU='Requested Delivery Date',
                        ISL='Umbeðin afgreiðsludagsetning';

            trigger OnValidate();
            begin
                TestStatusOpen;
                if "Promised Delivery Date" <> 0D then
                  ERROR(
                    Text028,
                    FIELDCAPTION("Requested Delivery Date"),
                    FIELDCAPTION("Promised Delivery Date"));

                if "Requested Delivery Date" <> 0D then
                  VALIDATE("Planned Delivery Date","Requested Delivery Date")
                else begin
                  GetSalesHeader;
                  VALIDATE("Shipment Date",SalesHeader."Shipment Date");
                end;
            end;
        }
        field(5791;"Promised Delivery Date";Date)
        {
            CaptionML = ENU='Promised Delivery Date',
                        ISL='Lofuð afgreiðsludagsetning';

            trigger OnValidate();
            begin
                TestStatusOpen;
                if "Promised Delivery Date" <> 0D then
                  VALIDATE("Planned Delivery Date","Promised Delivery Date")
                else
                  VALIDATE("Requested Delivery Date");
            end;
        }
        field(5792;"Shipping Time";DateFormula)
        {
            CaptionML = ENU='Shipping Time',
                        ISL='Afhendingartími';

            trigger OnValidate();
            begin
                TestStatusOpen;
                UpdateDates;
            end;
        }
        field(5794;"Planned Delivery Date";Date)
        {
            CaptionML = ENU='Planned Delivery Date',
                        ISL='Áætluð afgreiðsludagsetning';

            trigger OnValidate();
            begin
                TestStatusOpen;
                if ("Planned Delivery Date" <> 0D) then begin
                  PlannedDeliveryDateCalculated := true;

                  if FORMAT("Shipping Time") <> '' then
                    VALIDATE(
                      "Planned Shipment Date",
                      CalendarMgmt.CalcDateBOC(
                        FORMAT('-'+FORMAT("Shipping Time")),
                        "Planned Delivery Date",
                        CalChange."Source Type"::"Shipping Agent",
                        "Shipping Agent Code",
                        "Shipping Agent Service Code",
                        CalChange."Source Type"::Location,
                        '',
                        '',
                        true))
                  else
                    VALIDATE(
                      "Planned Shipment Date",
                      CalendarMgmt.CalcDateBOC(
                        FORMAT(''),
                        "Planned Delivery Date",
                        CalChange."Source Type"::"Shipping Agent",
                        "Shipping Agent Code",
                        "Shipping Agent Service Code",
                        CalChange."Source Type"::Location,
                        '',
                        '',
                        true));
                end;
            end;
        }
        field(5795;"Planned Shipment Date";Date)
        {
            CaptionML = ENU='Planned Shipment Date',
                        ISL='Áætluð afhendingardagsetning';

            trigger OnValidate();
            begin
                TestStatusOpen;
                if ("Planned Shipment Date" <> 0D) then begin
                  PlannedShipmentDateCalculated := true;

                    VALIDATE(
                      "Shipment Date",
                      CalendarMgmt.CalcDateBOC(
                        FORMAT(FORMAT('')),
                        "Planned Shipment Date",
                        CalChange."Source Type"::Location,
                        '',
                        '',
                        CalChange."Source Type"::"Shipping Agent",
                        "Shipping Agent Code",
                        "Shipping Agent Service Code",
                        false));
                end;
            end;
        }
        field(5796;"Shipping Agent Code";Code[10])
        {
            CaptionML = ENU='Shipping Agent Code',
                        ISL='Flutningsaðilakóti';
            TableRelation = "Shipping Agent";

            trigger OnValidate();
            begin
                TestStatusOpen;
                if "Shipping Agent Code" <> xRec."Shipping Agent Code" then
                  VALIDATE("Shipping Agent Service Code",'');
            end;
        }
        field(5797;"Shipping Agent Service Code";Code[10])
        {
            CaptionML = ENU='Shipping Agent Service Code',
                        ISL='Flutningaþjónustukóti';
            TableRelation = "Shipping Agent Services".Code WHERE ("Shipping Agent Code"=FIELD("Shipping Agent Code"));

            trigger OnValidate();
            begin
                TestStatusOpen;
                if "Shipping Agent Service Code" <> xRec."Shipping Agent Service Code" then
                  EVALUATE("Shipping Time",'<>');

                if ShippingAgentServices.GET("Shipping Agent Code","Shipping Agent Service Code") then begin
                  "Shipping Time" := ShippingAgentServices."Shipping Time"
                end else begin
                  GetSalesHeader;
                  "Shipping Time" := SalesHeader."Shipping Time";
                end;

                if (ShippingAgentServices."Shipping Time" <> xRec."Shipping Time") then
                  VALIDATE("Shipping Time","Shipping Time");
            end;
        }
        field(59241;"Wrap Weight";Decimal)
        {
            CaptionML = ENU='Wrap Weight',
                        ISL='Umbúðaþyngd';
        }
        field(59242;Width;Decimal)
        {
            CaptionML = ENU='Width',
                        ISL='Breidd';

            trigger OnValidate();
            begin
                "Unit Volume" := ROUND(Width * Hight * Depth);
            end;
        }
        field(59243;Hight;Decimal)
        {
            CaptionML = ENU='Hight',
                        ISL='Hæð';

            trigger OnValidate();
            begin
                "Unit Volume" := ROUND(Width * Hight * Depth);
            end;
        }
        field(59244;Depth;Decimal)
        {
            CaptionML = ENU='Depth',
                        ISL='Dýpt';

            trigger OnValidate();
            begin
                "Unit Volume" := ROUND(Width * Hight * Depth);
            end;
        }
        field(59245;"SSCC Reference";Code[20])
        {
            CaptionML = ENU='SSCC Reference',
                        ISL='SSCC strikamerki';
            Description = 'Serial Shipping Container Code';
            NotBlank = false;
            TableRelation = "SSCC Ref.";

            trigger OnLookup();
            var
                //ssccBarcodeReport : Report "SSCC barcode";
                ssccMsg3 : Label 'Viltu prenta þetta SSCC strikamerki?';
            begin
                SSCCrefREC.SETCURRENTKEY("Document No.");
                SSCCrefREC.SETFILTER("Document No.",'=%1|%2','',"Document No.");
                if not SSCCrefREC.FIND('-') then
                  SSCCrefREC.CreateBarCode(1,false);
                COMMIT;
                SSCCrefREC.FILTERGROUP(2);
                if PAGE.RUNMODAL(/*page::Page59249*/0,SSCCrefREC) = ACTION::LookupOK then begin
                  "SSCC Reference" := SSCCrefREC."SSCC Reference";
                  SSCCrefREC.SETRANGE("SSCC Reference",SSCCrefREC."SSCC Reference");
                  /*
                  ssccBarcodeReport.USEREQUESTFORM(FALSE);
                  ssccBarcodeReport.SETTABLEVIEW(SSCCrefREC);
                  IF CONFIRM(ssccMsg3,TRUE) THEN
                    ssccBarcodeReport.RUN;
                  */
                  SSCCrefREC.FIND('+');
                  VALIDATE("SSCC Reference");
                  MODIFY;
                end;
                SSCCrefREC.RESET;

            end;

            trigger OnValidate();
            var
                ssccMsg : TextConst ENU='This Bar-Code has not been printed!',ISL='Það á eftir að prenta út þetta strikamerki!';
                ssccMsg2 : TextConst ENU='This Bar-Code belongs to another Transport!',ISL='Þetta strikamerki tilheyrir annarri sendingu!';
                ssccMsg3 : Label 'Viltu prenta þetta SSCC strikamerki?';
            begin
                if "SSCC Reference" <> xRec."SSCC Reference" then begin
                  if SSCCrefREC.GET(xRec."SSCC Reference") then begin
                    SSCCrefREC."Document No." := '';
                    SSCCrefREC.MODIFY;
                  end;
                end;

                if "SSCC Reference" <> '' then begin
                  SSCCrefREC.GET("SSCC Reference");
                  if CurrFieldNo = FIELDNO("SSCC Reference") then
                    //IF SSCCrefREC.Printed = 0 THEN
                    //  ERROR(ssccMsg);
                  if (SSCCrefREC."Document No." <> '') and (SSCCrefREC."Document No." <> "SSCC Reference") then
                    ERROR(ssccMsg2);
                  SSCCrefREC."Document No." := "Document No.";
                  SSCCrefREC.MODIFY;
                end;
            end;
        }
        field(59246;Units;Decimal)
        {
            Caption = 'Einingar';
            Description = 'Einingar í sendingu';
        }
        field(59247;"Chemical Group";Code[10])
        {
            CaptionML = ENU='Default Chemical Group',
                        ISL='Efnavöruflokkur';
            TableRelation = "Chemical Group";
        }
        field(59248;"Item Type";Option)
        {
            CaptionML = ENU='Item Type',
                        ISL='Eðli vöru';
            OptionCaptionML = ENU='Normal,Chemical,Freeze,Cooler',
                              ISL='Þurrvara,Efnavara,Frystivara,Kælivara';
            OptionMembers = Normal,Chemical,Freeze,Cooler;
        }
        field(59249;Fargile;Boolean)
        {
            CaptionML = ENU='Item Treatment',
                        ISL='Brothætt';
        }
        field(59260;"Dangerous Item";Boolean)
        {
            Caption = 'Hættuleg vara';
        }
        field(59261;Temurature;Decimal)
        {
            Caption = 'Hitastig';
        }
    }

    keys
    {
        key(Key1;"Document No.","Line No.")
        {
            MaintainSIFTIndex = false;
            SumIndexFields = Quantity,"Gross Weight","Net Weight","Unit Volume","Units per Parcel";
        }
        key(Key2;"Document No.","Item Type","Unit of Measure Code","SSCC Reference")
        {
            SumIndexFields = Quantity,"Gross Weight","Net Weight","Unit Volume","Units per Parcel";
        }
    }

    fieldgroups
    {
    }

    trigger OnDelete();
    var
        DocDim : Integer;
        CapableToPromise : Codeunit "Capable to Promise";
    begin
        //TestStatusOpen;
    end;

    trigger OnInsert();
    var
        DocDim : Integer;
    begin
        /*
        TestStatusOpen;
        DocDim.LOCKTABLE;
        LOCKTABLE;
        SalesHeader."No." := '';
        */

    end;

    var
        Text000 : TextConst ENU='You cannot delete the order line because it is associated with purchase order %1.',ISL='Ekki er hægt að eyða pöntunarlínunni því hún tengist innkaupapöntun %1.';
        Text001 : TextConst ENU='You cannot rename a %1.',ISL='%1: Ekki er hægt að endurnefna.';
        Text002 : TextConst ENU='You cannot change %1 because the order line is associated with purchase order %2.',ISL='Ekki er hægt að breyta %1 því pöntunarlínan tengist innkaupapöntun %2.';
        Text003 : TextConst ENU='must not be less than %1',ISL='verður að vera minna en %1';
        Text005 : TextConst ENU='You cannot invoice more than %1 units.',ISL='Ekki er hægt að reikningsfæra meira en %1 einingar.';
        Text006 : TextConst ENU='You cannot invoice more than %1 base units.',ISL='Ekki er hægt að reikningsfæra meira en %1 grunneiningar.';
        Text007 : TextConst ENU='You cannot ship more than %1 units.',ISL='Ekki er hægt að afhenda meira en %1 einingar.';
        Text008 : TextConst ENU='You cannot ship more than %1 base units.',ISL='Ekki er hægt að afhenda meira en %1 grunneiningar.';
        Text009 : TextConst ENU=' must be 0 when %1 is %2.',ISL=' verður að vera 0 þegar %1 er %2.';
        Text010 : TextConst ENU='Prices including VAT cannot be calculated when %1 is %2.',ISL='Ekki er hægt að reikna verð með VSK þegar %1 er %2.';
        Text011 : TextConst ENU='Automatic reservation is not possible.\Reserve items manually?',ISL='Sjálfvirk frátekning er ekki möguleg.\Á að taka atriði frá handvirkt?';
        Text012 : TextConst ENU='Change %1 from %2 to %3?',ISL='Breyta %1 úr %2 í %3?';
        Text014 : TextConst ENU='%1 %2 is before Work Date %3',ISL='%1 %2 er á undan vinnudagsetningunni %3';
        Text015 : TextConst ENU='must not be less than zero',ISL='má ekki vera minna en núll';
        Text016 : TextConst ENU='Warehouse %1 is required for %2 = %3.',ISL='Vöruhús %1 er nauðsynlegt fyrir %2 = %3.';
        Text017 : TextConst ENU='\The entered information will be disregarded by warehouse operations.',ISL='\Vöruhúsaaðgerðir hunsa upplýsingarnar sem færðar eru inn.';
        Text018 : TextConst ENU='must not be specified when %1 = %2',ISL='má ekki tilgreina þegar %1 = %2';
        Text020 : TextConst ENU='You cannot return more than %1 units.',ISL='Ekki er hægt að skila fleiri en %1 einingum.';
        Text021 : TextConst ENU='You cannot return more than %1 base units.',ISL='Ekki er hægt að skila fleiri en %1 grunneiningum.';
        Text023 : TextConst ENU='%1 %2 cannot be found in the %3 or %4 table.',ISL='%1 %2 finnst ekki í töflunni %3 eða %4.';
        Text024 : TextConst ENU='%1 and %2 cannot both be empty when %3 is used.',ISL='%1 og %2 geta ekki báðir verið auðir þegar %3 er notað.';
        Text025 : TextConst ENU='No %1 has been posted for %2 %3 and %4 %5.',ISL='Ekkert %1 hefur verið bókað fyrir %2 %3 og %4 %5.';
        Text026 : TextConst ENU='You cannot change %1 if the item charge has already been posted.',ISL='Ekki er hægt að breyta %1 ef kostnaðaraukinn hefur þegar verið bókaður.';
        SalesHeader : Record "Transport Waybill SAM";
        SalesLine2 : Record "Transport Line";
        TempSalesLine : Record "Transport Line";
        SalesSetup : Record "Transport Setup";
        Item : Record Item;
        UnitOfMeasure : Record "Unit of Measure";
        ShippingAgentServices : Record "Shipping Agent Services";
        InvtSetup : Record "Inventory Setup";
        Location : Record Location;
        CalChange : Record "Customized Calendar Change";
        Currency : Record Currency;
        SKU : Record "Stockkeeping Unit";
        IOMrec : Record "Item Unit of Measure";
        PriceCalcMgt : Codeunit "Sales Price Calc. Mgt.";
        ItemCheckAvail : Codeunit "Item-Check Avail.";
        UOMMgt : Codeunit "Unit of Measure Management";
        AddOnIntegrMgt : Codeunit AddOnIntegrManagement;
        DimMgt : Codeunit DimensionManagement;
        ItemSubstitutionMgt : Codeunit "Item Subst.";
        TransferExtendedText : Codeunit "Transfer Extended Text";
        CalendarMgmt : Codeunit "Calendar Management";
        TransportItmeType : Record "Transport-Item Type";
        SSCCrefREC : Record "SSCC Ref.";
        FullAutoReservation : Boolean;
        StatusCheckSuspended : Boolean;
        HasBeenShown : Boolean;
        PlannedShipmentDateCalculated : Boolean;
        PlannedDeliveryDateCalculated : Boolean;
        ServMgtDocType : Integer;
        ServMgtDocNo : Code[20];
        Text027 : TextConst ENU='Item Tracking Lines exist for associated purchase order %1. Shall the lines be copied?',ISL='Vörurakningarlínur eru til fyrir tengda innkaupapöntun %1. Á að afrita línurnar?';
        Text028 : TextConst ENU='You cannot change the %1 when the %2 has been filled in.',ISL='Ekki er hægt að breyta %1 þegar %2 hefur verið slegið inn.';
        Text029 : TextConst ENU='must be positive',ISL='verður að vera jákvæð';
        Text030 : TextConst ENU='must be negative',ISL='verður að vera neikvæð';
        Text031 : TextConst ENU='You must either specify %1 or %2.',ISL='Tilgreina þarf annaðhvort %1 eða %2.';
        Text032 : TextConst ENU='You must select a %1 that applies to a range of entries when the related service contract is %2.',ISL='Velja verður %1 sem jafnast á færslur á ákveðnu bili ef tengdur þjónustusamningur er %2.';
        Text033 : TextConst ENU='You cannot modify the %1 field if the %2 and/or %3 fields are empty.',ISL='Ekki er hægt að breyta reitnum %1 ef reitirnir %2 og/eða %3 eru auðir.';

    local procedure CalcBaseQty(Qty : Decimal) : Decimal;
    begin
        TESTFIELD("Qty. per Unit of Measure");
        exit(ROUND(Qty * "Qty. per Unit of Measure",0.00001));
    end;

    procedure SetSalesHeader(NewSalesHeader : Record "Transport Waybill SAM");
    begin
        SalesHeader := NewSalesHeader;
    end;

    local procedure GetSalesHeader();
    begin
        TESTFIELD("Document No.");
        if "Document No." <> SalesHeader."Document No." then begin
          if not StatusCheckSuspended then
            SalesHeader.GET("Document No.");
        end;
    end;

    local procedure GetItem();
    begin
        TESTFIELD("No.");
        if "No." <> Item."No." then
          Item.GET("No.");
    end;

    local procedure UpdateUnitPrice(CalledByFieldNo : Integer);
    begin
    end;

    procedure UpdateAmounts();
    begin
    end;

    local procedure UpdateVATAmounts();
    var
        SalesLine2 : Record "Transport Line";
        TotalLineAmount : Decimal;
        TotalInvDiscAmount : Decimal;
        TotalAmount : Decimal;
        TotalAmountInclVAT : Decimal;
        TotalQuantityBase : Decimal;
    begin
    end;

    procedure GetDate() : Date;
    begin
        if ("Document Type" in ["Document Type"::"Posted Invoice","Document Type"::Quote]) and
           (SalesHeader."Posting Date" = 0D)
        then
          exit(WORKDATE);
        exit(SalesHeader."Posting Date");
    end;

    procedure ItemAvailability(AvailabilityType : Option Date,Variant,Location,Bin);
    begin
    end;

    procedure ShowDimensions();
    var
        DocDim : Integer;
    begin
        TESTFIELD("Document No.");
        TESTFIELD("Line No.");
        /*ath... sbr n³ja
        DocDim.SETRANGE("Table ID",DATABASE::"Sales Line");
        DocDim.SETRANGE("Document Type","Document Type");
        DocDim.SETRANGE("Document No.","Document No.");
        DocDim.SETRANGE("Line No.","Line No.");
        DocDimensions.SETTABLEVIEW(DocDim);
        DocDimensions.RUNMODAL;
        */

    end;

    procedure CreateDim(Type1 : Integer;No1 : Code[20];Type2 : Integer;No2 : Code[20];Type3 : Integer;No3 : Code[20]);
    var
        SourceCodeSetup : Record "Source Code Setup";
        TableID : array [10] of Integer;
        No : array [10] of Code[20];
    begin
    end;

    procedure ValidateShortcutDimCode(FieldNumber : Integer;var ShortcutDimCode : Code[20]);
    begin
        DimMgt.ValidateDimValueCode(FieldNumber,ShortcutDimCode);
        /*ath sbr n³ja
        IF "Line No." <> 0 THEN
          DimMgt.SaveDocDim(
            DATABASE::"Sales Line","Document Type","Document No.",
            "Line No.",FieldNumber,ShortcutDimCode)
        ELSE
          DimMgt.SaveTempDim(FieldNumber,ShortcutDimCode);
         */

    end;

    procedure LookupShortcutDimCode(FieldNumber : Integer;var ShortcutDimCode : Code[20]);
    begin
        DimMgt.LookupDimValueCode(FieldNumber,ShortcutDimCode);
        /*ath sbr n³ja
        IF "Line No." <> 0 THEN
          DimMgt.SaveDocDim(
            DATABASE::"Sales Line","Document Type","Document No.",
            "Line No.",FieldNumber,ShortcutDimCode)
        ELSE
          DimMgt.SaveTempDim(FieldNumber,ShortcutDimCode);
        */

    end;

    procedure ShowShortcutDimCode(var ShortcutDimCode : array [8] of Code[20]);
    begin
        /*ath sbr nyja
        if "Line No." <> 0 then
        
          DimMgt.ShowDocDim(
            DATABASE::"Sales Line","Document Type","Document No.",
            "Line No.",ShortcutDimCode)
        ELSE
          DimMgt.ShowTempDim(ShortcutDimCode);
        */

    end;

    procedure ShowItemSub();
    begin
    end;

    procedure ShowNonstock();
    begin
    end;

    local procedure GetFieldCaption(FieldNumber : Integer) : Text[100];
    var
        "Field" : Record "Field";
    begin
        Field.GET(DATABASE::"Sales Line",FieldNumber);
        exit(Field."Field Caption");
    end;

    local procedure GetCaptionClass(FieldNumber : Integer) : Text[80];
    begin
        if not SalesHeader.GET("Document No.") then begin
          SalesHeader."Document No." := '';
          SalesHeader.INIT;
        end;
    end;

    local procedure GetSKU() : Boolean;
    begin
    end;

    local procedure GetUnitCost();
    begin
        TESTFIELD("No.");
        GetItem;
        "Qty. per Unit of Measure" := UOMMgt.GetQtyPerUnitOfMeasure(Item,"Unit of Measure Code");
        if GetSKU then
          VALIDATE("Unit Cost (LCY)",SKU."Unit Cost" * "Qty. per Unit of Measure")
        else
          VALIDATE("Unit Cost (LCY)",Item."Unit Cost" * "Qty. per Unit of Measure");
    end;

    local procedure CalcUnitCost(ItemLedgEntry : Record "Item Ledger Entry") : Decimal;
    var
        ValueEntry : Record "Value Entry";
        InvdAdjustedCost : Decimal;
        ExpAdjustedCost : Decimal;
        UnitCost : Decimal;
    begin
        ValueEntry.RESET;
        ValueEntry.SETCURRENTKEY("Item Ledger Entry No.","Expected Cost");
        ValueEntry.SETRANGE("Item Ledger Entry No.",ItemLedgEntry."Entry No.");
        if ItemLedgEntry."Completely Invoiced" then
          ValueEntry.SETRANGE("Expected Cost",false);
        if ValueEntry.FIND('-') then
          repeat
            if ValueEntry."Expected Cost" then
              ExpAdjustedCost := ExpAdjustedCost + ValueEntry."Cost Amount (Expected)"
            else
              InvdAdjustedCost := InvdAdjustedCost + ValueEntry."Cost Amount (Actual)";
          until ValueEntry.NEXT = 0;
        UnitCost :=
          ((ExpAdjustedCost / ItemLedgEntry.Quantity *
           (ItemLedgEntry.Quantity - ItemLedgEntry."Invoiced Quantity")) +
          InvdAdjustedCost) / ItemLedgEntry.Quantity;
        exit(ABS(UnitCost * "Qty. per Unit of Measure"));
    end;

    local procedure TestStatusOpen();
    begin
        if StatusCheckSuspended then
          exit;
        GetSalesHeader;
    end;

    procedure SuspendStatusCheck(Suspend : Boolean);
    begin
        StatusCheckSuspended := Suspend;
    end;

    procedure UpdateVATOnLines(QtyType : Option General,Invoicing,Shipping;var SalesHeader : Record "Sales Header";var SalesLine : Record "Sales Line";var VATAmountLine : Record "VAT Amount Line");
    var
        TempVATAmountLineRemainder : Record "VAT Amount Line" temporary;
        Currency : Record Currency;
        RecRef : RecordRef;
        xRecRef : RecordRef;
        ChangeLogMgt : Codeunit "Change Log Management";
        NewAmount : Decimal;
        NewAmountIncludingVAT : Decimal;
        NewVATBaseAmount : Decimal;
        VATAmount : Decimal;
        VATDifference : Decimal;
        InvDiscAmount : Decimal;
        LineAmountToInvoice : Decimal;
    begin
        if QtyType = QtyType::Shipping then
          exit;
        if SalesHeader."Currency Code" = '' then
          Currency.InitRoundingPrecision
        else
          Currency.GET(SalesHeader."Currency Code");

        TempVATAmountLineRemainder.DELETEALL;

        with SalesLine do begin
          SETRANGE("Document Type",SalesHeader."Document Type");
          SETRANGE("Document No.",SalesHeader."No.");
          SETFILTER(Type,'>0');
          SETFILTER(Quantity,'<>0');
          case QtyType of
            QtyType::Invoicing:
              SETFILTER("Qty. to Invoice",'<>0');
            QtyType::Shipping:
              SETFILTER("Qty. to Ship",'<>0');
          end;
          LOCKTABLE;
          if FIND('-') then
            repeat
              VATAmountLine.GET("VAT Identifier","VAT Calculation Type","Tax Group Code",false,"Line Amount" >= 0);
              if VATAmountLine.Modified then begin
                xRecRef.GETTABLE(SalesLine);
                if not TempVATAmountLineRemainder.GET(
                  "VAT Identifier","VAT Calculation Type","Tax Group Code",false,"Line Amount" >= 0)
                then begin
                  TempVATAmountLineRemainder := VATAmountLine;
                  TempVATAmountLineRemainder.INIT;
                  TempVATAmountLineRemainder.INSERT;
                end;

                if QtyType = QtyType::General then
                  LineAmountToInvoice := "Line Amount"
                else
                  LineAmountToInvoice :=
                    ROUND("Line Amount" * "Qty. to Invoice" / Quantity,Currency."Amount Rounding Precision");

                if "Allow Invoice Disc." then begin
                  if VATAmountLine."Inv. Disc. Base Amount" = 0 then
                    InvDiscAmount := 0
                  else begin
                    TempVATAmountLineRemainder."Invoice Discount Amount" :=
                      TempVATAmountLineRemainder."Invoice Discount Amount" +
                      VATAmountLine."Invoice Discount Amount" * LineAmountToInvoice /
                      VATAmountLine."Inv. Disc. Base Amount";
                    InvDiscAmount :=
                      ROUND(
                        TempVATAmountLineRemainder."Invoice Discount Amount",Currency."Amount Rounding Precision");
                    TempVATAmountLineRemainder."Invoice Discount Amount" :=
                      TempVATAmountLineRemainder."Invoice Discount Amount" - InvDiscAmount;
                  end;
                  if QtyType = QtyType::General then begin
                    "Inv. Discount Amount" := InvDiscAmount;
                    CalcInvDiscToInvoice;
                  end else
                    "Inv. Disc. Amount to Invoice" := InvDiscAmount;
                end else
                  InvDiscAmount := 0;

                if QtyType = QtyType::General then
                  if SalesHeader."Prices Including VAT" then begin
                    if (VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount" = 0) or
                       ("Line Amount" = 0)
                    then begin
                      VATAmount := 0;
                      NewAmountIncludingVAT := 0;
                    end else begin
                      VATAmount :=
                        TempVATAmountLineRemainder."VAT Amount" +
                        VATAmountLine."VAT Amount" *
                        ("Line Amount" - "Inv. Discount Amount") /
                        (VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount");
                      NewAmountIncludingVAT :=
                        TempVATAmountLineRemainder."Amount Including VAT" +
                        VATAmountLine."Amount Including VAT" *
                        ("Line Amount" - "Inv. Discount Amount") /
                        (VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount");
                    end;
                    NewAmount :=
                      ROUND(NewAmountIncludingVAT,Currency."Amount Rounding Precision") -
                      ROUND(VATAmount,Currency."Amount Rounding Precision");
                    NewVATBaseAmount :=
                      ROUND(
                        NewAmount * (1 - SalesHeader."VAT Base Discount %" / 100),
                        Currency."Amount Rounding Precision");
                  end else begin
                    if "VAT Calculation Type" = "VAT Calculation Type"::"Full VAT" then begin
                      VATAmount := "Line Amount" - "Inv. Discount Amount";
                      NewAmount := 0;
                      NewVATBaseAmount := 0;
                    end else begin
                      NewAmount := "Line Amount" - "Inv. Discount Amount";
                      NewVATBaseAmount :=
                        ROUND(
                          NewAmount * (1 - SalesHeader."VAT Base Discount %" / 100),
                          Currency."Amount Rounding Precision");
                      if VATAmountLine."VAT Base" = 0 then
                        VATAmount := 0
                      else
                        VATAmount :=
                          TempVATAmountLineRemainder."VAT Amount" +
                          VATAmountLine."VAT Amount" * NewAmount / VATAmountLine."VAT Base";
                    end;
                    NewAmountIncludingVAT := NewAmount + ROUND(VATAmount,Currency."Amount Rounding Precision");
                  end
                else begin
                  if (VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount") = 0 then
                    VATDifference := 0
                  else
                    VATDifference :=
                      TempVATAmountLineRemainder."VAT Difference" +
                      VATAmountLine."VAT Difference" * (LineAmountToInvoice - InvDiscAmount) /
                      (VATAmountLine."Line Amount" - VATAmountLine."Invoice Discount Amount");
                  if LineAmountToInvoice = 0 then
                    "VAT Difference" := 0
                  else
                    "VAT Difference" := ROUND(VATDifference,Currency."Amount Rounding Precision");
                end;
                if (QtyType = QtyType::General) and (SalesHeader.Status = SalesHeader.Status::Released) then begin
                  Amount := NewAmount;
                  "Amount Including VAT" := ROUND(NewAmountIncludingVAT,Currency."Amount Rounding Precision");
                  "VAT Base Amount" := NewVATBaseAmount;
                end;
                SalesLine.InitOutstanding;
                MODIFY;
                RecRef.GETTABLE(SalesLine);
                ChangeLogMgt.LogModification(RecRef);//,xRecRef);

                TempVATAmountLineRemainder."Amount Including VAT" :=
                  NewAmountIncludingVAT - ROUND(NewAmountIncludingVAT,Currency."Amount Rounding Precision");
                TempVATAmountLineRemainder."VAT Amount" := VATAmount - NewAmountIncludingVAT + NewAmount;
                TempVATAmountLineRemainder."VAT Difference" := VATDifference - "VAT Difference";
                TempVATAmountLineRemainder.MODIFY;
              end;
            until NEXT = 0;
          SETRANGE(Type);
          SETRANGE(Quantity);
          SETRANGE("Qty. to Invoice");
          SETRANGE("Qty. to Ship");
        end;
    end;

    procedure CalcVATAmountLines(QtyType : Option General,Invoicing,Shipping;var SalesHeader : Record "Sales Header";var SalesLine : Record "Sales Line";var VATAmountLine : Record "VAT Amount Line");
    var
        Currency : Record Currency;
        SalesTaxCalculate : Codeunit "Sales Tax Calculate";
        QtyFactor : Decimal;
    begin
        if SalesHeader."Currency Code" = '' then
          Currency.InitRoundingPrecision
        else
          Currency.GET(SalesHeader."Currency Code");

        VATAmountLine.DELETEALL;

        with SalesLine do begin
          SETRANGE("Document Type",SalesHeader."Document Type");
          SETRANGE("Document No.",SalesHeader."No.");
          SETFILTER(Type,'>0');
          SETFILTER(Quantity,'<>0');
          if FIND('-') then
            repeat
              if "VAT Calculation Type" in
                 ["VAT Calculation Type"::"Reverse Charge VAT","VAT Calculation Type"::"Sales Tax"]
              then
                "VAT %" := 0;
              if not VATAmountLine.GET(
                "VAT Identifier","VAT Calculation Type","Tax Group Code",false,"Line Amount" >= 0)
              then begin
                VATAmountLine.INIT;
                VATAmountLine."VAT Identifier" := "VAT Identifier";
                VATAmountLine."VAT Calculation Type" := "VAT Calculation Type";
                VATAmountLine."Tax Group Code" := "Tax Group Code";
                VATAmountLine."VAT %" := "VAT %";
                VATAmountLine.Modified := true;
                VATAmountLine.Positive := "Line Amount" >= 0;
                VATAmountLine.INSERT;
              end;
              case QtyType of
                QtyType::General :
                  begin
                    VATAmountLine.Quantity := VATAmountLine.Quantity + "Quantity (Base)";
                    VATAmountLine."Line Amount" := VATAmountLine."Line Amount" + "Line Amount";
                    if "Allow Invoice Disc." then
                      VATAmountLine."Inv. Disc. Base Amount" :=
                        VATAmountLine."Inv. Disc. Base Amount" + "Line Amount";
                    VATAmountLine."Invoice Discount Amount" :=
                      VATAmountLine."Invoice Discount Amount" + "Inv. Discount Amount";
                    VATAmountLine."VAT Difference" := VATAmountLine."VAT Difference" + "VAT Difference";
                    VATAmountLine.MODIFY;
                  end;
                QtyType::Invoicing :
                  begin
                    QtyFactor := "Qty. to Invoice" / Quantity;
                    VATAmountLine.Quantity := VATAmountLine.Quantity + "Qty. to Invoice (Base)";
                    VATAmountLine."Line Amount" :=
                      VATAmountLine."Line Amount" +
                      ROUND("Line Amount" * QtyFactor,Currency."Amount Rounding Precision");
                    if "Allow Invoice Disc." then
                      VATAmountLine."Inv. Disc. Base Amount" :=
                        VATAmountLine."Inv. Disc. Base Amount" +
                        ROUND("Line Amount" * QtyFactor,Currency."Amount Rounding Precision");
                    VATAmountLine."Invoice Discount Amount" :=
                      VATAmountLine."Invoice Discount Amount" + "Inv. Disc. Amount to Invoice";
                    VATAmountLine."VAT Difference" := VATAmountLine."VAT Difference" + "VAT Difference";
                    VATAmountLine.MODIFY;
                  end;
                QtyType::Shipping :
                  begin
                    if "Document Type" in
                        ["Document Type"::"Return Order","Document Type"::"Credit Memo"]
                    then begin
                      QtyFactor := "Return Qty. to Receive" / Quantity;
                      VATAmountLine.Quantity := VATAmountLine.Quantity + "Return Qty. to Receive (Base)";
                    end else begin
                      QtyFactor := "Qty. to Ship" / Quantity;
                      VATAmountLine.Quantity := VATAmountLine.Quantity + "Qty. to Ship (Base)";
                    end;
                    VATAmountLine."Line Amount" :=
                      VATAmountLine."Line Amount" +
                      ROUND("Line Amount" * QtyFactor,Currency."Amount Rounding Precision");
                    if "Allow Invoice Disc." then
                      VATAmountLine."Inv. Disc. Base Amount" :=
                        VATAmountLine."Inv. Disc. Base Amount" +
                        ROUND("Line Amount" * QtyFactor,Currency."Amount Rounding Precision");
                    VATAmountLine."Invoice Discount Amount" :=
                      VATAmountLine."Invoice Discount Amount" +
                      ROUND("Inv. Discount Amount" * QtyFactor,Currency."Amount Rounding Precision");
                    VATAmountLine."VAT Difference" := VATAmountLine."VAT Difference" + "VAT Difference";
                    VATAmountLine.MODIFY;
                  end;
              end;
            until NEXT = 0;
          SETRANGE(Type);
          SETRANGE(Quantity);
        end;

        with VATAmountLine do
          if FIND('-') then
            repeat
              if SalesHeader."Prices Including VAT" then begin
                case "VAT Calculation Type" of
                  "VAT Calculation Type"::"Normal VAT",
                  "VAT Calculation Type"::"Reverse Charge VAT":
                    begin
                      "VAT Base" :=
                        ROUND(
                          ("Line Amount" - "Invoice Discount Amount") / (1 + "VAT %" / 100),
                          Currency."Amount Rounding Precision") - "VAT Difference";
                      "VAT Amount" :=
                        "VAT Difference" +
                        ROUND(
                          ("Line Amount" - "Invoice Discount Amount" - "VAT Base" - "VAT Difference") *
                          (1 - SalesHeader."VAT Base Discount %" / 100),
                          Currency."Amount Rounding Precision",Currency.VATRoundingDirection);
                      "Amount Including VAT" := "VAT Base" + "VAT Amount";
                    end;
                  "VAT Calculation Type"::"Full VAT":
                    begin
                      "VAT Base" := 0;
                      "VAT Amount" := "VAT Difference" + "Line Amount" - "Invoice Discount Amount";
                      "Amount Including VAT" := "VAT Amount";
                    end;
                  "VAT Calculation Type"::"Sales Tax":
                    begin
                      "Amount Including VAT" := "Line Amount" - "Invoice Discount Amount";
                      "VAT Base" :=
                        ROUND(
                          SalesTaxCalculate.ReverseCalculateTax(
                            SalesHeader."Tax Area Code","Tax Group Code",SalesHeader."Tax Liable",
                            SalesHeader."Posting Date","Amount Including VAT",Quantity,SalesHeader."Currency Factor"),
                          Currency."Amount Rounding Precision");
                      "VAT Amount" := "VAT Difference" + "Amount Including VAT" - "VAT Base";
                      if "VAT Base" = 0 then
                        "VAT %" := 0
                      else
                        "VAT %" := ROUND(100 * "VAT Amount" / "VAT Base",0.00001);
                    end;
                end;
              end else begin
                case "VAT Calculation Type" of
                  "VAT Calculation Type"::"Normal VAT",
                  "VAT Calculation Type"::"Reverse Charge VAT":
                    begin
                      "VAT Base" := "Line Amount" - "Invoice Discount Amount";
                      "VAT Amount" :=
                        "VAT Difference" +
                        ROUND(
                          "VAT Base" * "VAT %" / 100 * (1 - SalesHeader."VAT Base Discount %" / 100),
                          Currency."Amount Rounding Precision",Currency.VATRoundingDirection);
                      "Amount Including VAT" := "Line Amount" - "Invoice Discount Amount" + "VAT Amount";
                    end;
                  "VAT Calculation Type"::"Full VAT":
                    begin
                      "VAT Base" := 0;
                      "VAT Amount" := "VAT Difference" + "Line Amount" - "Invoice Discount Amount";
                      "Amount Including VAT" := "VAT Amount";
                    end;
                  "VAT Calculation Type"::"Sales Tax":
                    begin
                      "VAT Base" := "Line Amount" - "Invoice Discount Amount";
                      "VAT Amount" :=
                        SalesTaxCalculate.CalculateTax(
                          SalesHeader."Tax Area Code","Tax Group Code",SalesHeader."Tax Liable",
                          SalesHeader."Posting Date","VAT Base",Quantity,SalesHeader."Currency Factor");
                      if "VAT Base" = 0 then
                        "VAT %" := 0
                      else
                        "VAT %" := ROUND(100 * "VAT Amount" / "VAT Base",0.00001);
                      "VAT Amount" :=
                        "VAT Difference" +
                        ROUND("VAT Amount",Currency."Amount Rounding Precision",Currency.VATRoundingDirection);
                      "Amount Including VAT" := "VAT Base" + "VAT Amount";
                    end;
                end;
              end;
              "Calculated VAT Amount" := "VAT Amount" - "VAT Difference";
              MODIFY;
            until NEXT = 0;
    end;

    local procedure CalcInvDiscToInvoice();
    var
        OldInvDiscAmtToInv : Decimal;
    begin
    end;

    procedure UpdateDates();
    begin
        if "Promised Delivery Date" <> 0D then
          VALIDATE("Promised Delivery Date")
        else
          if "Requested Delivery Date" <> 0D then
            VALIDATE("Requested Delivery Date")
          else
            VALIDATE("Shipment Date");
    end;

    local procedure GetLocation(LocationCode : Code[10]);
    begin
        if LocationCode = '' then
          CLEAR(Location)
        else
          if Location.Code <> LocationCode then
            Location.GET(LocationCode);
    end;

    procedure FindItemType(var thlinerec : Record "Transport Line");
    var
        GroupingREC : Record "Transport-Item Type";
        ItemREC : Record Item;
    begin
        ItemREC.GET(thlinerec."No.");
        thlinerec."Item Category Code" := ItemREC."Item Category Code";
        thlinerec."Product Group Code" := ItemREC."Product Group Code";

        GroupingREC.SETRANGE("Item Category Code",thlinerec."Item Category Code");
        GroupingREC.SETRANGE("Product Group Code",thlinerec."Product Group Code");
        if GroupingREC.FIND('-') then
          thlinerec."Item Type" := GroupingREC."Item Type"
        else begin
          GroupingREC.SETRANGE("Product Group Code",'');
          if GroupingREC.FIND('-') then
            thlinerec."Item Type" := GroupingREC."Item Type"
          else begin
            GroupingREC.SETRANGE("Item Category Code",'');
            if GroupingREC.FIND('-') then
              thlinerec."Item Type" := GroupingREC."Item Type"
          end;
        end;
    end;
}

