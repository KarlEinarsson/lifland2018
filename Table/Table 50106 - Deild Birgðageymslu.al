table 50106 "Deild Birgðageymslu"
{
    // version Lifland


    fields
    {
        field(1;"Shortcut Dimension 1 Code";Code[20])
        {
            CaptionClass = '1,2,1';
            CaptionML = ENU='Shortcut Dimension 1 Code',
                        ISL='Flýtivídd 1 - Kóti';
            TableRelation = "Dimension Value".Code WHERE ("Global Dimension No."=CONST(1));
        }
        field(2;"Location Code";Code[10])
        {
            CaptionML = ENU='Location Code',
                        ISL='Kóti birgðageymslu';
            TableRelation = Location;
        }
    }

    keys
    {
        key(Key1;"Shortcut Dimension 1 Code")
        {
        }
    }

    fieldgroups
    {
    }
}

