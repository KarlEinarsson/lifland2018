tableextension 18 "Customer Extension" extends Customer
{
    fields
    {
        field(50000;"Freight Paid By";Option)
        {
            CaptionML = ENU = 'Freight Paid By',
                        ISL = 'Flutningsgjöld greiðast af';
            OptionMembers = Recipient,Prepaid,"Senders Account";
            OptionCaptionML = 	ENU = 'Recipient,Prepaid,Senders Account',
                                ISL = 'Viðtakanda,Staðgreitt,Í reikning sendanda';
            Description = '05';         
        }

        field(50001;"Social Security No.";Code[11])
        {
            CaptionML = ENU = 'Social Security No.',
                        ISL = 'Kennitala';
            Description = 'INNH';
        }

        field(50002;"Eftir á afsláttur %";Decimal)
        {
            CaptionML = ENU = 'Eftir á afsláttur %',
                        ISL = 'Eftir á afsláttur %';
            Description = '#01';                        
        }

        field(50003;"Reikningur með tölvupósti";Boolean)
        {
            CaptionML = ENU = 'Send Invoice via E-mail',
                        ISL = 'Reikningur með tölvupósti';
            Description = '#02,#03';
        }

        field(50004;"Discount Item No.";Code[20])
        {
            CaptionML = ENU = 'Discount Item No.',
                        ISL = 'Afsláttar vörunúmer';
            Description = '#08';
        }
    }
}