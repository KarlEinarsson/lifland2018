//codeunit 50050 "Vöruhús lítill miði"
/*{
    // version LSW18.0,RDN

    // LS8.0-01 ThJ #NAV-1255# 08.10.2014 - Add the possibility to print to file
    // LS8.0-02 ThJ #NAV-1522# 12.12.2014 - Printer selection not depending on userid
    // 
    // rdnr.ka = Rue de Net Reykjavik, Kristín Ásgeirsdóttir, kristin@ruedenet.net
    // 
    // NO   Date       Developer Jira-Issue  Description
    // -------------------------------------------------------------------------------------------------------------
    // #01  09.12.15   rdn.ka    LIF-??      Created CU (Based on LS retail EPL CU)
    // #02 23.06.16    hs        LIF-378     Laga framlegð í sölusögu. LS Retail kóða bætt við.

    Permissions = TableData "Sales Invoice Header"=rimd,
                  TableData "Value Entry"=rimd;

    trigger OnRun();
    var
        File : Record File;
        Tegund : Text;
        Str2 : Text;
        ReiknNo : Text;
        MyndNo : Text;
        Str4 : Text;
        PostedCreMemoHeader : Record "Purch. Cr. Memo Hdr.";
        PostedInvHeader : Record "Purch. Inv. Header";
        RecRef : RecordRef;
        MovePath : Text;
        CopyPath : Text;
        RecordLink : Record "Record Link";
        PostedStatem : Record Table99001485;
        MappingCU : Codeunit "H3 Import Pre-Mapping";
        Item : Record Item;
        ProductGroup : Record "Product Group";
        ItemCateg : Record "Item Category";
        ValueEntry : Record "Value Entry";
        ValueEntry2 : Record "Value Entry";
    begin
        // +#02
        
        //rdn.ka -+
        
        /*
        PostedStatem.RESET;
        PostedStatem.SETRANGE("Posted Date",290216d,080316d);
        Message('Fjöldi uppgjöra: ' + Format(PostedStatem.COUNT));
        
        IF PostedStatem.FINDSET THEN REPEAT
          MappingCU.FindStatement(PostedStatem."No.");
        UNTIL PostedStatem.NEXT = 0;
        */
        /*RecordLink.RESET;
        RecordLink.SETRANGE("User ID", 'RUE.ALFRED');
        RecordLink.SETRANGE(Type,RecordLink.Type::Link);
        RecordLink.SETFILTER(URL1,'@*Reikningsmyndir_NAV2009*');
        
        MESSAGE(FORMAT(RecordLink.COUNT));
        */
        /*File.SETRANGE(Path,'\\demeter\gogn\Rue de Net\Old_2009');
        File.SETRANGE("Is a file",TRUE);
        
        CopyPath := '\\demeter\sameign2\Upplýsingatækni\Navision\Reikningamyndir\Done\Reikningsmyndir_NAV2009\';
        MovePath := '\\demeter\gogn\Rue de Net\Old_Linked_2009\';
        IF File.FINDFIRST THEN REPEAT
        
          Tegund := COPYSTR(File.Name,1,STRPOS(File.Name,' - ') - 1);
        
          Str2 := COPYSTR(File.Name,STRPOS(File.Name,' - ') + 3);
        
          ReiknNo := COPYSTR(Str2,1,STRPOS(Str2, ' - '));
        
          //Str4 := COPYSTR(Str2,STRPOS(Str2, ' - ') + 3, STRPOS(Str2,'.') - (STRPOS(Str2, ' - ') + 3));
        
          IF Tegund = 'Bók. Kr. Reikningur' THEN BEGIN
            PostedCreMemoHeader.RESET;
            PostedCreMemoHeader.SETRANGE("No.",ReiknNo);
            IF PostedCreMemoHeader.FINDFIRST THEN BEGIN
              //Búa til RecordRef
              CLEAR(RecRef);
              RecRef.GETTABLE(PostedCreMemoHeader);
        
              //Búa til Link
              RecRef.ADDLINK(CopyPath + File.Name, File.Name);
        
              //Færa mynd
              IF NOT RENAME(File.Path + '\' + File.Name, MovePath + File.Name) THEN ERROR('2');
        
              //Afrita mynd
              IF NOT COPY(MovePath + File.Name, CopyPath + File.Name) THEN ERROR('1');
        
              COMMIT;
            END;
          END ELSE IF Tegund = 'Bók. Reikningur' THEN BEGIN
            PostedInvHeader.RESET;
            PostedInvHeader.SETRANGE("No.",ReiknNo);
            IF PostedInvHeader.FINDFIRST THEN BEGIN
              //Búa til RecordRef
              CLEAR(RecRef);
              RecRef.GETTABLE(PostedInvHeader);
        
              //Búa til Link
              RecRef.ADDLINK(CopyPath + File.Name, File.Name);
        
              //Færa mynd
              IF NOT RENAME(File.Path + '\' + File.Name, MovePath + File.Name) THEN ERROR('2');
        
              //Afrita mynd
              IF NOT COPY(MovePath + File.Name, CopyPath + File.Name) THEN ERROR('1');
        
              COMMIT;
            END;
          END ELSE
            ERROR('Ekki rétt týpa');
        UNTIL File.NEXT = 0;
        */
        /*//ValueEntry.SETRANGE(ValueEntry."Entry No.",2914040);
        CLEAR(ValueEntry);
        ValueEntry.SETRANGE("Item Ledger Entry Type",ValueEntry."Item Ledger Entry Type"::Transfer);
        ValueEntry.SETRANGE("Order Type",0);
        ValueEntry.SETFILTER("Posting Date",'1/9/2014..');
        IF NOT CONFIRM('Breyti '+FORMAT(ValueEntry.COUNT)+' Value Entry færslum!') THEN
          ERROR('hætt við');
        
        IF ValueEntry.FINDSET THEN REPEAT
          ValueEntry.Description:=COPYSTR(ValueEntry.Description,1,40)+'(#LIF-163)';
          ValueEntry."Order Type" := ValueEntry."Order Type"::Transfer;
          ValueEntry.MODIFY;
        UNTIL ValueEntry.NEXT = 0;
        EXIT;
        */

    //end;
/*
    var
        RetailUserRec : Record Table10000742;
        ShelfLabelRec : Record Table99001573;
        ItemUnitOfMeasure : Record "Item Unit of Measure";
        LabelFunctions : Record Table99001574;
        LabelPrint : DotNet "'LSTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=194563f11b671d8c'.LSRetail.NAV.Utils.PrintUtil" RUNONCLIENT;
        dotNetFile : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
        dotNetArray : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Array";
        //PosSession : Codeunit Codeunit99008919;
        //FileMgt : Codeunit "File Management";
        PrinterName : Text;
        LineEnd : Text;
        UOMText : Text;
        SalesAmountText : Text;
        ISOFileName : Text[250];
        FromChars : Label '""""';
        ToChars : Label '''';
        Text001 : TextConst ENU='There is nothing to print.',ISL='Það er ekkert til útprentunar.';
        Text002 : TextConst ENU='Pr. ',ISL='Pr.';
        Text003 : TextConst ENU='Could not connect to printer',ISL='Gat ekki tengst prentara';
        Text004 : TextConst ENU='File %1 created',ISL='Skrá %1 stofnuð';

    procedure CreateItemLabel(Item : Record Item;Qty : Integer);
    var
        "Miðalýsing" : Text[30];
        BarcodeNo : Code[20];
        ItemDescription : Text[50];
        ItemNo : Code[20];
        BarcodeRec : Record Table99001451;
    begin
        ItemDescription := Item.Description; //"850->850Visual"(Item.Description);
        ItemNo := Item."No."; //"850->850Visual"(Item."No.");
        Miðalýsing := COPYSTR(CONVERTSTR(Item.Description,FromChars,ToChars),1,25);

        BarcodeRec.SETCURRENTKEY(BarcodeRec."Item No.");
        BarcodeRec.SETRANGE(BarcodeRec."Item No.",Item."No.");
        BarcodeRec.SETRANGE(BarcodeRec."Show for Item",TRUE);
        IF BarcodeRec.FINDFIRST THEN
          BarcodeNo := BarcodeRec."Barcode No."
        ELSE
        BEGIN
          BarcodeRec.SETRANGE(BarcodeRec."Show for Item");
          IF BarcodeRec.FINDFIRST THEN
            BarcodeNo := BarcodeRec."Barcode No."
        END;

        InitPrint('VÖRUHÚS LÍTILL');
        WriteLine(LineEnd);
        WriteLine('OD' + LineEnd);
        WriteLine('N' + LineEnd);
        WriteLine('I8,A' + LineEnd);
        WriteLine('WN' + LineEnd);
        WriteLine('ZB' + LineEnd);
        WriteLine('S3' + LineEnd);
        WriteLine('D7' + LineEnd);
        WriteLine('q360' + LineEnd);
        WriteLine('Q200,24' + LineEnd);
        WriteLine('A10,10,0,1,1,2,N,"' +Miðalýsing + '"' + LineEnd);
        WriteLine('A20,70,0,3,2,2,N,"' +ItemNo + '"' + LineEnd);
        WriteLine('B10,130,0,E30,3,2,45,B,"' +BarcodeNo + '"' + LineEnd);
        WriteLine('P' + FORMAT(Qty) + LineEnd);
        WriteLine('FR' + LineEnd);
        WriteLine(LineEnd);
    end;

    procedure "850->850Visual"(Txt : Text[250]) Svar : Text[250];
    var
        StafurTxt : Text[1];
        StafurChar : Char;
        StafurCharUt : Char;
        AGildi : Integer;
        "NýrTexti" : Text[250];
        i : Integer;
        LengdTexta : Integer;
    begin
        LengdTexta := STRLEN(Txt);
        FOR i := 1 TO LengdTexta DO BEGIN
          StafurTxt := COPYSTR(Txt,i,1);
          IF StafurTxt <> '' THEN BEGIN
            EVALUATE(StafurChar,StafurTxt);
            AGildi := StafurChar;
            StafurCharUt := StafurChar;
               CASE AGildi OF
                 160 : StafurCharUt := 180;  //á
                 130 : StafurCharUt := 178;  //é
                 161 : StafurCharUt := 173;  //í
                 162 : StafurCharUt := 189;  //ó
                 163 : StafurCharUt := 156;  //ú
                 236 : StafurCharUt := 141;  //ý
                 145 : StafurCharUt := 200;  //æ
                 231 : StafurCharUt := 135;  //þ
                 208 : StafurCharUt := 209;  //ð
                 148 : StafurCharUt := 203;  //ö
        
                 181 : StafurCharUt := 230;  //Á
                 144 : StafurCharUt := 197;  //É
                 214 : StafurCharUt := 153;  //Í
                 222 : StafurCharUt := 153;  //Ì
                 224 : StafurCharUt := 133;  //Ó
                 233 : StafurCharUt := 130;  //Ú
                 237 : StafurCharUt := 161;  //Ý
                 146 : StafurCharUt := 201;  //Æ
                 232 : StafurCharUt := 138;  //Þ
                 209 : StafurCharUt := 165;  //Ð
                 153 : StafurCharUt := 233;  //Ö
               END;
            StafurTxt := FORMAT(StafurCharUt);
          END;
          NýrTexti := NýrTexti + StafurTxt;
        END;  /*For-lykkja*/
        //Svar := NýrTexti;

    //end;
/*
    procedure CreatedText() : Text[30];
    var
        teljari : Integer;
        ChrStafur : Char;
        Texti : Text[30];
        TxtStafur : Text[30];
    begin
        FOR teljari := 120 TO 140 DO
           BEGIN
             ChrStafur := teljari;
             TxtStafur := FORMAT(ChrStafur);
             Texti := Texti + TxtStafur;
           END;

        EXIT(Texti);
    end;

    procedure InitPrint(LabelCode : Code[20]);
    var
        FuncProfile : Record Table99001515;
        RetailUser : Record Table10000742;
        StoreRec : Record Table99001470;
        LabelPrinter : Record Table99001606;
        CR : Char;
        LF : Char;
    begin
        CR := 13;
        LF := 10;
        LineEnd := FORMAT(CR)+FORMAT(LF);

        //LS8.0-02 -
        IF LabelPrinter.GET('',LabelPrinter."Label Type"::"Shelf Label",LabelCode) THEN
          PrinterName := LabelPrinter."Printer Name";
        //LS8.0-02 +
        IF LabelPrinter.GET(USERID,LabelPrinter."Label Type"::"Shelf Label",LabelCode) THEN
          PrinterName := LabelPrinter."Printer Name";

        IF ISNULL(LabelPrint) THEN
          LabelPrint := LabelPrint.PrintUtil;

        IF ISNULL(LabelPrint) THEN
          ERROR(Text003);
    end;

    procedure WriteLine(EPL_Line : Text);
    begin
        //LS8.0-01-
        IF LabelFunctions."Print to File" THEN
          SaveLine(EPL_Line)
        ELSE
          PrintLine(EPL_Line);
        //LS8.0-01+
    end;

    procedure PrintLine(EPL_Line : Text);
    begin
        IF NOT LabelPrint.PrintString(PrinterName,EPL_Line) THEN
          ERROR(Text003);
    end;

    procedure AlignAmount(Amount : Integer) : Text;
    begin
        CASE Amount OF
          0..9 : EXIT(FORMAT(406));
          10..99 : EXIT(FORMAT(334));
          100..999 : EXIT(FORMAT(262));
          1000..9999 : EXIT(FORMAT(190));
          10000..99999 : EXIT(FORMAT(108));
          100000..999999 : EXIT(FORMAT(108));
        END;
    end;

    procedure GetBarcodeType(BarcodeNo : Code[20]) : Code[3];
    var
        BarcodeType : Code[3];
    begin
        IF COPYSTR(BarcodeNo,1,2) <> '20' THEN BEGIN
          CASE STRLEN(BarcodeNo) OF
            6:
              IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                BarcodeType := 'UE0'                                // UPC-E code(1)
              ELSE
                BarcodeType := '1';                                 // Code 128
            7:
              IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                BarcodeType := 'UE0'                                // UPC-E code(1)
              ELSE
                IF Code2Int(COPYSTR(BarcodeNo,1,2)) >= 30 THEN
                  BarcodeType := 'E80'                              // EAN 8 code (3)
                ELSE
                  BarcodeType := '1';                               // Code 128
            8:
              IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                BarcodeType := 'UE0'                                // UPC-E code(1)
              ELSE
                IF Code2Int(COPYSTR(BarcodeNo,1,2)) >= 30 THEN
                  BarcodeType := 'E80'                              // EAN 8 code(3)
                ELSE
                  BarcodeType := '1';                               // Code 128
            11:
               IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                 BarcodeType := 'UA0'                               // UPC-A code(4)
               ELSE
                 BarcodeType := '1';                                // Code 128
            12:
               IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                 BarcodeType := 'UA0'                               // UPC-A Code(4)
               ELSE
                 IF Code2Int(COPYSTR(BarcodeNo,1,2)) >= 30 THEN
                   BarcodeType := 'E30'                             // EAN 13 (5)
                 ELSE
                   BarcodeType := '1';                              // Code 128
            13:
               IF (Code2Int(COPYSTR(BarcodeNo,1,2)) >= 30) OR (Code2Int(COPYSTR(BarcodeNo,1,2)) <= 13) THEN
                 BarcodeType := 'E30'                               // EAN 13 code(5)
               ELSE
                 IF COPYSTR(BarcodeNo,1,1) <> '2' THEN
                   BarcodeType := '1';                              // Code 128
            1..5,9,10,14..22:
               BarcodeType := '1';                                  // Code 128
          END;
        END;

        IF (COPYSTR(BarcodeNo,1,2) = '20') THEN BEGIN
          IF (STRLEN(BarcodeNo) = 12) OR (STRLEN(BarcodeNo) = 13) THEN   // *** instorecode
            BarcodeType := 'E30'
          ELSE
            BarcodeType := '1';
        END;

        EXIT(BarcodeType);
    end;

    procedure Code2Int(pCode : Code[10]) : Integer;
    var
        xInt : Integer;
    begin
        IF NOT EVALUATE(xInt,pCode) THEN
          xInt := 0;
        EXIT(xInt);
    end;

    procedure UpdateLabelStatus();
    var
        ShelfLabelPoster2 : Record Table99001573;
    begin
        ShelfLabelPoster2 := ShelfLabelRec;
        ShelfLabelPoster2.Printed := TRUE;
        ShelfLabelPoster2."Date Last Printed" := TODAY;
        ShelfLabelPoster2."Time Last Printed" := TIME;
        ShelfLabelPoster2.MODIFY;
    end;

    procedure CreateFile();
    begin
        //LS8.0-01
        ISOFileName := FileMgt.ServerTempFileName('txt');
    end;

    procedure SaveLine(EPL_Line : Text);
    begin
        //LS8.0-01
        dotNetFile.AppendAllText(ISOFileName,EPL_Line);
    end;

    procedure CloseFile();
    begin
        //LS8.0-01
        dotNetArray := dotNetFile.ReadAllLines(ISOFileName);
        dotNetFile.WriteAllLines(ISOFileName,dotNetArray);

        MESSAGE(STRSUBSTNO(Text004,CONVERTSTR(ISOFileName,'\','/')));
    end;
*/


