codeunit 50010 "H3 Import Pre-Mapping"
{
    // version RDN

    // rdn.ka = Rue de Net Reykjavik, Kristín Ásgeirsdóttir, kristin@ruedenet.net
    // 
    // Num Date       Jira       Developer Description
    // --- ---------- ---------- --------- -------------------
    // #01 29.02.16   LIF-303    rdn.ka    Changes for H3 Import.

    TableNo = "Data Exch.";

    trigger OnRun();
    begin
        MapH3Fields(Rec);
    end;

    local procedure MapH3Fields(DataExch : Record "Data Exch.");
    var
        DataExchField : Record "Data Exch. Field";
        NewDataExchField : Record "Data Exch. Field";
        tmpDataExchField : Record "Data Exch. Field" temporary;
    begin
        //DataExchField.SETRANGE("Posting Exch. No.",DataExch."Entry No."); //update
        DataExchField.SETRANGE(DataExchField."Node ID",'');
        IF DataExchField.FINDSET THEN
          REPEAT
              tmpDataExchField := DataExchField;
              CASE DataExchField."Column No." OF
                1 : DataExchField.Value := ChangeAccountType(DataExchField.Value);
              END;
              IF tmpDataExchField.Value <> DataExchField.Value THEN
                DataExchField.MODIFY;
          UNTIL DataExchField.NEXT = 0;
    end;

    local procedure ChangeAccountType(Value : Text) ReturnValue : Text;
    begin
        //G/L Account,Customer,Vendor,Bank Account,Fixed Asset,IC Partner
        CASE Value OF
            'F' : ReturnValue := '0';
            'V' : ReturnValue := '1';
            'L' : ReturnValue := '2';
        END;
    end;
}

