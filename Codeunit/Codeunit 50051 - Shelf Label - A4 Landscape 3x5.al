codeunit 50051 "Shelf Label - A4 Landscape 3x5"
{/*
    // version LSW18.0,RDN

    // LS8.0-01 ThJ #NAV-1255# 08.10.2014 - Add the possibility to print to file
    // LS8.0-02 ThJ #NAV-1522# 12.12.2014 - Printer selection not depending on userid
    // 
    // rdnr.ka = Rue de Net Reykjavik, Kristín Ásgeirsdóttir, kristin@ruedenet.net
    // 
    // NO   Date       Developer Jira-Issue  Description
    // -------------------------------------------------------------------------------------------------------------
    // #01  10.12.15   rdn.ka    LIF-??      Created CU (Based on LS retail EPL CU)

    TableNo = Table99001573;

    trigger OnRun();
    var
        Intamount : Integer;
        i : Integer;
    begin
        ShelfLabelRec.COPYFILTERS(Rec);
        IF ShelfLabelRec.FINDSET THEN BEGIN
          LabelFunctions.GET(LabelFunctions.Type::"Shelf Label",ShelfLabelRec."Label Code"); //LS8.0-01
          InitPrint(ShelfLabelRec."Label Code");
          //LS8.0-01-
          IF LabelFunctions."Print to File" THEN
            CreateFile;
          //LS8.0-01+
          REPEAT
            SetItemParameters(ShelfLabelRec);
            //#01 -
            WriteLine(LineEnd);
            WriteLine('OD' + LineEnd);
            WriteLine('N' + LineEnd);
            WriteLine('I8,A' + LineEnd);
            WriteLine('WN' + LineEnd);
            WriteLine('ZB' + LineEnd);
            WriteLine('S3' + LineEnd);
            WriteLine('D7' + LineEnd);
            WriteLine('q370' + LineEnd);
            WriteLine('Q200,16' + LineEnd);
            IF STRLEN(ShelfLabelRec."Text 1") > 0 THEN
              WriteLine('A10,16,0,3,1,2,N,"' + COPYSTR(CONVERTSTR(ShelfLabelRec."Text 1",'"',''''),1,25) + '"' + LineEnd);  //T21
            WriteLine('A10,100,0,3,1,1,N,"' + ShelfLabelRec."Item No." + '"' + LineEnd);  //T9
            WriteLine('A200,70,0,3,1,3,N,"kr. ' + FORMAT(ShelfLabelRec."Price on Shelf Label",0,'<Precision,0><Standard Format,3>') + '"' + LineEnd);  //A13
            WriteLine('B10,125,0,' + GetBarcodeType(ShelfLabelRec."Barcode No.") + ',3,3,50,N,"' + ShelfLabelRec."Barcode No." + '"'+LineEnd);  //T4
            WriteLine('P' + FORMAT(ShelfLabelRec.Quantity)+LineEnd);
            WriteLine('FR' + LineEnd);
            WriteLine(LineEnd);
            //#01 +
            //LS8.0-01+
            UpdateLabelStatus;
          UNTIL ShelfLabelRec.NEXT = 0;
          //LS8.0-01-
          IF LabelFunctions."Print to File" THEN
            CloseFile;
          //LS8.0-01+
        END ELSE
          ERROR(Text001 + ' ' + ShelfLabelRec.GETFILTERS);
    end;

    var
        RetailUserRec : Record Table10000742;
        ShelfLabelRec : Record Table99001573;
        Item : Record Item;
        ItemUnitOfMeasure : Record "Item Unit of Measure";
        LabelFunctions : Record Table99001574;
        LabelPrint : DotNet "'LSTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=194563f11b671d8c'.LSRetail.NAV.Utils.PrintUtil" RUNONCLIENT;
        dotNetFile : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
        dotNetArray : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Array";
        PosSession : Codeunit Codeunit99008919;
        FileMgt : Codeunit "File Management";
        PrinterName : Text;
        LineEnd : Text;
        UOMText : Text;
        SalesAmountText : Text;
        Text001 : TextConst ENU='There is nothing to print.',ISL='Það er ekkert til útprentunar.';
        Text002 : TextConst ENU='Pr. ',ISL='Pr.';
        Text003 : TextConst ENU='Could not connect to printer',ISL='Gat ekki tengst prentara';
        ISOFileName : Text[250];
        Text004 : TextConst ENU='File %1 created',ISL='Skrá %1 stofnuð';

    procedure SetItemParameters(var LabelRec : Record Table99001573);
    var
        RetailPriceUtil : Codeunit Codeunit99001462;
    begin
        Item.GET(LabelRec."Item No.");
        SalesAmountText := '';
        UOMText := '';

        IF LabelRec."Price on Shelf Label" = 0 THEN
          LabelRec."Price on Shelf Label" := RetailPriceUtil.GetValidRetailPrice2(LabelRec."Store No.",LabelRec."Item No.",
            LabelRec."Label Is Valid on Date",TIME,LabelRec."Unit of Measure",LabelRec.Variant,'','','','','');

        IF LabelRec."Comp. Price on Shelf Label" = 0 THEN
          LabelRec."Comp. Price on Shelf Label" := RetailPriceUtil.CalcComparisonPrice(
            Item,LabelRec."Price on Shelf Label" / ItemUnitOfMeasure."Qty. per Unit of Measure");

        IF ItemUnitOfMeasure.GET(LabelRec."Item No.",LabelRec."Unit of Measure") AND (ItemUnitOfMeasure."Text on Shelf Label" <> '') THEN
          UOMText := ItemUnitOfMeasure."Text on Shelf Label"
        ELSE
          UOMText := LabelRec."Unit of Measure";
        IF STRLEN(UOMText) > 0 THEN
          UOMText := COPYSTR(UOMText,1,6);

        SalesAmountText := FORMAT(LabelRec."Price on Shelf Label",0,'<Integer><Decimal,3>');
    end;

    procedure InitPrint(LabelCode : Code[20]);
    var
        FuncProfile : Record Table99001515;
        RetailUser : Record Table10000742;
        StoreRec : Record Table99001470;
        LabelPrinter : Record Table99001606;
        CR : Char;
        LF : Char;
    begin
        CR := 13;
        LF := 10;
        LineEnd := FORMAT(CR)+FORMAT(LF);

        //LS8.0-02 -
        IF LabelPrinter.GET('',LabelPrinter."Label Type"::"Shelf Label",LabelCode) THEN
          PrinterName := LabelPrinter."Printer Name";
        //LS8.0-02 +
        IF LabelPrinter.GET(USERID,LabelPrinter."Label Type"::"Shelf Label",LabelCode) THEN
          PrinterName := LabelPrinter."Printer Name";

        IF ISNULL(LabelPrint) THEN
          LabelPrint := LabelPrint.PrintUtil;

        IF ISNULL(LabelPrint) THEN
          ERROR(Text003);
    end;

    procedure WriteLine(EPL_Line : Text);
    begin
        //LS8.0-01-
        IF LabelFunctions."Print to File" THEN
          SaveLine(EPL_Line)
        ELSE
          PrintLine(EPL_Line);
        //LS8.0-01+
    end;

    procedure PrintLine(EPL_Line : Text);
    begin
        IF NOT LabelPrint.PrintString(PrinterName,EPL_Line) THEN
          ERROR(Text003);
    end;

    procedure AlignAmount(Amount : Integer) : Text;
    begin
        CASE Amount OF
          0..9 : EXIT(FORMAT(406));
          10..99 : EXIT(FORMAT(334));
          100..999 : EXIT(FORMAT(262));
          1000..9999 : EXIT(FORMAT(190));
          10000..99999 : EXIT(FORMAT(108));
          100000..999999 : EXIT(FORMAT(108));
        END;
    end;

    procedure GetBarcodeType(BarcodeNo : Code[20]) : Code[3];
    var
        BarcodeType : Code[3];
    begin
        IF COPYSTR(BarcodeNo,1,2) <> '20' THEN BEGIN
          CASE STRLEN(BarcodeNo) OF
            6:
              IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                BarcodeType := 'UE0'                                // UPC-E code(1)
              ELSE
                BarcodeType := '1';                                 // Code 128
            7:
              IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                BarcodeType := 'UE0'                                // UPC-E code(1)
              ELSE
                IF Code2Int(COPYSTR(BarcodeNo,1,2)) >= 30 THEN
                  BarcodeType := 'E80'                              // EAN 8 code (3)
                ELSE
                  BarcodeType := '1';                               // Code 128
            8:
              IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                BarcodeType := 'UE0'                                // UPC-E code(1)
              ELSE
                IF Code2Int(COPYSTR(BarcodeNo,1,2)) >= 30 THEN
                  BarcodeType := 'E80'                              // EAN 8 code(3)
                ELSE
                  BarcodeType := '1';                               // Code 128
            11:
               IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                 BarcodeType := 'UA0'                               // UPC-A code(4)
               ELSE
                 BarcodeType := '1';                                // Code 128
            12:
               IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                 BarcodeType := 'UA0'                               // UPC-A Code(4)
               ELSE
                 IF Code2Int(COPYSTR(BarcodeNo,1,2)) >= 30 THEN
                   BarcodeType := 'E30'                             // EAN 13 (5)
                 ELSE
                   BarcodeType := '1';                              // Code 128
            13:
               IF (Code2Int(COPYSTR(BarcodeNo,1,2)) >= 30) OR (Code2Int(COPYSTR(BarcodeNo,1,2)) <= 13) THEN
                 BarcodeType := 'E30'                               // EAN 13 code(5)
               ELSE
                 IF COPYSTR(BarcodeNo,1,1) <> '2' THEN
                   BarcodeType := '1';                              // Code 128
            1..5,9,10,14..22:
               BarcodeType := '1';                                  // Code 128
          END;
        END;

        IF (COPYSTR(BarcodeNo,1,2) = '20') THEN BEGIN
          IF (STRLEN(BarcodeNo) = 12) OR (STRLEN(BarcodeNo) = 13) THEN   // *** instorecode
            BarcodeType := 'E30'
          ELSE
            BarcodeType := '1';
        END;

        EXIT(BarcodeType);
    end;

    procedure Code2Int(pCode : Code[10]) : Integer;
    var
        xInt : Integer;
    begin
        IF NOT EVALUATE(xInt,pCode) THEN
          xInt := 0;
        EXIT(xInt);
    end;

    procedure UpdateLabelStatus();
    var
        ShelfLabelPoster2 : Record Table99001573;
    begin
        ShelfLabelPoster2 := ShelfLabelRec;
        ShelfLabelPoster2.Printed := TRUE;
        ShelfLabelPoster2."Date Last Printed" := TODAY;
        ShelfLabelPoster2."Time Last Printed" := TIME;
        ShelfLabelPoster2.MODIFY;
    end;

    procedure CreateFile();
    begin
        //LS8.0-01
        ISOFileName := FileMgt.ServerTempFileName('txt');
    end;

    procedure SaveLine(EPL_Line : Text);
    begin
        //LS8.0-01
        dotNetFile.AppendAllText(ISOFileName,EPL_Line);
    end;

    procedure CloseFile();
    begin
        //LS8.0-01
        dotNetArray := dotNetFile.ReadAllLines(ISOFileName);
        dotNetFile.WriteAllLines(ISOFileName,dotNetArray);

        MESSAGE(STRSUBSTNO(Text004,CONVERTSTR(ISOFileName,'\','/')));
    end;
*/}

