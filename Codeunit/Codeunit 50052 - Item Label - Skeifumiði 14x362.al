codeunit 50052 "Item Label - Skeifumiði 14x362"
{/*
    // version LSW18.0

    // LS8.0-01 ThJ #NAV-1255# 08.10.2014 - Add the possibility to print to file
    // LS8.0-02 ThJ #NAV-1522# 12.12.2014 - Printer selection not depending on userid
    // 
    // rdnr.ka = Rue de Net Reykjavik, Kristín Ásgeirsdóttir, kristin@ruedenet.net
    // 
    // NO   Date       Developer Jira-Issue  Description
    // -------------------------------------------------------------------------------------------------------------
    // #01  10.12.15   rdn.ka    LIF-??      Created CU (Based on LS retail EPL CU)

    TableNo = Table99001548;

    trigger OnRun();
    var
        Intamount : Integer;
        i : Integer;
    begin
        ItemLabelRec.COPYFILTERS(Rec);
        IF ItemLabelRec.FINDSET THEN BEGIN
          LabelFunctions.GET(LabelFunctions.Type::"Item Label","Label Code"); //LS8.0-01
          InitPrint(ItemLabelRec."Label Code");
          //LS8.0-01-
          IF LabelFunctions."Print to File" THEN
            CreateFile;
          //LS8.0-01+
          REPEAT
            SetItemParameters(ItemLabelRec);
            //#01 -
            WriteLine(LineEnd);
            WriteLine('OD' + LineEnd);
            WriteLine('N' + LineEnd);
            WriteLine('I8,A' + LineEnd);
            WriteLine('WN' + LineEnd);
            WriteLine('R0,0' + LineEnd);
            WriteLine('ZB' + LineEnd);
            WriteLine('S3' + LineEnd);
            WriteLine('D7' + LineEnd);
            WriteLine('q280' + LineEnd);
            WriteLine('Q96,16' + LineEnd);
            WriteLine('A10,20,0,1,1,1,N,"' + ItemLabelRec."Item No." + '"' + LineEnd);  //T9
            IF STRLEN(ItemLabelRec."Text 1") > 0 THEN
              WriteLine('A10,45,0,1,1,1,N,"' + COPYSTR(CONVERTSTR(ItemLabelRec."Text 1",'"',''''),1,25) + '"' + LineEnd);  //T21
            WriteLine('B28,60,0,' + GetBarcodeType(ItemLabelRec."Barcode No.") + ',2,2,40,N,"' + ItemLabelRec."Barcode No." + '"'+LineEnd);  //T4
            WriteLine('P' + FORMAT(ItemLabelRec.Quantity)+LineEnd);
            WriteLine('FR' + LineEnd);
            WriteLine(LineEnd);
            //#01 +
            //LS8.0-01 +
            UpdateLabelStatus;
          UNTIL ItemLabelRec.NEXT = 0;
          //LS8.0-01-
          IF LabelFunctions."Print to File" THEN
            CloseFile;
          //LS8.0-01+
        END ELSE
          ERROR(Text001 + ' ' + ItemLabelRec.GETFILTERS);
    end;

    var
        RetailUserRec : Record Table10000742;
        ItemLabelRec : Record Table99001548;
        Item : Record Item;
        ItemUnitOfMeasure : Record "Item Unit of Measure";
        LabelFunctions : Record Table99001574;
        LabelPrint : DotNet "'LSTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=194563f11b671d8c'.LSRetail.NAV.Utils.PrintUtil" RUNONCLIENT;
        dotNetFile : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.IO.File";
        dotNetArray : DotNet "'mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089'.System.Array";
        PosSession : Codeunit Codeunit99008919;
        FileMgt : Codeunit "File Management";
        PrinterName : Text;
        LineEnd : Text;
        SalesAmountText : Text;
        Text001 : TextConst ENU='There is nothing to print.',ISL='Það er ekkert til útprentunar.';
        Text002 : TextConst ENU='Pr. ',ISL='Pr.';
        Text003 : TextConst ENU='Could not connect to printer',ISL='Gat ekki tengst prentara';
        ISOFileName : Text;
        Text004 : TextConst ENU='File %1 created',ISL='Skrá %1 stofnuð';

    procedure SetItemParameters(var LabelRec : Record Table99001548);
    var
        RetailPriceUtil : Codeunit Codeunit99001462;
    begin
        Item.GET(LabelRec."Item No.");
        SalesAmountText := '';

        IF LabelRec."Price on Item Label" = 0 THEN
          LabelRec."Price on Item Label" := RetailPriceUtil.GetValidRetailPrice2(LabelRec."Store No.",LabelRec."Item No.",
            LabelRec."Label Is Valid on Date",TIME,LabelRec."Unit of Measure",LabelRec.Variant,'','','','','');

        SalesAmountText := FORMAT(LabelRec."Price on Item Label",0,'<Integer><Decimal,3>');
    end;

    procedure InitPrint(LabelCode : Code[20]);
    var
        FuncProfile : Record Table99001515;
        RetailUser : Record Table10000742;
        StoreRec : Record Table99001470;
        LabelPrinter : Record Table99001606;
        CR : Char;
        LF : Char;
    begin
        CR := 13;
        LF := 10;
        LineEnd := FORMAT(CR)+FORMAT(LF);

        //LS8.0-02 -
        IF LabelPrinter.GET('',LabelPrinter."Label Type"::"Shelf Label",LabelCode) THEN
          PrinterName := LabelPrinter."Printer Name";
        //LS8.0-02 +
        IF LabelPrinter.GET(USERID,LabelPrinter."Label Type"::"Item Label",LabelCode) THEN
          PrinterName := LabelPrinter."Printer Name";

        IF ISNULL(LabelPrint) THEN
          LabelPrint := LabelPrint.PrintUtil;

        IF ISNULL(LabelPrint) THEN
          ERROR(Text003);
    end;

    procedure WriteLine(EPL_Line : Text);
    begin
        //LS8.0-01
        IF LabelFunctions."Print to File" THEN
          SaveLine(EPL_Line)
        ELSE
          PrintLine(EPL_Line);
    end;

    procedure PrintLine(EPL_Line : Text);
    begin
        IF NOT LabelPrint.PrintString(PrinterName,EPL_Line) THEN
          ERROR(Text003);
    end;

    procedure SaveLine(EPL_Line : Text);
    begin
        //LS8.0-01
        dotNetFile.AppendAllText(ISOFileName,EPL_Line);
    end;

    procedure CreateFile();
    begin
        ISOFileName := FileMgt.ServerTempFileName('txt');
    end;

    procedure CloseFile();
    begin
        //LS8.0-01
        dotNetArray := dotNetFile.ReadAllLines(ISOFileName);
        dotNetFile.WriteAllLines(ISOFileName,dotNetArray);

        MESSAGE(STRSUBSTNO(Text004,CONVERTSTR(ISOFileName,'\','/')));
    end;

    procedure AlignAmount(Amount : Integer) : Text;
    begin
        CASE Amount OF
          0..9 : EXIT(FORMAT(560));
          10..99 : EXIT(FORMAT(450));
          100..999 : EXIT(FORMAT(340));
          1000..9999 : EXIT(FORMAT(230));
          10000..99999 : EXIT(FORMAT(120));
          100000..999999 : EXIT(FORMAT(10));
        END;
    end;

    procedure GetBarcodeType(BarcodeNo : Code[20]) : Code[3];
    var
        BarcodeType : Code[3];
    begin
        IF COPYSTR(BarcodeNo,1,2) <> '20' THEN BEGIN
          CASE STRLEN(BarcodeNo) OF
            6:
              IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                BarcodeType := 'UE0'                                // UPC-E code(1)
              ELSE
                BarcodeType := '1';                                 // Code 128
            7:
              IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                BarcodeType := 'UE0'                                // UPC-E code(1)
              ELSE
                IF Code2Int(COPYSTR(BarcodeNo,1,2)) >= 30 THEN
                  BarcodeType := 'E80'                              // EAN 8 code (3)
                ELSE
                  BarcodeType := '1';                               // Code 128
            8:
              IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                BarcodeType := 'UE0'                                // UPC-E code(1)
              ELSE
                IF Code2Int(COPYSTR(BarcodeNo,1,2)) >= 30 THEN
                  BarcodeType := 'E80'                              // EAN 8 code(3)
                ELSE
                  BarcodeType := '1';                               // Code 128
            11:
               IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                 BarcodeType := 'UA0'                               // UPC-A code(4)
               ELSE
                 BarcodeType := '1';                                // Code 128
            12:
               IF COPYSTR(BarcodeNo,1,1) = '0' THEN
                 BarcodeType := 'UA0'                               // UPC-A Code(4)
               ELSE
                 IF Code2Int(COPYSTR(BarcodeNo,1,2)) >= 30 THEN
                   BarcodeType := 'E30'                             // EAN 13 (5)
                 ELSE
                   BarcodeType := '1';                              // Code 128
            13:
               IF (Code2Int(COPYSTR(BarcodeNo,1,2)) >= 30) OR (Code2Int(COPYSTR(BarcodeNo,1,2)) <= 13) THEN
                 BarcodeType := 'E30'                               // EAN 13 code(5)
               ELSE
                 IF COPYSTR(BarcodeNo,1,1) <> '2' THEN
                   BarcodeType := '1';                              // Code 128
            1..5,9,10,14..22:
               BarcodeType := '1';                                  // Code 128
          END;
        END;

        IF (COPYSTR(BarcodeNo,1,2) = '20') THEN BEGIN
          IF (STRLEN(BarcodeNo) = 12) OR (STRLEN(BarcodeNo) = 13) THEN   // *** instorecode
            BarcodeType := 'E30'
          ELSE
            BarcodeType := '1';
        END;

        EXIT(BarcodeType);
    end;

    procedure Code2Int(pCode : Code[10]) : Integer;
    var
        xInt : Integer;
    begin
        IF NOT EVALUATE(xInt,pCode) THEN
          xInt := 0;
        EXIT(xInt);
    end;

    procedure UpdateLabelStatus();
    var
        ItemLabelPoster2 : Record Table99001548;
    begin
        ItemLabelPoster2 := ItemLabelRec;
        ItemLabelPoster2.Printed := TRUE;
        ItemLabelPoster2."Date Last Printed" := TODAY;
        ItemLabelPoster2."Time Last Printed" := TIME;
        ItemLabelPoster2.MODIFY;
    end;
*/}

