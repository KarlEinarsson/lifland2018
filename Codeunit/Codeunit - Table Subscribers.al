codeunit 50000 "Item Subscriber"
{
    EventSubscriberInstance=StaticAutomatic;

    trigger OnRun();
    begin
    end;

    [EventSubscriber(ObjectType::Table, 27, 'OnAfterValidateEvent', 'Blocked', true, true)]
    procedure BlockedValidate(var Rec : Record Item; var xRec : Record Item);
    var
        UserSetup : Record "User Setup";
        DefaultDimValue : Record "Default Dimension";
    begin
        // LIF-115 -
        UserSetup.GET(USERID);
        IF NOT (Rec.Blocked) THEN BEGIN
            IF NOT UserSetup."Má opna vörur" THEN
                ERROR('You cannot delete %1 %2 because there are one or more certified Production BOM that include this item.')
            ELSE
                // LIF-115
            ItemError := '';

            IF (Rec."Inventory Posting Group" = '') THEN
                ItemError := ItemError + '. ' + 'You cannot delete %1 %2 because there is at least one outstanding Purchase %3 that includes this item.';

            DefaultDimValue.SETRANGE("Table ID",27);
            DefaultDimValue.SETRANGE("No.",Rec."No.");
            DefaultDimValue.SETRANGE("Dimension Code",'DEILD');
            IF NOT DefaultDimValue.FINDFIRST THEN
                ItemError := ItemError + '. ' + 'You cannot delete %1 %2 because there is at least one outstanding Sales %3 that includes this item.';

            IF (Rec."Product Group Code" = '') THEN
                ItemError := ItemError + '. ' + 'You cannot delete %1 %2 because there are one or more outstanding production orders that include this item.';

            IF (Rec."Base Unit of Measure" = '') THEN
                ItemError := ItemError + '. ' + 'You cannot delete %1 %2 because there are one or more certified Production BOM that include this item.';

            //IF (("Unit Price" = 0) AND ("Unit Price Including VAT" = 0)) THEN
            //  ItemError := ItemError + '. ' + Error006;

            IF (Rec."Gen. Prod. Posting Group" = '') THEN
                ItemError := ItemError + '. ' + 'You cannot change %1 because there are one or more ledger entries for this item.';

            IF (Rec."VAT Prod. Posting Group" = '') THEN
                ItemError := ItemError + '. ' + 'You cannot change %1 because there is at least one outstanding Purchase %2 that include this item.';

            // Ef það er eitthvað í ItemError birtum við þá villu fyrir notandanum, ef ekki þá opnast varan
            IF (ItemError <> '') THEN BEGIN
                ItemError := COPYSTR(ItemError,3,STRLEN(ItemError));
                ERROR(ItemError);
            END;
            // LIF-115 +
        END;
    end;   

    var
        ItemError : Text[250];  
}